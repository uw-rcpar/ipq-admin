<!DOCTYPE html>
<html lang="en">
<head>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
    <script src="js/tinymce/jquery.tinymce.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/handlebars.js"></script>
    <script src="js/IrsFormWizard.js"></script>
    <script src="js/adminapp.js"></script>
	<script src="js/angular.min.js"></script>
	<script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
	<script src="js/NavModule.js"></script>
	<script>
    angular.element(document).ready(function() {
      angular.bootstrap($("#navModule"), ["NavModule"]);
    });
  </script>
	 <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
</head>
<body>

<?php include 'navbar.php'; ?>

	<div class="container">
		
	


		

<div class="modal hide fade" id="modal-login" data-backdrop="static">
	<form class="form-signin">
  <div class="modal-header">
    <h3>Please Sign In</h3>
  </div>
  <div class="modal-body">
   
        
        <input type="text" class="input-block-level" placeholder="Email address" name="login_email">
        <input type="password" class="input-block-level" placeholder="Password"  name="login_password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
     
  </div>
  <div class="modal-footer">
    <button class="btn btn-large btn-primary" type="submit">Sign in</button>
  </div>
  </form>
</div>

<!-- end modal login -->


	
	<div class="row wizardstep" id="step1" >
		<span class="span8 well offset1">
			<p class="lead"><span class="muted">Step 1</span> Which course does your question belong to?</p>

		</span>
	</div>
	<div class="row wizardstep" id="step2" style="display:none;">
		<span class="span8 well offset1">
			<p class="lead"><span class="muted">Step 2</span> Select the topics relevant to your question.</p>
				<p>You may come back to this step later, however your question will not be complete until it has been tagged with at least one topic.</p>

		</span>
	</div>

	<div class="row wizardstep" id="step3" style="display:none;">
		<div class="span10 well offset1">
			<p class="lead"><span class="muted">Step 3</span> Select Question Type</p>
			<div class="row">
				<div class="span5">
					<div class="btn-group btn-group-vertical rcpa-group" data-toggle="buttons-radio" id="question-type-nav">
						<button type="button" class="btn btn-rcpa btn-large" rel="multiple-choice">Multiple Choice</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-research">TBS - Research</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-multiple-list">TBS - Journal (Multiple List)</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-written-communication">TBS - Written Communication</button>
						<!--
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-irs-form">TBS - IRS Form</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-spreadsheet">TBS - Spreadsheet</button>
						-->
					</div>
				</div>
				<div class="span5" id="question-type-preview-image">
					<img src='img/question-types/multiple-choice.png' />
				</div>
			</div>
		</div>
	</div>


	<div class="row wizardstep" id="step4" style="display:none;">
		<div class="span10 well">

		</div>
	</div>


	<div class="modal hide fade" id="answer-option-window">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <p class="lead">Question Option</p>
	  </div>
	  <div class="modal-body">
	  	<p class="muted lead">Text of an answer</p>
	    <textarea id="option-text">
	    </textarea>

	    <p class="muted lead" style="margin-top:25px;">Is this answer correct?</p>
	    <div class="btn-group" data-toggle="buttons-radio" id="option-correct-btns">
  		  	<button type="button" class="btn btn-primary active" data-val="0">Wrong</button>
			<button type="button" class="btn btn-primary" data-val="1">Correct</button>
		</div>

		<p class="muted lead">Explain why:</p>
		<textarea id="option-explanation-text" style="width:100%;height:120px;">
	    </textarea>

	  </div>
	  <div class="modal-footer">
	    <a href="#" class="btn">Close</a>
	    <a href="#" class="btn btn-primary" id="save-option-btn">Save option</a>
	  </div>
	</div>



	<div class="row wizardstep" id="step5" style="display:none;">
		<div class="span8 well offset1">
			<div class="success-container">
				<p class="success lead">Your new question has been saved.<br/>
				<a class="proofLink" target="_blank">Check the question in the editor</a>
				</p>
			</div>
			<div class="error-container">
				<p class="fail lead">An error has occurred please contact Sam before continuing.</p>
			</div>
			<div class="duplicates">
				<h3>Error</h3>
				<div class="pull-left">
					<p>There are other questions that may be duplicates. Please click on a question below to view it:</p>
					<div class="well">
					</div>
				</div>
				<div class="buttons">
					<div class="btn-group">
							<div class="save-anyway btn btn-primary">Save Anyway</div>

							<a href="#" rel="new-question" class="btn btn-default">Create a New Question</a>
							
							<a href="#" rel="start-over"  class="btn btn-default">Start Over</a>
							  
					</div>
				</div>
			</div>

			<div class="row wizardstep-buttons">
				<div class="span3 offset1">

					<a href="#" rel="new-question" class="btn btn-block">Create a New Question</a></li>
					
				</div>
				<div class="span3">
					<a href="#" rel="start-over"  class="btn btn-block">Start Over</a></li>
					  
				</div>
			</div>
		</div>
	</div>




	<div class="navbar navbar-inverse navbar-fixed-bottom">
  		<div class="navbar-inner">
  			<div class="container">
<div class="span3 offset5">
	<div class="pagination wizardprogress">
		  <ul>
		    <li><a href="#step1" rel="0">1</a></li>
		    <li class="disabled"><a href="#step2" rel="1">2</a></li>
		    <li class="disabled"><a href="#step3" rel="2">3</a></li>
		    <li class="disabled"><a href="#step4" rel="3">4</a></li>
		    <li class="disabled"><a href="#step5" rel="4">5</a></li>
		</ul>
	</div>
</div>

			    <ul class="nav nav-pills pull-right">
			      <li><button href="#next" class="btn btn-large NextButton">Next</button></li>
			  </ul>
			</div>
		</div>
	</div>








    </div>


    <script id="section-nav" type="text/x-handlebars-template">
    <div class="span5 offset2">
		<div class="btn-group btn-block" data-toggle="buttons-radio" id="pick-section-nav">
		{{#each sections}}
		  <button type="button" class="btn btn-primary btn-large" rel="{{this}}">{{this}}</button>
		 {{/each}}
		</div>
	</div>
	</script>




	<script id="topics-select"  type="text/x-handlebars-template">

	{{#each chapters}}
		<p class="lead">{{chapter}}</p>
		<div class="btn-group rcpa-group" data-toggle="buttons-checkbox">
		{{#each topics}}
  			<button type="button" class="btn btn-rcpa btn-block h-topic" id="topic_{{id}}" 
  								data-topic="{{id}}" data-chapter="{{../id}}" value="{{id}}">{{topic}}</button>
 		{{/each}}
		</div>
	{{/each}}
	</script>

<script id="multiple-choice-form"  type="text/x-handlebars-template">
<p class="lead"><span class="muted">Step 4</span> Write your multiple choice question</p>
<div class="row">
	<div class="span10">
	<textarea id="question-text" name="question-text" style="width:100%;height:240px"></textarea>
	</div>
</div>
<div class="row" style="padding-top:20px;">
	<div class="span8" id="explain-container">

	</div>
	<div class="span2">
		<button class="btn btn-primary" id="new-option-btn">Add New Option</button>
	</div>
</div>
</script>

<script id="multiple-choice-option"  type="text/x-handlebars-template">
	<div class="q-choice span6 well" data-id="{{id}}">
		<div class="row">
			<div class="span1">
			{{debug}}
				{{#if is_correct}}
					<img src="img/is-correct.png" />
				{{else}}
					<img src="img/is-wrong.png" />
				{{/if}}
			</div>
			<div class="span5">
				<div class="row">
					<p>{{choice}}</p>
				</div>
				<div class="row">
					<div class="span4 offset1">
						<p class="muted">{{explanation}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>

<!-- js rule automatically appends "-form" to the ret attribute of the question type button list -->
<script type="text/x-handlebars-template" id="tbs-irs-form-form">
	<p class="lead"><span class="muted text-small">Task Based Simulation</span> Select Form Type</p>
			<div class="row">
				<div class="span5">

					{{#forms}}
						<div class="btn-group rcpa-group irs-form-page-btngroup" data-toggle="buttons-radio" id="irs-form-nav">
						
						<h4>{{name}}</h4>
						
						<div class="btn-group" data-toggle="buttons-radio">
						{{#pages}}
						<button type="button" class="btn btn-inverse" data-previewImage="{{previewImage}}" data-wizardID="{{wizardID}}">{{pageLabel}}</button>
						{{/pages}}
						</div>
						
					</div>
					{{/forms}}
				</div>
				<div class="span5" id="irsform-preview-img">
					<img src='img/question-types/multiple-choice.png' />
				</div>
			</div>
</script>

    
    <!--
    <script src="js/jquery.touchwipe.min.js"></script>
    -->

</body>
</html>
