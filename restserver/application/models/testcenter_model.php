<?php
class Testcenter_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function get_questions($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('multiple_choice_questions');
			return $query->result_array();
		}
		
		//$query = $this->db->get_where('news', array('slug' => $slug));
		//return $query->row_array();
	}
	public function get_questions_answers($id = 0)
	{
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->join('multiple_choice_answers', 'multiple_choice_answers.question_id = multiple_choice_questions.id');
		$this->db->where('multiple_choice_questions.id', $id);
		
		$query = $this->db->get();
		return $query->result_array();

	}
	//get topics and sub topics
	public function get_topics($section = 'far')
	{
		$this->db->select('*');
		$this->db->from('chapters');
		$this->db->join('topics', 'topics.chapter_id = chapters.id');
		$this->db->where('chapters.section', $section); 
		$this->db->order_by('chapters.id','topics.id');
		
		$query = $this->db->get();
		return $query->result_array();

	}
	//get student progress info
	public function get_student_info($student_id, $question_id=0)
	{
		$this->db->select('*');
		$this->db->from('student_progress');
		$this->db->join('student_progress_answers', 'student_progress.student_id = student_progress_answers.student_id');
		$this->db->where('student_progress_answers.question_id = student_progress.question_id'); 
		$this->db->where('student_progress.student_id', $student_id); 
		//if question id is passed
		if ($question_id != 0) {
			$this->db->where('student_progress_answers.question_id', $question_id); 
		}
		$this->db->order_by('student_progress_answers.question_id');		
		$query = $this->db->get();
		return $query->result_array();

	}
	
	/**
	 * insert record in student_progress table
	 * data array needs these paramenters
	 * 
	 * student_id - required
	 * question_id - required
	 * answer_id - required
	 * correct_answer -required
	 * notes
	 * bookmark - default false
	 */
	public function set_student_progress($data)
	{
		$this->db->insert('student_progress', $data); 
		return $data;

	}
	/**
	 * Insert the question and return the new ID
	 */
	 public function set_question($data){
	 	$this->db->insert('multiple_choice_questions', $data);
		//return the new question id
		$data['question_id']=$this->db->insert_id();
		return $data['question_id'];
		
	 }
	/**
	 * Insert the answer
	 */
	 public function set_answer($data){
	 	$this->db->insert('multiple_choice_answers', $data);
		//return the new question id
		$data['answer_id']=$this->db->insert_id();
		return $data;
		
	 }
	 /**
	 * Insert the correct answer
	 */
	 public function set_correct_answer($data, $id){
	 	$this->db->update('multiple_choice_questions', $data, "id = ".$id);
		return $data;
		
	 }
}