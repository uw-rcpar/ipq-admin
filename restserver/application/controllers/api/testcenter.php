<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Testcenter
 *
 * Main controler for test center api
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Testcenter extends REST_Controller
{
	    
    //get questions and answers
	function multiple_choice_get()
    {
        	
        if(!$this->get('id'))
        {
        	$this->response(NULL, 400);
        }
		
		$id=$this->get('id');
		$this->load->model('testcenter_model');
		$questions = $this->testcenter_model->get_questions_answers($id);
		
        if($questions)
        {
            	
			//format the db results into an array like views/topic.json
            $myquestions=array();
			$current_question=NULL;   
			
			$i=0;
            
            foreach ($questions as $question) {
            	
					
            	if ($current_question != $question['question_id']) {
					$myquestions['questions'][]=array(
						'question'=>$question['question'],
						'id'=>$question['question_id'],
						'chapter_id'=>$question['chapter_id'],
						'topic_id'=>$question['topic_id'],
						'col1_header'=>$question['col1_header'],
						'col2_header'=>$question['col2_header'],
						'col3_header'=>$question['col3_header'],
						'col4_header'=>$question['col4_header'],
						'answer_id'=>$question['answer_id'],
						'explanation'=>$question['explanation'],
						'video_id'=>$question['video_id'],
						'section'=>$question['section'],
						'difficulty'=>$question['difficulty']
						
						);						
					$current_question=$question['question_id'];
					//add in sub topics
					foreach($questions as $choice){
						if ($choice['question_id'] == intval($current_question)) {
							
							$myquestions['questions'][$i]['choices'][]=array('choice'=>$choice['choice'], 'id'=>$choice['id'], 'explanation'=>$choice['explanation'], 'is_correct'=>$choice['is_correct']);
							
						}
					}
					$i++;				
					
				}
			    
			}	
				
				
            $this->response($myquestions, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any questions!'), 404);
        }
    }
	
	//get topics and sub topics
	function chapters_get()
    {
        //we neede the session variable from the url controller/resource/paramenter/value
        //  /api/testcenter/topics/section/aud/format/json        
        if(!$this->get('section'))
        {
        	$this->response(NULL, 400);
        }
        
		$section=$this->get('section');
		$this->load->model('testcenter_model');
		$chapters = $this->testcenter_model->get_topics($section);
		$mytopics=array();
		
				
        if($chapters)
        {
            
           
           //format the db results into an array like views/topic.json
           
			$current_topic=NULL;   
			$mytopics['section']=$chapters[0]['section'];
			$i=0;
            
            foreach ($chapters as $chapter) {
            	
					
            	if ($current_topic != $chapter['chapter_id']) {
					$mytopics['chapters'][]=array('chapter'=>$chapter['chapter'],'id'=>$chapter['chapter_id']);						
					$current_topic=$chapter['chapter_id'];
					//add in sub topics
					foreach($chapters as $subtopic){
						if ($subtopic['chapter_id'] == intval($current_topic)) {
							
							$mytopics['chapters'][$i]['topics'][]=array('topic'=>$subtopic['topic'], 'id'=>$subtopic['id']);
							
						}
					}
					$i++;				
					
				}
			    
			}			
			 
            $this->response($mytopics, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any topics!'), 404);
        }
    }

	//get topics
	function topics_get()
    {
    	if(!$this->get('section'))
        {
        	$this->response(array('error' => 'No exam section passed. Use far, aud, reg or bec'), 400);
        }
        
		$section=$this->get('section');
		$this->load->model('testcenter_model');
		$topics = $this->testcenter_model->get_topics($section);
		$mytopics=array();
		
		if($topics)
        {
        	//var_dump($topics);
			//format the db results into an array like views/topic.json
           
			$current_topic=NULL;   
			$mytopics['section']=$topics[0]['section'];
			$i=0;
			foreach ($topics as $topic) {
				$mytopics['topic'][]=array($topic['chapter']=>$topic['chapter_id'],$topic['topic']=>$topic['id']);		
			}
			$this->response($mytopics, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any topics!'), 404);
		}
	}

	/**
	 * insert record in student_progress table
	 * data array needs these paramenters
	 * 
	 * student_id - required
	 * question_id - required
	 * answer_id - required
	 * correct_answer -required
	 * notes
	 * bookmark - default false
	 */
	function student_progress_post()
    {
    	//no id gets the ol heave ho	
    	/*
		if(!$this->get('id') && !$this->post('id'))
				{
					$this->response(NULL, 400);
				}*/
				
		
		$message = array('id' => $this->get('id'), 'notes' => $this->post('notes'), 'student_id' => $this->post('student_id'), 'message' => 'ADDED!');
		
		
        $this->response($message,200); 
		//insert
		if ($this->post('id')) {
			
			$data = array(
				'student_id' => $this->post('id'), 
				'question_id' => $this->post('question_id'), 
				'answer_id' => $this->post('answer_id'), 
				'correct_answer' => $this->post('correct_answer'),
				'notes' => $this->post('notes'),
				'bookmark' => $this->post('bookmark')
				);
				
			
			$this->load->model('testcenter_model');
			$result = $this->testcenter_model->set_student_progress($data);
			if($result) {
				$this->response($result, 200);
			} else {
				$this->response(array('error' => 'Insert failed!'), 400);
			}
		} else {
			$this->response(array('error' => 'no freakin post data!'), 400);
		}
	}

	function section_test_post(){
		
		if ($this->post('email')) {
			$result['sections'][]=array('far','aud','reg','bec');
			$result['sections'][]=array('success'=>'TRUE');
			$this->response($result, 200);
		} else {
			$this->response(array('error' => 'no email passed in post or some other error'), 400);
		}
	}

	function student_progress_get()
    {
    	if(!$this->get('student_id'))
        {
        	$this->response(NULL, 400);
        }
		//set a default question id to 0. Means it has not passed
		$question_id = (!$this->get('question_id') ? 0 : $this->get('question_id'));
		
		
		$student_id=$this->get('student_id');
		$this->load->model('testcenter_model');
		$student_info = $this->testcenter_model->get_student_info($student_id,$question_id);
		
		
		if($student_info)
        {
            	
			//format the db results into an array like views/topic.json
            $mystudent_info=array();
			$current_question=NULL;
			$mystudent_info['student_id']=$student_info[0]['student_id'];   
			
			$i=0;
            
            foreach ($student_info as $question) {
            	
					
            	if ($current_question != $question['question_id']) {
					$mystudent_info['questions'][]=array(
						'question_id'=>$question['question_id'],
						'correct_answer'=>$question['correct_answer'],
						'timer_preference'=>$question['timer_preference'],
						'notes'=>$question['notes'],
						'bookmark'=>$question['bookmark']						
						);						
					$current_question=$question['question_id'];
					//add in sub topics
					foreach($student_info as $answer){
						if ($answer['question_id'] == intval($current_question)) {
							
							$mystudent_info['questions'][$i]['answer_attempts'][]=array('answer_id'=>$answer['answer_id'], 'is_correct'=>$answer['is_correct'], 'seconds_to_complete'=>$answer['seconds_to_complete'], 'timestamp'=>$answer['timestamp']);
							
						}
					}
					$i++;				
					
				}
			    
			}	
				
				
            $this->response($mystudent_info, 200); // 200 being the HTTP response code
        }
		 
		 else {
			$this->response(array('error' => 'No student progress data for parameters passed'), 400);
		}
		
	}
	
	
	//Login 
	public function login_post()
	{
		if ($this->post('username')) {
			//hard code login for now
			$username=$this->post('username');
			$password=$this->post('password');
			
				
			//password is R0ckinR0g
			if ($username=='administrator' && $password=='R0ckinR0g') {
				$result=array('SUCCESS'=>1);
				$result['sections']=array('AUD','BEC','FAR','REG');			
				$this->response($result, 200);
			} else {
				$result=array('SUCCESS'=>0);
				$result['sections']=array('FAIL');
				$this->response($result, 200);
				//$this->response(array('error' => 'Incorrect username or password'), 400);
			}
			
		} else {
			$this->response(array('error' => 'no email passed in post or some other error'), 400);
		}
	}
	
	
	//save answer
	public function save_question_post()
	{
		if ($this->post('question')) {
			
			$question=$this->post('question');
			
			$json = json_decode($question, true);
			
			//print_r($json);
			//setup a new array for the insert
			$data=array();
			$data_choice=array();
			$this->load->model('testcenter_model');
			//multiple choice
			switch ($json['type']) {
			    case "multiple-choice":
					
					//set a new question row for each topic in the db
					foreach($json['topics'] as $topic) {
					    
						$data['topic_id']=$topic['topic'];
						$data['chapter_id']=$topic['chapter'];
						$data['question']=$json['question'];
						//$data['section']=$json['section'];
						$data['section']='aud';
						//set_question returns newly inserted id					
						$question_id=$this->testcenter_model->set_question($data);
				
						}
					
					//insert answers
					foreach($json['choices'] as $choice) {
						
						$data_choice['question_id']=$question_id;
						$data_choice['choice']=$choice['choice'];
						$data_choice['explanation']=$choice['explanation'];
						$data_choice['is_correct']=$choice['is_correct'];
						
						$answer=$this->testcenter_model->set_answer($data_choice);
						
						//if this is the correct answer update the questions table
						if ($data_choice['is_correct']==1) {
							$correct['answer_id']=$answer['answer_id'];
							$correct['explanation']=$data_choice['explanation'];
							$a=$this->testcenter_model->set_correct_answer($correct, $data_choice['question_id']);
							
						}
						
						}
						//print_r($data);
						$result=array('SUCCESS'=>1);
			        
			        break;
			   
			    //simulation
			    case "simulation":
			        
			        break;
			    
			}
			
			
			//$result['sections']=array('AUD','BEC','FAR','REG');
			
			$this->response($result, 200);
		} else {
			$this->response(array('error' => 'no question object in post or some other error'), 400);
		}
	}


	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		
	}
	
}