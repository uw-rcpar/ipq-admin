<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$title?></title>
	<link href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" media="screen">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
  	<div class="container">
    <ul class="nav nav-pills">
    	<a class="brand" href="#">RogerCPAReview</a>
      <li class="active" ><a href="#">New Question</a></li>
      <li><a href="#">Incomplete Questions</a></li>
      <li><a href="#">Search & Edit Questions</a></li>
      <li><a href="#">Questions</a></li>
    </ul>
	</div>
  </div>
</div>