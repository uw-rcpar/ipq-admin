<div class="container">


		

<div class="modal hide fade" id="modal-login" data-backdrop="static">
	<form class="form-signin">
  <div class="modal-header">
    <h3>Please Sign In</h3>
  </div>
  <div class="modal-body">
   
        
        <input type="text" class="input-block-level" placeholder="Email address" name="login_email">
        <input type="password" class="input-block-level" placeholder="Password"  name="login_password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
     
  </div>
  <div class="modal-footer">
    <button class="btn btn-large btn-primary" type="submit">Sign in</button>
  </div>
  </form>
</div><!-- end modal login -->


	
	<div class="row wizardstep" id="step1" >
		<span class="span8 well offset1">
			<p class="lead">Which course does your question belong to?</p>

		</span>
	</div>
	<div class="row wizardstep" id="step2" style="display:none;">
		<span class="span8 well offset1">
			<p class="lead">Step 2<br/>
				Select the topics relevant to your question.</p>
				<p>You may come back to this step later, however your question will not be complete until it has been tagged with at least one topic.</p>

		</span>
	</div>

	<div class="row wizardstep" id="step3" style="display:none;">
		<div class="span10 well offset1">
			<p class="lead">Select Question Type</p>
			<div class="row">
				<div class="span5">
					<div class="btn-group btn-group-vertical rcpa-group" data-toggle="buttons-radio" id="question-type-nav">
						<button type="button" class="btn btn-rcpa btn-large" rel="multiple-choice">Multiple Choice</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-research">TBS - Research</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-multiple-list">TBS - Multiple List</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-lit-lookup">TBS - Literature Lookup</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-written-communication">TBS - Written Communication</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-irs-form">TBS - IRS Form</button>
						<button type="button" class="btn btn-rcpa btn-large" rel="tbs-spreadsheet">TBS - Spreadsheet</button>
					</div>
				</div>
				<div class="span5" id="question-type-preview-image">
					<img src='<?=base_url()?>/assets/img/question-types/multiple-choice.png' />
				</div>
			</div>
		</div>
	</div>


	<div class="row wizardstep" id="step4" style="display:none;">
		<div class="span10 well">
			<p class="success">Your new question has been saved.</p>
			<p class="fail">An error has occurred please contact sam.</p>
		</div>
	</div>


	<div class="modal hide fade" id="answer-option-window">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <p class="lead">Question Option</p>
	  </div>
	  <div class="modal-body">
	  	<p class="muted lead">Text of an answer</p>
	    <textarea id="option-text">
	    </textarea>

	    <p class="muted lead" style="margin-top:25px;">Is this answer correct?</p>
	    <div class="btn-group" data-toggle="buttons-radio" id="option-correct-btns">
  		  	<button type="button" class="btn btn-primary active" data-val="0">Wrong</button>
			<button type="button" class="btn btn-primary" data-val="1">Correct</button>
		</div>

		<p class="muted lead">Explain why:</p>
		<textarea id="option-explanation-text" style="width:100%;height:120px;">
	    </textarea>

	  </div>
	  <div class="modal-footer">
	    <a href="#" class="btn">Close</a>
	    <a href="#" class="btn btn-primary" id="save-option-btn">Save option</a>
	  </div>
	</div>



	<div class="row wizardstep" id="step5" style="display:none;">
		<div class="span8 well">
			<p class="success">Your new question has been saved.</p>
			<p class="fail">An error has occurred please contact sam.</p>
			<div class="navbar">
			  <div class="navbar-inner">	
			    <ul class="nav">
			      <li><a href="#" rel="new-question">Create a New Question</a></li>
			      <li><a href="#" rel="start-over">Start Over</a></li>
			    </ul>
			  </div>
			</div>
		</div>
	</div>




	<div class="navbar navbar-inverse navbar-fixed-bottom">
  		<div class="navbar-inner">
  			<div class="container">
<div class="span3 offset4">
	<div class="pagination wizardprogress">
		  <ul>
		    <li><a href="#step1" rel="0">1</a></li>
		    <li class="disabled"><a href="#step2" rel="1">2</a></li>
		    <li class="disabled"><a href="#step3" rel="2">3</a></li>
		    <li class="disabled"><a href="#step4" rel="3">4</a></li>
		    <li class="disabled"><a href="#step5" rel="4">5</a></li>
		</ul>
	</div>
</div>

			    <ul class="nav nav-pills pull-right">
			      <li><button href="#next" class="btn btn-large NextButton">Next</button></li>
			  </ul>
			</div>
		</div>
	</div>








    </div>


    <script id="section-nav" type="text/x-handlebars-template">
    <div class="span5 offset2">
		<div class="btn-group btn-block" data-toggle="buttons-radio" id="pick-section-nav">
		{{#each sections}}
		  <button type="button" class="btn btn-primary btn-large" rel="{{this}}">{{this}}</button>
		 {{/each}}
		</div>
	</div>
	</script>




	<script id="topics-select"  type="text/x-handlebars-template">

	{{#each chapters}}
		<p class="lead">{{chapter}}</p>
		<div class="btn-group rcpa-group" data-toggle="buttons-checkbox">
		{{#each topics}}
  			<button type="button" class="btn btn-rcpa btn-block h-topic" id="topic_{{id}}" 
  								data-topic="{{id}}" data-chapter="{{../id}}" value="{{id}}">{{topic}}</button>
 		{{/each}}
		</div>
	{{/each}}
	</script>

<script id="multiple-choice-form"  type="text/x-handlebars-template">
<p class="lead">Multiple Choice Question</p>
<div class="row">
	<div class="span10">
	<textarea id="question-text" name="question-text" style="width:100%;height:240px"></textarea>
	</div>
</div>
<div class="row" style="padding-top:20px;">
	<div class="span8" id="explain-container">

	</div>
	<div class="span2">
		<button class="btn btn-primary" id="new-option-btn">Add New Option</button>
	</div>
</div>
</script>

<script id="multiple-choice-option"  type="text/x-handlebars-template">
	<div class="q-choice span6 well" data-id="{{id}}">
		<div class="row">
			<div class="span1">
			{{debug}}
				{{#if is_correct}}
					<img src="<?=base_url()?>/assets/img/is-correct.png" />
				{{else}}
					<img src="<?=base_url()?>/assets/img/is-wrong.png" />
				{{/if}}
			</div>
			<div class="span5">
				<div class="row">
					<p>{{choice}}</p>
				</div>
				<div class="row">
					<div class="span4 offset1">
						<p class="muted">{{explanation}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>