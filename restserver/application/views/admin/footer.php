<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script>
	//setup the base url for js paths
	var config = {
     	base: '<?=base_url()?>',
     	loginUrl:'<?=base_url()?>index.php/api/testcenter/login/format/json',
		topicsUrl:'<?=base_url()?>index.php/api/testcenter/chapters/format/json',
		saveQuestionUrl:'<?=base_url()?>index.php/api/testcenter/save_question/format/json'     
 		};
	</script>
	<script src="<?=base_url()?>assets/js/tinymce/tinymce.min.js"></script>
    <script src="<?=base_url()?>assets/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/handlebars.js"></script>
    <script src="<?=base_url()?>assets/js/adminapp.js"></script>
</body>
</html>