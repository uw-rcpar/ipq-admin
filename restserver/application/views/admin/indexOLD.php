<div id="content">
    <h1>Edit Questions</h1>
     <?php foreach ($questions as $question): ?>
    <section>
      <article>
        <h2><?php echo $question['question'] ?></h2>
        <p><?php echo $question['explanation'] ?> </p>
        <p>Correct answer: <?php echo $question['answer_id'] ?></p>
        <p>Exam Section: <?php echo $question['section'] ?></p>
      </article>      
    </section>
    <?php endforeach ?>
  </div>
  <!-- /end #content-->