-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 09, 2013 at 05:41 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testcenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter` varchar(300) NOT NULL,
  `section` varchar(4) NOT NULL COMMENT 'far,aud,reg,bec',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `chapter`, `section`) VALUES
(1, 'Audit Standards and Engagement Planning', 'aud'),
(2, 'Internal Control', 'aud'),
(3, 'Audit Sampling', 'aud'),
(4, 'Professional Responsibilities and Ethics', 'aud'),
(5, 'Audit Evidence', 'aud');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_answers`
--

CREATE TABLE IF NOT EXISTS `multiple_choice_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `choice` varchar(1000) NOT NULL,
  `explanation` varchar(500) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `multiple_choice_answers`
--

INSERT INTO `multiple_choice_answers` (`id`, `question_id`, `choice`, `explanation`, `is_correct`) VALUES
(1, 1, 'The disclosures provide reasonable assurance that the financial statements are free of material misstatement.', 'Explanation holding text 1', 0),
(2, 1, 'The auditor tested the overall internal control.', 'Explanation holding text 2', 0),
(3, 1, 'An audit includes evaluating the reasonableness of significant estimates made by management.', 'Explanation holding text 3', 1),
(4, 1, 'The financial statements are consistent with those of the prior period.', 'Explanation holding text 4', 0),
(5, 2, 'Intro (report on F/S)|Managements Responsibility', 'Explanation holding text 5', 0),
(6, 2, 'Management’s Responsibility|Auditor’s Responsibility', 'Explanation holding text 6', 0),
(7, 2, 'Auditor’s Responsibility|Opinion', 'Explanation holding text 7', 1),
(8, 2, 'Intro|Opinion', 'Explanation holding text 8', 0),
(9, 3, 'Report on the Financial Statements (Intro)', 'Explanation holding text 9', 0),
(10, 3, 'Management''s Responsibility for the Financial Statements', 'Explanation holding text 10', 0),
(11, 3, 'Auditor''s Responsibility', 'Explanation holding text 11', 1),
(12, 3, 'Opinion', 'Explanation holding text 12', 0),
(17, 11, '<p>10 pieces</p>', '<p>because its wrong</p>', 0),
(18, 11, '<p>15 pieces</p>', '<p>Because this is correct!</p>', 1),
(19, 12, '<p>nothing</p>', '<p>because it is wrong</p>', 0),
(20, 12, '<p>Everything</p>', '<p>because it is correct.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_questions`
--

CREATE TABLE IF NOT EXISTS `multiple_choice_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `col1_header` varchar(250) DEFAULT NULL,
  `col2_header` varchar(250) DEFAULT NULL,
  `col3_header` varchar(250) DEFAULT NULL,
  `col4_header` varchar(250) DEFAULT NULL,
  `answer_id` int(11) NOT NULL,
  `explanation` varchar(1000) NOT NULL,
  `section` varchar(4) NOT NULL COMMENT 'FAR, AUD, REG or BEC',
  `video_id` int(11) NOT NULL,
  `difficulty` int(11) NOT NULL COMMENT 'On a scale of 1 to 5, 1 being hardest',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `multiple_choice_questions`
--

INSERT INTO `multiple_choice_questions` (`id`, `chapter_id`, `topic_id`, `question`, `col1_header`, `col2_header`, `col3_header`, `col4_header`, `answer_id`, `explanation`, `section`, `video_id`, `difficulty`, `active`, `timestamp`) VALUES
(1, 1, 1, 'Which of the following statements is a basic element of the auditor''s standard report?', NULL, NULL, NULL, NULL, 3, 'In the Auditors responsibility paragraph, it mentions that and auditor evaluates the appropriateness of accounting policies used and the reasonableness of significant accounting estimates made by management.  The requirement is to identify the statement that is included in the auditor’s standard report. Answer (c) is correct because the auditor''s standard report states that an audit includes assessing significant estimates made by management; the other replies provide information not directly mentioned in a standard report. ', 'aud', 147, 1, 0, '0000-00-00 00:00:00'),
(2, 1, 1, 'Which paragraphs of an auditor''s standard report on financial statements of a nonissuer should refer to auditing standards generally accepted in the U.S. and Principles generally accepted in the U.S.?', 'Auditing standards generally accepted in the U.S.', 'Principles generally accepted in the U.S. Principles generally', NULL, NULL, 7, 'The basic unmodified audit report for a nonissuer consists of 4 paragraphs.  The Introductory or Report on the financial statements paragraph, management’s responsibility paragraph, auditor’s responsibility paragraph and the opinion paragraph. Reference to the auditing standards used is made in the Auditors responsibility paragraph and reference to accordance with the principles is made in the opinion paragraph. ', 'aud', 148, 1, 0, '0000-00-00 00:00:00'),
(3, 1, 1, 'In which paragraph of a standard unmodified report is a disclaimer on internal control mentioned?', NULL, NULL, NULL, NULL, 11, 'In the Auditor’s Responsibility paragraph, it is specifically mentioned that the auditor considers internal control relevant to the entity’s preparation and fair presentation of the financial statements in order to design audit procedures that are appropriate in the circumstances, but not for the purpose of expressing an opinion, and accordingly, we express no such opinion.', 'aud', 148, 1, 0, '0000-00-00 00:00:00'),
(11, 1, 1, '<p>How much wood would a whatever?</p>', NULL, NULL, NULL, NULL, 18, '', 'aud', 0, 0, 0, '2013-07-09 00:12:37'),
(12, 1, 5, '<p>What is up with you?</p>', NULL, NULL, NULL, NULL, 20, '<p>because it is correct.</p>', 'aud', 0, 0, 0, '2013-07-09 00:39:41');

-- --------------------------------------------------------

--
-- Table structure for table `student_progress`
--

CREATE TABLE IF NOT EXISTS `student_progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `correct_answer` tinyint(1) NOT NULL DEFAULT '0',
  `timer_preference` varchar(50) NOT NULL DEFAULT 'elapsed' COMMENT 'set to elapsed or countdown',
  `notes` text,
  `bookmark` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Used to track student progress through the test' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `student_progress`
--

INSERT INTO `student_progress` (`id`, `student_id`, `question_id`, `correct_answer`, `timer_preference`, `notes`, `bookmark`, `timestamp`) VALUES
(1, 12098, 1, 0, 'elapsed', 'This question was awesome and totally made my day.', 0, '2013-06-20 23:49:08'),
(2, 12098, 3, 1, 'elapsed', 'Nailed it', 0, '2013-06-19 16:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `student_progress_answers`
--

CREATE TABLE IF NOT EXISTS `student_progress_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `seconds_to_complete` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `student_progress_answers`
--

INSERT INTO `student_progress_answers` (`id`, `student_id`, `question_id`, `answer_id`, `is_correct`, `seconds_to_complete`, `timestamp`) VALUES
(1, 12098, 1, 2, 0, 10, '2013-06-20 23:44:33'),
(2, 12098, 1, 3, 1, 10, '2013-06-20 23:44:33'),
(3, 12098, 3, 7, 0, 5, '2013-06-20 23:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) NOT NULL COMMENT 'id from topic table',
  `topic` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `chapter_id`, `topic`) VALUES
(1, 1, 'Generally Accepted Auditing Standards'),
(2, 1, 'Steps in the Audit Process'),
(3, 1, 'Planning Procedures'),
(4, 1, 'Audit Risk'),
(5, 1, 'Quality Control'),
(6, 1, 'Research Task Format'),
(7, 2, 'Internal Control'),
(8, 2, 'Understanding the Internal Control Structure'),
(9, 2, 'Operating Cycles'),
(10, 2, 'Personnel and Payroll Cycle'),
(11, 2, 'Investing and Financing Cycle'),
(12, 2, 'Documentation of Internal Control Structure'),
(13, 2, 'Internal Control Reports and Communications'),
(14, 3, 'Audit Sampling'),
(15, 3, 'Variables Sampling'),
(16, 3, 'Probability Proportional to Size Sampling'),
(17, 4, 'Professional Responsibilities and Ethics'),
(18, 4, 'Sarbanes Oxley'),
(19, 4, 'International Ethics Standards for Accountants'),
(20, 5, 'Audit Evidence'),
(21, 5, 'Management’s Assertions'),
(22, 5, 'Analytical Procedures'),
(23, 5, 'Financial Statement Accounts. Cash, Recievables'),
(24, 5, 'Financial Statement Accoints. Investments and Marketable Securities'),
(25, 5, 'Financial Statement Accoints. Inventories'),
(26, 5, 'Financial Statement Accoints. PPE through Payrolll');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
