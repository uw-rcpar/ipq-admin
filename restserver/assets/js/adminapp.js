if(console == null) {
	console = {};
	console.log = function(){};
	console.dir = function(){};
}
AdminApp = function(){};
AdminApp.prototype = {
	sections:null,
	topics:null,
	user:null,
	//loginUrl:'http://localhost/restserver/index.php/api/testcenter/login/format/json',
	//topicsUrl:'http://localhost/restserver/index.php/api/testcenter/chapters/format/json',
	//saveQuestionUrl:'http://localhost/restserver/index.php/api/testcenter/save_question/format/json',
	loginUrl: config.loginUrl,
	topicsUrl: config.topicsUrl,
	saveQuestionUrl: config.saveQuestionUrl,
	question:null,
	examSection:null,

	nextStepEnabled:false,
	wizardSteps:null, //array of div containers
	currentStep:null, //the container of the current form of the wizard
};

AdminApp.prototype.init = function() {
	var self = this;
	if(self.user == null) {
		self.showLogin();
	}
	self.wizardSteps = $.makeArray($(".wizardstep"));
	self.currentStep = 0;
	$(".NextButton").on('click', function(e){
		if(self.nextStepEnabled == true) {
			self.gotoNextStepInWizard();
		}
	});
	$("#question-type-nav button").on('click', function(e) {
		self.question.model.type = $(this).attr('rel');
		switch(self.question.model.type) {
			case 'multiple-choice':
				$("#question-type-preview-image").html("<img src='"+config.base+"'/assets/img/question-types/multiple-choice.png' />").fadeIn(220);
				break;
			default:
				return;

		}
		self.renderQuestionForm();
	});
	$(".wizardprogress ul li a").on("click", function(e){
		var step = $(this).attr('rel');
		self.navigateToWizardStep(step);
	});
};



AdminApp.prototype.showLogin = function() {
	var self = this;
	$("#modal-login").modal();
	$("#modal-login .btn-primary").on('click', function(e) {
		e.preventDefault();
		self.doLogin(	$('[name=login_email]').val(), 
						$('[name=login_password]').val()	);
	});
};

AdminApp.prototype.doLogin = function($email, $password) {
	var self = this;
	$.ajax({
		type:'POST',
		url:self.loginUrl,
		//dataType:'json',
		data:{
			username:$email,
			password:$password
		},
		success:function($data) {
			//$data = $.parseJSON($data.responseText);
			console.log($data);
			if($data.SUCCESS == 1) {
				self.sections = $data.sections;
				self.user = $email;
				$('#modal-login').modal('hide');
				self.showSectionNav();
			} else {
				$("#modal-login .modal-header").addClass('btn-danger').text('Incorrect Email Address or Password');
			}
		},
		error: function (jqXHR,  textStatus, errorThrown) {
        	console.log(textStatus, errorThrown, jqXHR);
      	},
      complete: function (jqXHR, textStatus) {
      	console.log(textStatus);
      }
	});
};


AdminApp.prototype.showSectionNav = function() {
	var self = this;
	var source   = $("#section-nav").html();
	var template = Handlebars.compile(source);
	var html = template({sections:self.sections});
	$("#step1 .well").append(html);

	$("#pick-section-nav button").on('click', function(e) {
		self.pickExamSection( $(this).attr('rel') );
	});

	$("#pick-section-nav button").dblclick(function(e) {
		if(self.nextStepEnabled == true) {
			self.gotoNextStepInWizard();
		}
	});
};

AdminApp.prototype.pickExamSection = function($section) {
	var self = this;
	self.section = $section;
	self.question = new Question();
	self.question.init();
	self.question.model.section = self.examSection;
	self.nextStepEnabled = true;
	self.initTopicSelection();
};

AdminApp.prototype.initTopicSelection = function() {
	var self = this;
	$.ajax({
		type:'GET',
		url:self.topicsUrl,
		//dataType:'json',
		data:{
			section:self.section
		},
		success:function($data) {
			console.log($data);
			self.topics = $data;
			self.showTopics();
			
		},
		error: function (jqXHR,  textStatus, errorThrown) {
        	console.log(textStatus, errorThrown, jqXHR);
      	},
      	complete: function (jqXHR, textStatus) {
      		console.log(textStatus);
      	}
	});
};

AdminApp.prototype.showTopics = function() {
	var self = this;
	var source   = $("#topics-select").html();
	var template = Handlebars.compile(source);
	var html = template(self.topics);
	$("#step2 .well").append(html);

	$(".h-topic").on('click', function(e) {
		var check = $(this);
		var t = setTimeout(function() {
			if(check.hasClass('active')) {
				self.question.addTopic(check.data('topic'), check.data('chapter'));
			} else {
				self.question.removeTopic(check.data('topic'), check.data('chapter'));
			}
		}, 150);
	});
	$(".ch-topic").dblclick(function(e) {
		if(self.nextStepEnabled == true) {
			self.gotoNextStepInWizard();
		}
	});

};

AdminApp.prototype.gotoNextStepInWizard = function() {
	var self = this;
	$(".NextButton").blur();
	if(self.currentStep < self.wizardSteps.length) {
		$(self.wizardSteps[self.currentStep]).fadeOut(220, function() {
			self.currentStep++;
			for(var i = 0; i <= self.currentStep; i++) {
				$(".wizardprogress a[rel='" + i + "']").parent().removeClass("disabled");
			}
			$(self.wizardSteps[self.currentStep]).fadeIn(220, function() {
				if(self.currentStep == self.wizardSteps.length - 2) {
					self.swapNextButton();
				}
			});
		});
	}
};

AdminApp.prototype.navigateToWizardStep = function(step) {
	var self = this;
	
	$(self.wizardSteps[self.currentStep]).fadeOut(220, function() {
		self.currentStep = step;
		$(self.wizardSteps[self.currentStep]).fadeIn(220, function() {
			if(self.currentStep == self.wizardSteps.length - 2) {
				self.swapNextButton();
			} else {
				self.unSwapNextButton();
			}
		});
	});
};

AdminApp.prototype.swapNextButton = function() {
	var self = this;
	$(".NextButton").off("click");
	$(".NextButton").on("click", function(e){
		self.submitQuestion();
	});
	$(".NextButton").text("Save Question");
};

AdminApp.prototype.unSwapNextButton = function() {
	var self = this;
	$(".NextButton").off("click");
	$(".NextButton").on("click", function(e){
		self.gotoNextStepInWizard();
	});
	$(".NextButton").text("Next");

};

AdminApp.prototype.renderQuestionForm = function() {
	var self = this;
	var source   = $("#" + self.question.type + "-form").html();
	var template = Handlebars.compile(source);
	//var html = template('');
	$("#step4 .well").html(template);

	switch(self.question.type) {
		case "multiple-choice":
			self.initMultipleChoiceFormEvents();
			break;
	}


};
	
AdminApp.prototype.initMultipleChoiceFormEvents = function() {
	var self = this;
	self.isOptionCorrect = false;
	self.question.choices = [];
	$("#new-option-btn").on('click', function(e) {
		$("#answer-option-window").modal({backdrop:"static"});
		tinyMCE.get("option-text").setContent("");
		tinyMCE.get("option-explanation-text").setContent("");
		$("#save-option-btn").on('click', function(e){
			self.saveQuestionOption();
			$("#answer-option-window").modal('hide');
			$("#save-option-btn").off('click');
		});
	});
	tinymce.init({
	    selector: "textarea",
	    theme: "modern",
	    plugins: [
	        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	        "searchreplace wordcount visualblocks visualchars code fullscreen",
	        "insertdatetime media nonbreaking save table contextmenu directionality",
	        "emoticons template paste textcolor"
	    ],
	    statusbar:false,
	    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
	    image_advtab: true,
	    entity_encoding: "raw",
	    templates: [
	        {title: 'Test template 1', content: 'Test 1'},
	        {title: 'Test template 2', content: 'Test 2'}
	    ]
	});
};

AdminApp.prototype.saveQuestionOption = function() {
	var self = this;
	var option = { 	"choice" : tinyMCE.get('option-text').getContent(),
			"explanation" : tinyMCE.get('option-explanation-text').getContent(),
            "is_correct" : $("#option-correct-btns .active").data("val"),
            "id" : self.question.choices.length,
			};
	self.question.model.choices.push( option );
	self.showOption(option);
};

AdminApp.prototype.editQuestionOption = function(optionid) {

};

AdminApp.prototype.submitQuestion = function() {
	var self =  this;
	self.question.model.question = tinyMCE.get('question-text').getContent();
	var data = JSON.stringify(self.question.model);
	$.ajax({
		type:'POST',
		url:self.saveQuestionUrl,
		//dataType:'json',
		data:{
			question:data,
		},
		success:function($data) {
			console.log($data);
			$("#step5 .fail").hide();
			self.gotoNextStepInWizard();
			self.initEndFrameEvents();
		},
		error: function (jqXHR,  textStatus, errorThrown) {
        	console.log(textStatus, errorThrown, jqXHR);
        	$("#step5 .success").hide();
			gotoNextStepInWizard();
      	},
      	complete: function (jqXHR, textStatus) {
      		console.log(textStatus);
      	}
	});
};

AdminApp.prototype.initEndFrameEvents = function() {
	var self = this;
	self.unSwapNextButton();
	self.resetQuestion();
	$(".NextButton").hide();
	$("#step5 .well a").on("click", function(e){
		$(".NextButton").show();
		$("#step5 .well a").off("click");
		var cmd = $(this).attr("rel");
		if(cmd == "start-over") {
			self.doStartOver();
		} else {
			self.gotoSelectTopics();
		}
	});
};
AdminApp.prototype.resetQuestion = function() {
	var self = this;
	self.question = new Question();
	self.question.init();
	$(".wizardstep button.active").removeClass("active");

};	
AdminApp.prototype.doStartOver = function() {
	var self = this;
	for(var i = 1; i <= self.wizardSteps.length; i++) {
		$(".wizardprogress li a[rel='" + i + "']").parent().addClass("disabled");
	}

	$(self.wizardSteps[self.currentStep]).fadeOut(220, function() {
		self.currentStep = 0;
		$(self.wizardSteps[self.currentStep]).fadeIn(220);
	});
};

AdminApp.prototype.gotoSelectTopics = function() {
	var self = this;
	self.question.model.section = self.examSection;
	$(self.wizardSteps[self.currentStep]).fadeOut(220, function() {
		self.currentStep = 1;
		$(self.wizardSteps[self.currentStep]).fadeIn(220);
	});

};
AdminApp.prototype.showOption = function($option) {

	var source   = $("#multiple-choice-option").html();
	var template = Handlebars.compile(source);
	var html = template($option);
	$("#explain-container").append(html);
};

var Question = function(){};
Question.prototype = {
	section:null,
	type:"multiple-choice",
	topics:[],

};
Question.prototype.init = function($section) {
	this.model={};
	this.model.topics = [];
	this.model.section;
	this.model.choices = [];
}
Question.prototype.addTopic = function(topic, chapter) {
	this.model.topics.push({'topic':topic, 'chapter':chapter});
}

Question.prototype.removeTopic = function(topic, chapter) {
	var self = this;
	var len = self.model.topics.length;
	for(var i = 0; i < len; i++) {
		var obj = self.model.topics[i];
		if(obj.topic == topic && obj.chapter == chapter) {
			self.model.topics.splice(i, 1);
			break;
		}
	}
}


Handlebars.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);
 
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});

JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

window.app = null;
// Shorthand for $( document ).ready()
$(function() {
	window.app = new AdminApp();
	window.app.init();
});