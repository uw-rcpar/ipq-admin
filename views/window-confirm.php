<div class='modal-header clearfix'>
<p class="lead pull-left"  ng-bind-html-unsafe="confirmHeading">
</p>
	<button type="button" class="close pull-right" ng-click="cancel()"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body" ng-bind-html-unsafe="confirmMsg">


</div>
<div class='modal-footer'>
<button class="btn btn-inverse" ng-click="cancel()">Cancel</button>
<button class="btn btn-inverse" ng-click="confirm()">OK</button>
</div>