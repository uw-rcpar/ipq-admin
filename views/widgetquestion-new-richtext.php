 <div class="modal-header">
    <button type="button" class="close" ng-click="close()">&times;</button>
    <p class="lead">Add text block</p>
  </div>
  <div class="modal-body">
		    <textarea id="newTextBlockEditor" style="width:100%;height:120px;">
		    </textarea>
	</div>

  </div>
  <div class="modal-footer">
    <a class="btn" ng-click="close()">Close</a>
    <a class="btn btn-primary" id="save-option-btn" ng-click="saveAndClose()">Save text block</a>
  </div>