 <div class="modal-header">
  <div class="tabbable">
  <ul class="nav nav-tabs" >
  
    <li ng-class="{active: tabs.course == 'AUD'}" >
  		<a ng-click="pickSection('AUD')">AUD</a>
	</li>
   	<li ng-class="{active: tabs.course == 'BEC'}" >
  		<a ng-click="pickSection('BEC')">BEC</a>
	</li>
	<li ng-class="{active: tabs.course == 'FAR'}" >
  		<a ng-click="pickSection('FAR')">FAR</a>
	</li>
	<li ng-class="{active: tabs.course == 'REG'}" >
  		<a ng-click="pickSection('REG')">REG</a>
	</li>
	<li class="pull-right">
	<button type="button" class="close" ng-click="close()"><i class="fa fa-times"></i></button>
	</li>
  </div>


    
    
  </div>
  <div class="modal-body">
 

  <div class="quiz-pick-topics-container" ng-show="tabs.course == 'AUD'">
				<div class="pin-container">
					<div  ng-repeat="chapter in sections.AUD" class="pin">
						<h3 class="lead small"><i class="fa fa-angle-right" ></i> <span>{{chapter.chapter}}</span></h3>
						<div class="btn-group btn-group-vertical btn-block" id="chapter_{{chapter.id}}">
							<button ng-repeat="topic in chapter.topics" class="btn btn-block btn-check btn-inverse" ng-class="{active:topic.include == true}" ng-click="toggleTopic(topic)" >
								{{topic.topic}}</button>
							</div>
						</div>
					</div>
					</div>


	<div class="quiz-pick-topics-container" ng-show="tabs.course == 'BEC'">
     			<div class="pin-container">
					<div  ng-repeat="chapter in sections.BEC" class="pin">
						<h3 class="lead small"><i class="fa fa-angle-right"></i> <span>{{chapter.chapter}}</span></h3>
						<div class="btn-group btn-group-vertical btn-block" id="chapter_{{chapter.id}}">
							<button ng-repeat="topic in chapter.topics" class="btn btn-block btn-check btn-inverse" ng-class="{active:topic.include == true}" ng-click="toggleTopic(topic)" >
								{{topic.topic}}</button>
							</div>
						</div>
					</div>
				</div>
  	<div class="quiz-pick-topics-container" ng-show="tabs.course == 'FAR'">
     			<div class="pin-container">
					<div  ng-repeat="chapter in sections.FAR" class="pin">
						<h3 class="lead small"><i class="fa fa-angle-right"></i> <span>{{chapter.chapter}}</span></h3>
						<div class="btn-group btn-group-vertical btn-block" id="chapter_{{chapter.id}}">
							<button ng-repeat="topic in chapter.topics" class="btn btn-block btn-check btn-inverse" ng-class="{active:topic.include == true}" ng-click="toggleTopic(topic)" >
								{{topic.topic}}</button>
							</div>
						</div>
					</div>
				</div>

	<div class="quiz-pick-topics-container" ng-show="tabs.course == 'REG'">
     			<div class="pin-container">
					<div  ng-repeat="chapter in sections.REG" class="pin">
						<h3 class="lead small"><i class="fa fa-angle-right"></i> <span>{{chapter.chapter}}</span></h3>
						<div class="btn-group btn-group-vertical btn-block" id="chapter_{{chapter.id}}">
							<button ng-repeat="topic in chapter.topics" class="btn btn-block btn-check btn-inverse" ng-class="{active:topic.include == true}" ng-click="toggleTopic(topic)" >
								{{topic.topic}}</button>
							</div>
						</div>
					</div>
				</div>
			
  </div>
  <div class="modal-footer">
    <a class="btn btn-inverse" ng-click="close()">Close</a>
    <a class="btn btn-inverse" id="save-option-btn" ng-click="saveAndClose()">Save topics</a>
  </div>

