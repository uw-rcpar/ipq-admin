 <div> 
<div class="container" ng-show="feed.alerts.length > 0">
	<div class="span10">
		<div ng-repeat="alert in feed.alerts" class="alert {{alert.type}}">{{alert.message}}</div>
	</div>
</div>
 <div class="container" ng-show="feed.editQuestion != null">
 <div class="row">
	<div class="span10">
		<p class="lead">Edit multiple choice question #{{feed.editQuestion.id}}</p>
		<ul class="nav nav-pills" style="margin-bottom:0"><li><a ng-href="{{feed.editQuestion.document.url}}" target="_blank"><i class="icon-flag icon-red" ng-show="feed.editQuestion.document.isFlagged == '1'"></i><i class="icon-file"></i> {{feed.editQuestion.document.title}}</a></li>
		<li>
			<button class="btn btn-inverse pull-right" data-ng-click="launchInQuiz()">View question in quiz</button>
		</li>
		<li class="pull-right"><button class="btn btn-inverse pull-right" ng-click="feed.deleteQuestion()"><i class="icon-white icon-remove"></i> delete</button></li>
		</ul>
	</div>

</div>
<div class="row">
	<div class="span10">
	<textarea id="questionText" style="width:100%;height:240px"></textarea>

	</div>
</div>
<div class="row">
<div class="span10" style="padding-top:10px;">
<button class="btn btn-inverse pull-right" ng-click="showNewChoiceForm()"><i class="icon-white icon-plus"></i>Add choice</button>
</div>
</div>
<div class="row" style="padding-top:20px;">
	<div class="span8" id="explain-container">
		<div class="q-choice span9 well" ng-repeat="choice in question.choices | filter:{is_correct:'1'}">
			<div class="row">
				<div class="span1" ng-click="showChoiceEditForm(choice)">
					<img src="img/is-correct.png" />
				</div>
				<div class="span7" ng-click="showChoiceEditForm(choice)">
					<div class="row">
						<p ng-bind-html="choice.choice"></p>
					</div>
					<div class="row">
						<div class="span6 offset1">
							<p class="muted" ng-bind-html="choice.explanation"></p>
						</div>
					</div>
				</div>
				<ul class="nav nav-pills" style="margin-bottom:0">
					<li class="pull-right"><a ng-click="feed.deleteChoice(choice.id)"><i class="icon-remove"></i> delete</a></li>
				</ul>
			</div>
		</div>

		<div class="q-choice span9 well" ng-repeat="choice in question.choices | filter:{is_correct:'0'}">
			<div class="row">
				<div class="span1"  ng-click="showChoiceEditForm(choice)">
						<img src="img/is-wrong.png" />
				</div>
				<div class="span7"  ng-click="showChoiceEditForm(choice)">
					<div class="row">
						<p ng-bind-html="choice.choice"></p>
					</div>
					<div class="row">
						<div class="span6 offset1">
							<p class="muted" ng-bind-html="choice.explanation"></p>
						</div>
					</div>
				</div>
				<ul class="nav nav-pills" style="margin-bottom:0">
					<li class="pull-right"><a ng-click="feed.deleteChoice(choice.id)"><i class="icon-remove"></i> delete</a></li>
				</ul>
			</div>
		</div>

	</div>


</div><!-- /.container -->
</div>
<div class="navbar navbar-inverse navbar-fixed-bottom roger">
      <div class="navbar-inner">
        <div class="container">
        <div class="row">
        		        <div class="span5 pull-left">
          <div class="btn-group dropup">
                  <a class="btn btn-inverse" disabled="disabled">{{topics.course}}</a>
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown">Topics</a>
                  <ul class="dropdown-menu btn-inverse">
                    <li ng-repeat="topic in topics.questionTopics">
                      <a><i class="fa fa-times" ng-click="topics.removeTopic(topic)" tooltip="Remove this topic"></i>
                      {{topic.prefix}} : <strong>{{topic.topic}}</strong></a>
                    </li>
                    <li><a ng-click="topics.addTopic()" class="add-new"><i class="fa fa-plus"></i> Add topic</a></li>
                    
                   
                  </ul>
                </div>
        </div>


                <div class="span2 pull-right">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button class="NextButton pull-right btn btn-success" ng-click="updateQuestion()">
                      <i class="icon-ok" ng-show="isLoading == false"></i><i class="fa fa-cog fa-spin" ng-show="isLoading == true"></i> Update question</button>
                      </div>
			<div class="btn-group">
				<button class="NextButton btn" ng-hide="feed.editQuestion.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Disable</button>  
				<button class="NextButton btn" ng-show="feed.editQuestion.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Enable</button>  
			</div>
                    </div>
                </div>
              </div>
              
              </div>
            </div>
        </div>
    </div>
  </div>
</div>

