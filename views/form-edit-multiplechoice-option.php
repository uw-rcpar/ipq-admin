	  <div class="modal-header">
	    <button type="button" class="close" ng-click="closeOptionBox()">&times;</button>
	    <p class="lead">Question Option</p>
	  </div>
	  <div class="modal-body">
	  <div class="row">
	  	<div class="span4">
	  	<p class="muted lead">Text of an answer</p>
	  	</div>
	  </div>
	  <div class="row">
	  		<div class="span7" style="width:520px;">
	  		<!--
	  			<tiny-editor ng-model="feed.editChoice.choice"></tiny-editor>
	  			<textarea ui-tinymce ng-model="feed.editChoice.choice" id="choiceText">
			    </textarea>
	  		-->	
			    <textarea id="choiceText" style="width:100%;height:120px;">
			    </textarea>
			    
		    </div>
	  </div>
	<div class="row">
	    <div class="span2">
	    	<p class="muted lead" style="margin-top:25px;">This answer is</p>
	    </div>
	    <div class="span1" style="padding-top:25px;">
		    <div class="btn-group">
	  		  	<button type="button" class="btn btn-primary" ng-class="{active:choice.is_correct == '0' }" ng-click="setOptionIsCorrect('0')">Wrong</button>
				<button type="button" class="btn btn-primary" ng-class="{active:choice.is_correct == '1' }"  ng-click="setOptionIsCorrect('1')">Correct</button>
			</div>
		</div>
	</div>   
<div class="row">
	<div class="span7" style="width:520px;">
		<p class="muted lead">Explain why</p>
		<!--
		<tiny-editor ng-model="feed.editChoice.explanation"></tiny-editor>
		<textarea style="width:100%;height:120px;" ui-tinymce ng-model="feed.editChoice.explanation" id="explainText">
	    </textarea>
		 -->
		<textarea style="width:100%;height:120px;" id="explainText">
	    </textarea>
	   
	</div>
</div>
	  </div>
	  <div class="modal-footer">
	    <a class="btn" ng-click="closeOptionBox()">Close</a>
	    <a class="btn btn-primary" id="save-option-btn" ng-click="closeAndSaveOptionBox()">Save option</a>
	  </div>