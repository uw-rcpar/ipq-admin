 <div class="modal-header">
  <div class="tabbable" ng-show="isReady == true">
  <ul class="nav nav-tabs" >
  
    <li ng-class="{active: tabs.type == 'mcq'}" ng-show="results.mcq.length != null && results.mcq.length > 0">
  		<a ng-click="show('mcq')">Multiple Choice</a>
	</li>
   	<li ng-class="{active: tabs.type == 'tbs'}" ng-show="results.tbs.length != null && results.tbs.length > 0">
  		<a ng-click="show('tbs')">Journal</a>
	</li>
	<li ng-class="{active: tabs.type == 'research'}" ng-show="results.research.length != null && results.research.length > 0">
  		<a ng-click="show('research')">Research</a>
	</li>
	<li ng-class="{active: tabs.type == 'wc'}" ng-show="results.wc.length != null && results.wc.length > 0">
  		<a ng-click="show('wc')">Written Communication</a>
	</li>
	<li class="pull-right">
	<button type="button" class="close" ng-click="close()"><i class="fa fa-times"></i></button>
	</li>

  </ul>
  </div>
  <p class="lead" ng-show="isReady == false" style="color:#fff">Scanning the content database for your search terms...</p>
  </div>
  <div class="modal-body">

  <p class="text-center lead" ng-show="isReady == false"><i class="fa fa-gear fa-spin fa-4x"></i></p>

  <div ng-show="isReady == true">

   <div class="search-result-pane" ng-show="tabs.type == 'mcq'">

  <p class="lead">Multiple Choice</p>
  <p>Showing {{displayResults.length}} <span ng-show="displayResults.length > 1">results</span><span ng-show="displayResults.length == 1">result</span> for <small>{{query}}</small></p>

  <div ng-repeat="match in displayResults" ng-hide="match.exclude" class="search-result clearfix">
    <span class="qNumber">{{match.id}}</span>
    <a href="{{match.url}}" target="_blank" class="clearfix"><span ng-bind-html-unsafe="match.extract"></span></a>

  </div>


  </div>

 <div class="search-result-pane" ng-show="tabs.type == 'tbs'">

  <p class="lead">TBS Journal</p>
  <p>Showing {{displayResults.length}} <span ng-show="displayResults.length > 1">results</span><span ng-show="displayResults.length == 1">result</span> for <small>{{query}}</small></p>

  <div ng-repeat="match in displayResults" class="search-result clearfix">
    <span class="qNumber">{{match.id}}</span>
    <a href="{{match.url}}" target="_blank" class="clearfix">
    <span class="qType" ng-bind-html-unsafe="match.type"></span>
    <span ng-bind-html-unsafe="match.extract"></span></a>
  </div>


  </div>
 <div class="search-result-pane" ng-show="tabs.type == 'research'">

  <p class="lead">TBS Research</p>
  <p>Showing {{results.research.length}} <span ng-show="displayResults.length > 1">results</span><span ng-show="displayResults.length == 1">result</span> for <small>{{query}}</small></p>

  <div ng-repeat="match in displayResults" class="search-result clearfix">
    <span class="qNumber">{{match.id}}</span>
    <a href="{{match.url}}" target="_blank" class="clearfix">
    <span class="qType" ng-bind-html-unsafe="match.type"></span>
    <span ng-bind-html-unsafe="match.extract"></span></a>
  </div>

  </div>
 <div class="search-result-pane" ng-show="tabs.type == 'wc'">

  <p class="lead">Written Communication</p>
  <p>Showing  {{results.research.length}} <span ng-show="displayResults.length > 1">results</span><span ng-show="displayResults.length == 1">result</span> for <small>{{query}}</small></p>

    <div ng-repeat="match in displayResults" class="search-result clearfix">
    <span class="qNumber">{{match.id}}</span>
    <a href="{{match.url}}" target="_blank" class="clearfix">
    <span ng-bind-html-unsafe="match.extract"></span></a>
  </div>

  </div>

  </div>


  </div>
  <div class="modal-footer">
    <a class="btn btn-inverse" ng-click="close()">Close</a>

  </div>
