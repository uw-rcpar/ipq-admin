<?php
//no cookie, no showie. Cookie set on authorize.php
if (!$_COOKIE['isadmin']){
	//back to login
	header("Location: index.php");
	die();
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
  <!--jQuery References-->    

<!-- jQuery/jQueryUI-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jcanvas.min.js" type="text/javascript"></script>

    <!-- Angular (after jQuery)
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.js" type="text/javascript" ></script>
 -->
    <script src="js/angular.min.js"></script>
    <!-- Wijmo -->
    <link href="css/cobalt/jquery-wijmo.css" rel="stylesheet" title="cobalt-jqueryui" type="text/css" />
<!--
    <link href="css/wijmo/jquery.wijmo-open.3.20132.9.css" rel="stylesheet" type="text/css" />
    <link href="css/wijmo/jquery.wijmo-pro.3.20132.9.min.css" rel="stylesheet" type="text/css" /> -->
    <link href="css/wijmo/jquery.wijmo-pro.all.3.20132.9.min.css" rel="stylesheet" type="text/css" />


    <script src="js/wijmo/jquery.wijmo-open.all.3.20132.9.min.js" type="text/javascript"></script>
    <script src="js/wijmo/jquery.wijmo-pro.all.custom.3.20132.9.js" type="text/javascript"></script>
    <script src="js/wijmo/angular.wijmo.3.20132.9.min.js" type="text/javascript"></script>
   
    <!--
    <script src="js/wijmo/angular.wijmo.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/interop/angular.wijmo.3.20132.8.min.js" type="text/javascript"></script>
-->

 <script src="js/angular-sanitize.js"></script>


<script src="js/wijmo/jquery.wijmo.wijspread.all.1.20132.5.js" type="text/javascript"></script>
<link href="css/wijmo/jquery.wijmo.wijspread.1.20132.5.css" rel="stylesheet" type="text/css" />

 <!-- 

 -->

  <script src="js/tinymce4/tinymce.min.js"></script>
  
  
  <!--
  <script src="js/tinymce-angular.js"></script>
  -->
  <script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
  <script src="js/NavModule.js"></script>
  <script src="js/TopicsModule.js"></script>
  <script src="js/build-widget-module.js"></script>

  <script>
    window.topics = JSON.parse(decodeURIComponent($.cookie('tbs_topics')));
    
    angular.element(document).ready(function() {
      angular.bootstrap($("#navModule"), ["NavModule"]);
      angular.bootstrap($("#buildWidgetQuestionApp"), ["BuildWidgetQuestionModule"]);
    });
  </script>

   <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php include 'navbar.php'; ?>
<div id="buildWidgetQuestionApp">


    <div class="container"  ng-controller="WCQuestionController">

      <div class="row">
        <p class="lead">Written Communication Builder</p>

        
      </div>

      <div class="row" id="question-region">
        <div class="span9">
  
       
        </div>
      </div>
      
      <div class="navbar navbar-inverse navbar-fixed-bottom roger">
      <div class="navbar-inner">
        <div class="container">
        <div class="row">
                <div class="span2 pull-right">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button class="NextButton pull-right btn btn-success" ng-click="saveQuestion()"><span ng-show="question.isSaved == false">Save question</span>
                      <span ng-show="question.isSaved == true">Update question {{question.questionID}}</span>
                      </button>
                      </div>
                    </div>
                </div>
              </div>
              
              </div>
            </div>
        </div>
    </div> <!-- /.container -->
</div><!-- /.app -->

</body>
</html>