 <div class="container">
<!--
<div class="well span9 offset2 form-horizontal">
	<div class="control-group">
	  <div class="controls">
	    <div class="input-prepend">
	    	<span class="add-on"><i class="icon-search"></i></span>
	    	<input class="span2" id="sessionName" type="text" ng-model="sessionName">
	    </div>
	  </div>
	</div>
</div>
-->
<div class="row" ng-show="feed.lastUpdated != null">

<div class="alert alert-success span11">Question #{{feed.lastUpdated}} has been updated</div>

</div>

<div class="row" ng-show="feed.lastDeleted != null">

<div class="alert alert-info span11">Question #{{feed.lastDeleted}} has been <strong>deleted</strong></div>

</div>

<div class="row">
  <table class="table table-striped table-hover span12 question-list" ng-show="questions.length > 0" style="margin:auto;">
	  <thead>
	    <tr> 
	      <th>ID</th><th>Created</th><th>Updated</th><th>Question</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr ng-repeat="q in questions" ng-click="editQuestion(q)" ng-class="{lastUpdated: feed.lastUpdated == q.id}">
	      <td>{{q.id}}</td>
	      <td><span popover-placement="top" popover="{{q.createdBy}}" popover-trigger="mouseenter">{{q.createdOn}}</span></td>
	      <td><span popover-placement="top" popover="{{q.updatedBy}}" popover-trigger="mouseenter">{{q.updatedOn}}</span></td>
	      <td ng-bind-html="q.question"></td>
	    </tr>
	  </tbody>
  </table>
 </div>

 </div>