 <div class="modal-header">
    <button type="button" class="close" ng-click="close()">&times;</button>
    <p class="lead">Add Spreadsheet Widget</p>
  </div>
  <div class="modal-body">
  	
  <div class="nav nav-pills">
          <div class="btn-group">
            <a class="btn btn-pill" tooltip="Cell and text color" tooltip-placement="bottom" ng-click="setCellStyle(style.cellStyle)"><span class="{{style.cellStyle}}" style="font-family:{{style.font}}">Cell style</span></a>
            <a class="btn btn-pill dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li ng-repeat="style in cellStyles"><a  ng-click="setCellStyle(style)"><span class="{{style}}">Cell style</span></a></li>
            </ul>
          </div>

  
  	<div class="btn-group">
	  	<a class="btn btn-pill" tooltip="Align text left" tooltip-placement="bottom" tooltip-trigger="mouseenter" ng-click="alignleft()"><i class="icon-align-left"></i></a>
	  	<a class="btn btn-pill" tooltip="Align text center" tooltip-placement="bottom" tooltip-trigger="mouseenter" ng-click="aligncenter()"><i class="icon-align-center"></i></a>
	  	<a class="btn btn-pill" tooltip="Align text right" tooltip-placement="bottom" tooltip-trigger="mouseenter" ng-click="alignright()"><i class="icon-align-right"></i></a>
  	</div>

  	<div class="btn-group">
 	    <a class="btn btn-pill" tooltip="merge/unmerge cells" tooltip-placement="bottom" tooltip-trugger="mouseenter" ng-click="merge()"><i class="icon-merge"></i></a>
  	</div>



  	<div class="btn-group">
            <a class="btn btn-pill" ><i class="icon-text-height icon-white"></i> {{style.fontSize}}</a>
            <a class="btn btn-pill dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li ng-repeat="fontSize in fontSizes"><a  ng-click="setFontSize(fontSize)">{{fontSize}}</a></li>
            </ul>
          </div>

  		<div class="btn-group">
            <a class="btn btn-pill">{{style.font}}</a>
            <a class="btn btn-pill dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li ng-repeat="font in fontList | filter:filterCurrentFont"><a style="font-family:{{font}}" ng-click="setFont(font)">{{font}}</a></li>
            </ul>
          </div>
</div>


	<div id="spreadContainer">
	</div>

 	
</div>
  <div class="modal-footer">
    <a class="btn" ng-click="close()">Close</a>
    <a class="btn btn-primary" id="save-option-btn" ng-click="closeAndSave()">Save spreadsheet</a>
  </div>