 <div class="modal-header">
	<div class="tabbable">
	  <ul class="nav nav-tabs" >
	  
	    <li ng-repeat="key in answerKeys" ng-class="{active: tabs.key.model.name == key.model.name}" >
	  		<a ng-click="show(key)">{{key.model.name}}</a>
		</li>
	<!--	<li class="new-answer-key-btn">
		<a ng-click="newAnswerKey()"><i class="fa fa-plus"></i> New Answerkey</a>
		</li>
-->
	  
		<li class="pull-right">
		<button type="button" class="close" ng-click="close()"><i class="fa fa-times"></i></button>
		</li>

	  </ul>
	</div>
 </div>
 <div class="modal-body">

 <div class="answerkeys" ng-repeat="key in answerKeys" ng-show="tabs.key.model.name == key.model.name">
          <answerkey-gridblock-tabpane ng-model="key" parent-id="tab"></answerkey-gridblock-tabpane>
 </div>




 </div>
<div class="modal-footer">

    <a class="btn btn-inverse" ng-click="close()">Close</a>
    <a class="btn btn-inverse" ng-click="save()" ng-disabled="answer.answer == null"><span ng-show="answer.answer != null">Save <strong>{{answer.answer.list}} : {{answer.answer.letter}}</strong></span>
    <span ng-show="answer.answer == null">No answer selected</span></a>


</div>