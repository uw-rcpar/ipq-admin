<div class="modal-header">
<p class="lead"><span ng-show="result.matches.num_of_results > 1">Found {{result.matches.num_of_results}} potential matches.</span> 
<span ng-show="result.matches.num_of_results == 1">Found {{result.matches.num_of_results}} potential match.</span>
<span ng-show="result.matches.num_of_results == 0">Sorry, no luck...</span><button type="button" class="close" ng-click="close()"><i class="fa fa-times"></i></button></p>
  	
  	
</div>
  <div class="modal-body">
  <div ng-repeat="match in result.matches.qry_results" class="docmatch">
    <a href="{{match.url}}" target="_blank" class="btn btn-inverse btn-block btn-check"><i class="fa fa-cloud-download"></i> {{match.url}}</a>
    <div class="snippet">
      <p><span ng-bind-html-unsafe="match.fulltxt"></span></p>
       
    </div>
      <div class="docmatch-toolbar clearfix">
       <a ng-click="assignDocument(match.url)" class="btn btn-inverse pull-right" ng-class="{active:assign.pickedDocument == match.url}"><i class="fa fa-link"></i> Assign</a>
        </div>
    </div>

  </div>

  <div class="modal-footer">

  <button class="btn btn-inverse" ng-click="close()">Close</button>
  <button class="btn btn-inverse" ng-click="save()" ng-disabled="assign.pickedDocument == null">
    <span ng-show="assign.pickedDocument == null">Assign document</span>
    <span ng-show="assign.pickedDocument != null">Assign {{assign.title}} to this question</span>
  </button>
  </div>