 <div class="modal-header">
    <button type="button" class="close" ng-click="close()">&times;</button>
    <p class="lead">Add answer key</p>
  </div>
  <div class="modal-body">
  		<div class="control-group" style="padding-top:4px">
				<div class="controls">
						<input class="span3 input-small" type="text" placeholder="Label for links to this answer key" ng-model="answerkeyName"  tooltip-placement="bottom" tooltip="This is the label that will appear on buttons to make this answer key pop up." tooltip-trigger="focus">
					
				</div>
			</div>
  		<form novalidate class="form-horizontal">
	  		<div class="control-group" style="padding-top:4px">
				<div class="controls">
						<input class="span3 input-small" type="text" placeholder="Answer text" ng-model="newAnswer.text"  tooltip-placement="bottom" tooltip="Text of answer" tooltip-trigger="focus">
		
					<button class="btn btn-info" ng-click="addAnswer()"  tooltip-placement="bottom" tooltip="Add answer to key" tooltip-trigger="mouseenter" ><i class="icon-plus-sign icon-white"></i></button>
				</div>
			</div>
		</form>
	<div class="qgrid-container">
		<wij-grid data="model.model.newAnswerKey.answers" allow-editing="true" id="answerKeyGrid" columns-autogeneration-mode="none">
			<columns>
				<column data-key="answer" header-text="Answers"></column>
			</columns>
		</wij-grid>

	</div>
  </div>

  <div class="modal-footer">
    <a class="btn" ng-click="close()">Close</a>
    <a class="btn btn-primary" ng-click="saveAndClose()">Save Answer Key</a>
  </div>