 <div class="modal-header">
    <button type="button" class="close" ng-click="close()">&times;</button>
    <p class="lead">Add Interactive Table</p>
    <div class="clearfix">
    <div class="btn-group pull-left">
						<button class="btn btn-mini btn-inverse" ng-class="{active:selectMode == 'singleRange'}"  ng-click="setSelectMode('singleRange')">cells</button>
						<button class="btn btn-mini btn-inverse" ng-class="{active:selectMode == 'singleRow'}" ng-click="setSelectMode('singleRow')">row</button>
						<button class="btn btn-mini btn-inverse" ng-class="{active:selectMode == 'singleColumn'}"  ng-click="setSelectMode('singleColumn')">column</button>
					</div>
					<div class="btn-group pull-left">
						<button class="btn btn-mini btn-inverse" ng-click="styleTextAlign('left')"><i class="icon-white icon-align-left"></i></button>
						<button class="btn btn-mini btn-inverse" ng-click="styleTextAlign('center')"><i class="icon-white icon-align-center"></i></button>
						<button class="btn btn-mini btn-inverse" ng-click="styleTextAlign('right')"><i class="icon-white icon-align-right"></i></button>
					</div>
					<div class="btn-group pull-left">
						<a class="btn btn-inverse btn-mini" ng-click="styleBoldToggle()"><i class="icon-white icon-bold"> </i></a>
						<a class="btn btn-mini btn-inverse dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						 <ul class="dropdown-menu">
			              <li ng-repeat="weight in fontWeights"><a ng-click="setStyleBold(weight)">{{weight}}</a></li>

			            </ul>
					</div>
					<div class="btn-group pull-left">    <a class="btn btn-inverse btn-mini"><i class="icon-text-height icon-white"></i> </a>
			            <a class="btn btn-inverse btn-mini dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			            <ul class="dropdown-menu">
			              <li ng-repeat="fontSize in fontSizes"><a  ng-click="setFontSize(fontSize)">{{fontSize}}</a></li>

			            </ul>

			          </div>
			        <div class="btn-group pull-left">
			            <a class="btn btn-inverse btn-mini" ng-click="setCellStyle(style.cellStyle)"><span>Cell style</span></a>
			            <a class="btn btn-inverse btn-mini dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			            <ul class="dropdown-menu">
			              <li ng-repeat="style in cellStyles"><a  ng-click="setCellColors(style)"><span class="{{style}}">style</span></a></li>
			            </ul>
			          </div>
			  		<div class="btn-group pull-left">
			            <a class="btn btn-mini btn-inverse">Font</a>
			            <a class="btn btn-mini btn-inverse dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			            <ul class="dropdown-menu">
			              <li ng-repeat="font in fontList"><a style="font-family:{{font}}" ng-click="setFont(font)">{{font}}</a></li>
			            </ul>
			        </div>

			        <div class="btn-group pull-left">
						<a class="btn btn-mini btn-inverse" ng-click="addRichtext()" tooltip="Add rich text to selected cell"><i class="fa fa-code"></i></a>

			   		</div>
			   		<div class="btn-group pull-left">
						<a class="btn btn-mini btn-inverse" ng-click="setAnswerCell()"><i class="fa fa-link"></i> Set answer cell</a>

			   		</div>
		</div>
  </div>
<div class="modal-body" id="questionGridWindow">
			

		<div class="gridInput">
		<table id="questionGrid">
		</table>
		</div>
</div>

<div class="modal-footer">
<div class="table-size-controls clearfix" style="margin-left:0;width:480px;float:left">
	<div class="alert clearfix" style="margin-bottom:0px">

		<div class="table-size-controls  pull-left clearfix" style="margin-bottom:0px">
	  			<form  class="form-horizontal pull-left">
					<div class="controls pull-left">
						<div class="input-prepend ">
							<span class="add-on"><i class="icon-resize-horizontal"></i></span>
							<input class="span1 input-mini" type="number" step="1" placeholder="Columns" ng-model="resize.cols">
						</div>

						<div class="input-prepend">
							<span class="add-on"><i class="icon-resize-vertical"></i></span>
							<input class="span1  input-mini" type="number" step="1" placeholder="Rows" ng-model="resize.rows">
						</div>
						<button class="btn btn-link" ng-click="resizeGrid(resize.rows, resize.cols)"  tooltip-placement="bottom" tooltip="Resize grid" tooltip-trigger="mouseenter" ><i class="fa fa-refresh"></i></button>
					</div>
				</form>
			</div>
	</div>
</div>


    <a class="btn btn-inverse" ng-click="close()">Close</a>
    <a class="btn btn-inverse" id="save-option-btn" ng-click="saveAndClose()">Save grid</a>
  </div>