<form>
<div class="modal-header">
	<button type="button" class="close" ng-click="closeOptionBox()"><i class="icon-white icon-remove"></i></button>
	    <p class="lead">Report Document Issue</p>
	  </div>
	  <div class="modal-body">
	  

		<p class="lead">Describe the problem, including the page number for</p>
		<p>{{currentDocument.oldTitle}}</p>
		<p class="small">Also copy and paste the text of the question and answers</p>

		<textarea style="width:100%;height:120px;" id="reportText">
	    </textarea>
	   
	    <div class="control-group">
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox" value="true" ng-model="currentDocument.report.doCheckIn">
                Check this document back in
              </label>
            </div>
          </div>
          <p class="muted">Check this box if you cannot continue working on this document so that you can begin work on another.</p>
		</div>
</div>
	  </div>
	  <div class="modal-footer">
	    <a class="btn" ng-click="closeOptionBox()">Close</a>
	    <a class="btn btn-primary" id="save-option-btn" ng-click="saveReport()">Save report</a>
	  </div>
	  </form>