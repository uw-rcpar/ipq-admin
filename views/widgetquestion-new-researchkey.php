 <div class="modal-header">
    <button type="button" class="close" ng-click="close()">&times;</button>
    <p class="lead">Add Research Answer Thingie</p>
  </div>
  <div class="modal-body">

	<div>
		  	<div class="btn-group" data-toggle="buttons-radio">
							<button class="btn btn-mini btn-inverse"  
									ng-repeat="research in researchTypes" 
									ng-class="{active:pickedType.type == research.type}" 
									ng-click="setType(research)">{{research.type}}</button>
			</div>
	</div>

	<div  class="input-prepend">
		<span class="add-on">{{pickedType.type}}</span>
		<input ng-model="mask" type="text" placeholder="xxx-xxx-xxx-xxx">
	</div>
		<p class="muted">Seperate field with a dash, e.g. <strong>325-20-35-1</strong></p>
	

  </div>

  <div class="modal-footer">
    <a class="btn btn-inverse" ng-click="close()">Close</a>
    <a class="btn btn-inverse" ng-click="saveAndClose()">Save Answer Key</a>
  </div>