<?php
//no cookie, no showie. Cookie set on resterver admin_login_post()
if (!$_COOKIE['isadmin']){
	//back to login
	header("Location: index.php");
	die();
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
  
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script src="js/angular.min.js"></script>
  <script src="js/tinymce4/tinymce.min.js"></script>
  <script src="js/masonry.js"></script>
  <script src="js/angular-sanitize.js"></script>
  <script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
  <script src="js/NavModule.js"></script>
  <script src="js/TopicsModule.js"></script>
  <script src="js/search-edit-module.js"></script>
  <script>
    angular.element(document).ready(function() {
      angular.bootstrap($("#navModule"), ["NavModule"]);
      angular.bootstrap($("#browseSearchApp"), ["BrowseSearchModule"]);
    });
  </script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
</head>
<body>
<?php include 'navbar.php'; ?>
<div id="browseSearchApp">
    <ng-view>

    </ng-view>
</div>



</body>
</html>
