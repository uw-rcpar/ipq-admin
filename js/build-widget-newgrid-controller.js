BuildWidgetQuestionModule.controller('AddGridRichtextController',  function($scope, $timeout, dialog, cellContent) {
	$scope.dialog = dialog;
/* Add grid text editor */
	$timeout(function(){
		tinymce.init({
			    selector: "#newText",
			    theme: "modern",
			    plugins: [
			        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
			        "searchreplace wordcount visualblocks visualchars code fullscreen",
			        "insertdatetime media nonbreaking save table contextmenu directionality",
			        "emoticons template paste textcolor jbimages"
			    ],
			    statusbar:false,
			    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons jbimages",
			    image_advtab: true,
			    entity_encoding: "raw",
			    setup:function(ed) {		
			    	ed.on("init", function(){
						$scope.qText = ed;
						$scope.qText.setContent(cellContent);
			    	});
			    }
			    
			});
		});
	$scope.close = function() {
		$scope.dialog.close();
	}

	$scope.save = function() {
		$scope.dialog.close($scope.qText.getContent());
	}
});

BuildWidgetQuestionModule.controller("AnswerKeysWindowController", function($scope, $timeout, dialog, answer, WidgetQuestion) {
	$scope.answerKeys = WidgetQuestion.model.question.answerkeys;
	if($scope.answerKeys.length > 0 && answer == null) {
		$scope.tabs = {key:$scope.answerKeys[0]};
	} else {
		$scope.tabs = {key:null};
	}
	$scope.answer = {};

	
	$scope.show = function(key) {
		$scope.tabs.key = key;
		$timeout(function() {
			$scope.$broadcast("show-answerkey-tab", {key:key.model.name, answer:$scope.answer.answer});
		});
	};

	if(answer != null) {
		$scope.answer.answer = answer;
		angular.forEach($scope.answerKeys, function(key) {
			if(key.model.id == answer.keyID) {
				$scope.show(key);
			}

		});
		
	}

	$scope.$on("picked-answer", function($event, answer) {
		answer.list = $scope.tabs.key.model.name;
		$scope.answer.answer = answer;
		$scope.answer.answer.keyID = $scope.tabs.key.model.id;

		$scope.$digest();
	});

	$scope.close = function() {
		dialog.close();
	};

	$scope.save = function() {
		WidgetQuestion.model.question.answerkeys = $scope.answerKeys;
		dialog.close($scope.answer.answer);
	}

	//{ type: "answerKey", "model":{"content":gridData, id:"aKey_" + WidgetQuestion.model.question.answerkeys.length, name:$scope.answerkeyName}, order:WidgetQuestion.model.question.answerkeys.length }	
});
BuildWidgetQuestionModule.controller("NewGridController", function($scope, $timeout, dialog, $dialog, WidgetQuestion, gridWidget) {
	$scope.dialog = dialog;
	$scope.gridWidget = gridWidget;
	$scope.model = WidgetQuestion;
	$scope.columnsizes = {};
	$scope.resize = {};
	$scope.resize.rows = gridWidget.model.content.length;
	var colCount = 0;
	for(var k in gridWidget.model.content[0]) {
		colCount++;
	}
	$scope.resize.cols = colCount;
	$scope.fontList = [
				"Arial",
				"Courier New",
				"Times New Roman",
				"Verdana",
				"Tahoma"
				];

	$scope.fontSizes = [  "10px", "12px", "14px", "16px", "18px", "20px", "22px", "24px" ];

	$scope.fontWeights = [   "400", "700" ];

	$scope.cellStyles = [
		"whiteCell",
		"redCell",
		"greenCell",
		"blueCell",
		"blackCell"
	];

	$scope.selectMode = 'singleRange';

	$scope.setSelectMode = function(mode){
		$scope.selectMode = mode;
		$scope.grid.wijgrid("option", "selectionMode", mode);
	};
	
	$scope.setRowAsHeader = function() {
		var selection = $scope.grid.selection();
		var selectedCells = selectionObj.selectedCells();
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			console.dir(selectedCells.item(i));
		}
	};

	$scope.style = {};


	$scope.initAnswerButtons = function() {
		if($scope.grid != null) {
			$("#questionGrid").wijgrid("destroy");
		}
			$scope.grid = $("#questionGrid").wijgrid({ 	data:$scope.gridWidget.model.content,
 														columns:$scope.gridWidget.model.columns,
														currentCellChanging:$scope.currentCellChanging, 
														allowColSizing:true, 
														columnResizing:$scope.saveColumnSize,
														cellStyleFormatter:$scope.cellStyleFormatter, 
														selectionMode:$scope.selectMode, 
														ensureControl:false,
														allowEditing:true, 
														highlightOnHover:false}); 
 			

		$timeout(function() {
			var links = $("#questionGrid a.button.answerlink").toArray();
			for(var i = 0; i < links.length; i++) {
				var inner = $(links[i]).html();
				$(links[i]).html(inner.split(" :")[0]);
				$(links[i]).attr("id", "link_" + i);
			}
				
			$("#questionGrid button.btn.answerlink").on("click", function(e) {
				e.preventDefault();
				e.stopPropagation();
				var answerButton = $(this);
		        var scope = angular.element("#questionGridWindow").scope();
				scope.openAnswerKeyWindow( answerButton );
			});
		});
	};
	$scope.getAnswerkeyRow = function(key, row) {
		var answer = null;
		for(var i = 0; i < WidgetQuestion.model.question.answerkeys.length; i++) {
			var a = WidgetQuestion.model.question.answerkeys[i];
			if(a.model.id == key) {
				answer = angular.copy(a.model.content[row]);
				answer.keyID = a.model.id;
				answer.list = a.model.name;
			}
		}
		return answer;
	}
	$scope.setAnswerCell = function() {
		var ccell = $("#questionGrid").wijgrid('currentCell');
		$scope.answerCell = {row:ccell._ri, col:ccell._cia};
		$scope.gridWidget.model.content[ccell._ri]["col_"+ccell._cia] = "<button class='btn btn-info answerlink' data-list='' data-answer='' ng-click='clickAnswerKeyLink()' data-row='" + $scope.answerCell.row + "' data-col='" + $scope.answerCell.col + "'>Not set</button>";
		$scope.initAnswerButtons();
	};

	$scope.openAnswerKeyWindow = function(answerButton) {
		var answerRow = $scope.getAnswerkeyRow(answerButton.data("list"), answerButton.data("answer"));
		if(answerRow != null) {
			answerRow.rowIndex = answerButton.data("answer");
		}
		$timeout(function() {
			var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/widgetquestion-answerkeys-window.php',
			    controller: 'AnswerKeysWindowController',
			    dialogClass: 'modal answerWindow',
			    resolve:{
			    	answer:function (){
						return answerRow;
			    	}
			    }
			    
			};
		    var d = $dialog.dialog(opts);
	    
		    d.open().then(function(answer) {
		    	if(answer != null) {
		    		var row = answerButton.data("row");
		    		var col = answerButton.data("col");
		    		//var ccell = $("#questionGrid").wijgrid('currentCell');
		    		var  btn = "<button class='btn btn-info answerlink' data-list='" + answer.keyID + "' data-answer='" + answer.rowIndex + "' ng-click='clickAnswerKeyLink()'  data-row='" + row + "' data-col='" + col + "'> " + answer.list + " : " + answer.letter + "</button>";
		    		$scope.gridWidget.model.content[row][col] =  btn;
					$scope.initAnswerButtons();
		 		} else {

		 		}

		    });
		});

	};

	$scope.addRichtext = function() {
		var ccell = $scope.grid.wijgrid('currentCell');
		var cellContent = $scope.gridWidget.model.content[ccell._ri]["col_"+ccell._cia];

		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/widgetquestion-grid-richtext-window.php',
		    controller: 'AddGridRichtextController',
		    dialogClass: 'modal',
		    resolve:{
		    	cellContent:function() {
		    		return cellContent;
		    	}
		    }
		  };
	    var d = $dialog.dialog(opts);
	    d.open().then(function(richtext) {
	    	if(richtext != null && richtext != "") {
		    	var ccell = $("#questionGrid").wijgrid('currentCell');
				$scope.gridWidget.model.content[ccell._ri]["col_"+ccell._cia] = richtext;
				$("#questionGrid").wijgrid("destroy");
		 		 $scope.grid = $("#questionGrid").wijgrid({  data:$scope.gridWidget.model.content, 
																columns:$scope.gridWidget.model.columns,
																currentCellChanging:$scope.currentCellChanging, 
																allowColSizing:true, 
																columnResizing:$scope.saveColumnSize,
																cellStyleFormatter:$scope.cellStyleFormatter, 
																selectionMode:$scope.selectMode, 
																ensureControl:false,
																allowEditing:true, 
																highlightOnHover:false}); 
	    	}
	    });
	};

	$scope.styleTextAlign = function(val) {
		if($scope.style == null) {
			$scope.style = {};
		}
		$scope.style["textAlign"] = val;
		var selection = $scope.grid.wijgrid("selection");
		var selectedCells = selection.selectedCells();
		//$("#questionGrid").wijgrid("ensureControl", false);
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.css["text"] = {"text-align":val};
		}
		$("#questionGrid").wijgrid("ensureControl", false);
	};

	$scope.setFont = function(val) {
		if($scope.style == null) {
			$scope.style = {};
		}
		$scope.style["font"] = val;

		var selection = $("#questionGrid").wijgrid("selection");
		var selectedCells = selection.selectedCells();
		//$("#questionGrid").wijgrid("ensureControl", false);
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.css["font"] = {"font-family":val};
			cellStyle.key = "font";
		}
		$("#questionGrid").wijgrid("ensureControl", false);
	}
	$scope.setFontSize = function(val) {
		if($scope.style == null) {
			$scope.style = {};
		}
		$scope.style["fontSize"] = val;
		var selection = $("#questionGrid").wijgrid("selection");
		var selectedCells = selection.selectedCells();
		//$("#questionGrid").wijgrid("ensureControl", false);
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.css["fontSize"] = {"font-size":val};
			cellStyle.css["lineHeight"] = {"line-height":val};
			cellStyle.key = "fontSize";
		}
		$("#questionGrid").wijgrid("ensureControl", false);
	}

	$scope.setCellColors = function(cStyle) {
		if($scope.style == null) {
			$scope.style = {};
		}
		var bgColor = "#ffffff";
		var txtColor = "#000000";
		$scope.style.cellStyle = cStyle;
		switch(cStyle) {
			case "redCell" :
				bgColor = "#F81B05";
				txtColor = "#ffffff";
				break;
			case "blueCell":
				bgColor = "#3EC3F5";
				txtColor = "#ffffff";
				break;
			case "greenCell" :
				bgColor = "#2CA322";
				txtColor = "#ffffff";
				break;
			case "whiteCell" :
				bgColor = "#ffffff";
				txtColor = "#000000";
				break;
			case "blackCell":
					bgColor = "#000000";
					txtColor = "#ffffff";
				break;
			}
		$scope.style.colorStyle = cStyle;
		var selection = $("#questionGrid").wijgrid("selection");
		var selectedCells = selection.selectedCells();
		//$("#questionGrid").wijgrid("ensureControl", false);
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.css["textColor"] = {"color":txtColor};
			cellStyle.css["backgroundColor"] = {"background-color":bgColor};
			cellStyle.key = "cellColors";
		}
		$("#questionGrid").wijgrid("ensureControl", false);

	};
	$scope.setStyleBold = function(weight) {

		var selection = $("#questionGrid").wijgrid("selection");
		var selectedCells = selection.selectedCells();
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.key = "fontWeight";
			cellStyle.css["fontWeight"] = {"font-weight" : weight};
		}
		$("#questionGrid").wijgrid("ensureControl", false);

	};
	$scope.styleBoldToggle = function() {
		var boldRules = $scope.queryCSS("fontWeight");
		var found = false;
		angular.forEach(boldRules, function(rule) {
			if(rule.value != "800") {
				rule.value = "800";
				found = true;
			} else {
				rule.value = "400";
				found = true;
			}
		});

		if(!found) {
			$scope.declareCSS("font-weight", "800", "fontWeight");
			
		}

		var selection = $("#questionGrid").wijgrid("selection");
		var selectedCells = selection.selectedCells();
		for (var i = 0, len = selectedCells.length(); i < len; i++) {
			var cellStyle = $scope.getDictionaryEntryBySelection(selectedCells.item(i));
			cellStyle.key = "fontWeight";
			cellStyle.css["fontWeight"] = {"font-weight" : "700"};
		}
		$("#questionGrid").wijgrid("ensureControl", false);
	}

	$scope.declareCSS = function(rule, value, name) {
		if(name == null) {
			var styleLen = 0;
			styleLen = angular.forEach($scope.style, function(rule) {
				angular.forEach(rule.css, function(css) {
					return ++styleLen;
				});
			});
			name = "style_" + styleLen;
		}
		$scope.style[name] = value;
	}

	$scope.queryCSS = function(rule) {
		var matches = [];
		styleLen = angular.forEach($scope.style, function(rule) {
				angular.forEach(rule.css, function(css) {
						if(rule[css] != null) {
							matches.push({rule:rule, value:rule[css], source:rule.key});
						}
				});
			});
		return matches;
	};

	$scope.updateRule = function(dirtyRule, remove) {
		angular.forEach(style, function(rule) {
			angular.forEach(rule.css, function(css) {
						if(css != null) {
							matches.push({rule:rule, value:css[rule], source:rule});
						}
				});
		});
	};

	$scope.getDictionaryEntryBySelection = function(cell) {
		
		var key = "cell_" + cell._ci  + "_" + cell._ri;
		var style = {key:key, css:{}};
		if($scope.styleDictionary[key] == null) {
			style = $scope.styleDictionary[key] = {"key": key, css:{}};
		}
		return style;
		
	};

	$scope.getDictionaryEntryByFormatter = function(cell) {
		var key = "cell_" + cell.column.dataIndex  + "_" + cell.row.dataItemIndex
		var style = {key : key, css:{}};
		if($scope.styleDictionary != null) {
			style = $scope.styleDictionary[key];
		}
		return style;
	};

	$scope.style = {};
	$scope.style.font = $scope.fontList[0];
	$scope.style.fontSize = $scope.fontSizes[1];
	$scope.style.cellStyle = $scope.cellStyles[0];

	$scope.styleDictionary = {};

	$scope.filterCurrentFont = function(f) {
		return $scope.style.font != f;
	};

	
	$scope.saveAndClose = function() {
		
		var isNewWidget = true;
		for(var i = 0; i < WidgetQuestion.model.question.widgets.length; i++) {
			var w = WidgetQuestion.model.question.widgets[i];
			if(w.type == "questionGridblock" && w.model.id == $scope.gridWidget.model.id) {
				$scope.gridWidget.order = i;
				WidgetQuestion.model.question.widgets[i] = angular.copy($scope.gridWidget);
				isNewWidget = false;
				break;
			}
		}

		if(isNewWidget == true) {
			$scope.gridWidget.model.styles = angular.copy($scope.styleDictionary);
			$scope.gridWidget.model.id = "qGrid_" + WidgetQuestion.model.question.widgets.length;
			$scope.gridWidget.order = WidgetQuestion.model.question.widgets.length;
			WidgetQuestion.model.question.widgets.push(angular.copy($scope.gridWidget));
		}
			//WidgetQuestion.model.question.widgets.push({"blank":true});
	
		if(WidgetQuestion.isSaved == true) {
			WidgetQuestion.isDirty = true;
		}
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};

	$scope.close = function() {
		dialog.close();
	};

 	$scope.saveColumnSize = function (e, args) {
		var len = $scope.gridWidget.model.columns.length;
		for(i = 0; i < len; i++) {
			var col = $scope.gridWidget.model.columns[i];
			if(col.dataKey == args.column.dataKey) {
				col.width = args.newWidth;
			}
		}
 	};

 	$scope.getColumns = function() {
 		return $scope.gridWidget.model.columns; 
 	}

 	$scope.initNewGrid = function(rows, cols) {
		var newGrid = {};
		newGrid.columns = [];
		newGrid.list = [];
		for(var i = 0; i < rows; i++) {
			var row = {};	
			for(var j = 0; j < cols; j++) {
				if(i < $scope.gridWidget.model.content.length && $scope.gridWidget.model.content[i]["col_" + j] != null) {
					row["col_" + j ] = $scope.gridWidget.model.content[i]["col_" + j];
				} else {
					row["col_" + j ] = "";
				}
				if(i == 0) {
					newGrid.columns.push({'dataKey': "col_" + j, "headerText":"resize column", "editable":true});
				}
			}

			newGrid.list.push(row);
		}
	

		$scope.gridWidget.model.content = newGrid.list;
		$scope.gridWidget.model.columns = newGrid.columns;
		

	};

 	$scope.resizeGrid = function(rows, cols) {
 		$scope.initNewGrid(rows, cols);

 		 $("#questionGrid").wijgrid("destroy");

 		 $scope.grid = $("#questionGrid").wijgrid({  data:$scope.gridWidget.model.content, 
														columns:$scope.gridWidget.model.columns,
														currentCellChanging:$scope.currentCellChanging, 
														allowColSizing:true, 
														columnResizing:$scope.saveColumnSize,
														cellStyleFormatter:$scope.cellStyleFormatter, 
														selectionMode:$scope.selectMode, 
														ensureControl:false,
														allowEditing:true, 
														highlightOnHover:false}); 
 	};
 

 	$scope.cellStyleFormatter = function (args) {
 		if(args.row.type == 1) {
 			args.$cell.css("font-familty", "Helvetica" );
 			args.$cell.css("font-size", "9px" );
 			args.$cell.css("background", "#fff" );
 			args.$cell.css("font-weight", "120" );
 		} else {
 			var style = $scope.getDictionaryEntryByFormatter(args);
 			if(style != null) {
 				angular.forEach(style.css, function(rule) {
 					var cssKey;
 					var val;
 					for(key in rule) {
 						cssKey = key;
 						val = rule[key];
 						args.$cell.css(cssKey, val);
 					}
 				});
 			}
 		}

    };

 	$timeout(function() {
 		$scope.styleDictionary = $scope.gridWidget.model.styles;
		
 		$scope.initAnswerButtons();
			
	});

});
