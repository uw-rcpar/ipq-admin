
var NavModule = angular.module("NavModule", ["ui.bootstrap"]).config(function($routeProvider){

});
NavModule.controller("NavigationController", function($scope, $location, $routeParams, $dialog) {
	$scope.doSearch = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/search-window.php',
		    controller: 'SearchController',
		    dialogClass: 'modal topicWindow',
		    resolve: {
			searchType: function() {
				return $scope.searchType;
			},
		    	query:function() {
		    		return $scope.query;
		    	}
		    }
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	    /*.then(function(newTopics) {
	    	self.questionTopics = newTopics;
	    });*/

	}


	$scope.docnav = [
		{url:"checkout-question-documents.php", title:"Documents - Multiple choice",current:true, divisor:false},
		{url:"checkout-question-documents-widget-TBS.php", title:"Documents - TBS journal and research", divisor:false, current:false},
	    {url:"checkout-question-documents-widget-WC.php", title:"Documents - Written communication", divisor:false, current:false},
	    {url:"checkout-question-documents-widget-IRS.php", title:"Documents - TBS IRS form", divisor:false, current:false},
		{url:"flagged-documents.php", title: "Flagged documents", divisor:true, current:false}
	];

	$scope.noDivisor = function(doc) {
		return (! doc.divisor) && (!doc.current);
	}

	$scope.useDivisor = function(doc) {
		return doc.divisor && (!doc.current);
	};

 
	$scope.currentdocnav = $scope.docnav[0];
	var _url = $location.$$absUrl;
	for(var i = 0; i < $scope.docnav.length; i++) {
		if( _url.indexOf($scope.docnav[i].url) != -1) {
			$scope.currentdocnav = $scope.docnav[i];
			$scope.docnav[i].current = true;
		} else {
			$scope.docnav[i].current = false;
		}
	}
	$scope.currentdocnav.current = true;


});

NavModule.controller("SearchController", function($scope, $http, query, searchType, dialog) {
	$scope.searchType = searchType;
	$scope.query = query;
	$scope.tabs = {"type":"mcq"};
	/*$scope.tbsReady = false;
	$scope.mpReady = false;
	$scope.wcReady = false;
	$scope.researchReady = false;*/
	$scope.isReady = false;

	$scope.$watch("displayResults", function(n) {
		if(n != null) $scope.isReady = true;
	})
	

	$scope.close = function() {
		dialog.close();
	}

	$scope.show = function(type) {
		$scope.tabs.type = type;
		$scope.displayResults = $scope.results[type];
	}

	$scope.processResults = function() {

					var mcqIds = [],
					tbsIds = [],
					researchIds = [],
					wcIds = [];

					angular.forEach($scope.results.mcq, function(match) {
						match.matchParts = match.question.split($scope.query);

						if ($scope.searchType == 'Keyword') {
							match.id = match.question_id;
						}
						match.extract = match.matchParts.join("<span class='match'>" + $scope.query + "</span>");
						match.url = "/testmodule-admin/search-edit.php#/question/multiple-choice/edit/" + match.id;
console.log(match);
						if ($.inArray(match.id, mcqIds) > -1) {
							match.exclude = true;
						}
						else {
							mcqIds.push(match.id);
						}
						
					});
					
					angular.forEach($scope.results.tbs, function(match) {
						match.url = "/testmodule-admin/build-widget-question.php#/edit/" + match.id;
						var isError = false;
						try{
							var newModel = JSON.parse(match.question_json);
							var question = decodeURIComponent(newModel.question); 
						}
						catch(e) {
							isError = true;
							match.extract = "<div class='alert alert-error'><i class='fa fa-alarm fa-2x'></i> This question has corrupt data, click here to go fix it.</div>";
						}
						if(isError == false) {
							match.question = JSON.parse(question);
							if(match.type == "tbs-wc") {
								match.type = "written communication";
							}
							angular.forEach(match.question.question.widgets, function(widget) {

								if(widget.type == "questionTextblock") {
									match.matchParts = widget.model.content.split($scope.query);
									match.extract = match.matchParts.join("<span class='match'>" + $scope.query + "</span>");
								}else if(widget.type == "researchInput" ) {
									match.type += " <span class='research-subtype'>" + widget.model.content.type.type + "</span>";
								}
							});
						}
						if ($.inArray(match.id, tbsIds) > -1) {
							match.exclude = true;
						}
						else {
							tbsIds.push(match.id);
						}
					});

					angular.forEach($scope.results.research, function(match) {
						match.url = "/testmodule-admin/build-widget-question.php#/edit/" + match.id;
						var isError = false;
						try{
							var newModel = JSON.parse(match.question_json);
							var question = decodeURIComponent(newModel.question); 
						}
						catch(e) {
							isError = true;
							match.extract = "<div class='alert alert-error'><i class='fa fa-alarm fa-2x'></i> This question has corrupt data, click here to go fix it.</div>";
						}
						if(isError == false) {
						match.question = JSON.parse(question);
							angular.forEach(match.question.question.widgets, function(widget) {

								if(widget.type == "questionTextblock") {
									match.matchParts = widget.model.content.split($scope.query);
									match.extract = match.matchParts.join("<span class='match'>" + $scope.query + "</span>");
								}else if(widget.type == "researchInput" ) {
									match.type += " <span class='research-subtype'>" + widget.model.content.type.type + "</span>";
								}
							});
						}
						if ($.inArray(match.id, researchIds) > -1) {
							match.exclude = true;
						}
						else {
							researchIds.push(match.id);
						}
					
					});


					angular.forEach($scope.results.wc, function(match) {

						match.url = "/testmodule-admin/build-wc-question.php#/edit/" + match.id;

						if(match.type == "tbs-wc") {
							match.type = "written communication";
							match.url = "/testmodule-admin/build-wc-question.php#/edit/" + match.id;
							var q = JSON.parse(match.question_json);
				
							match.question = JSON.parse(decodeURIComponent(q.question));
							match.matchParts = match.question.question.question.split($scope.query);
							match.extract = match.matchParts.join("<span class='match'>" + $scope.query + "</span>");
					
						} else {
							match.extract = "Not a written communication"
						}
						
						if ($.inArray(match.id, wcIds) > -1) {
							match.exclude = true;
						}
						else {
							wcIds.push(match.id);
						}
					});


					angular.forEach($scope.results, function(category, key) {
						if (category.length > 0) {
//							$scope.displayResults = category;
							$scope.show(key);
						}
					});
	}

	$scope.search = function(q, type) {
		if(q != null) {
			$scope.query = q;
		}
		if(type != null) {
			$scope.restrictByType = type;
		}

		if ($scope.searchType == "Keyword") {
			return $http.get("/restserver/index.php/api/testcenter/search_all_questions/format/json?search=" + $scope.query).
				success(function(data, status, headers, config) {
					$scope.results = data.results;
					$scope.processResults();
				});
		} //end if
		else { 
console.log('fdsfdfsdf');
			return $http.get("/restserver/index.php/api/testcenter/search_questions_by_id/format/json?search=" + $scope.query).
				success(function(data, status, headers, config) {
					$scope.results = data;
					$scope.processResults();
				});
		}
	};

	$scope.search();


});
