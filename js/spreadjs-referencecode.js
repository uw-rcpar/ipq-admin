        function zoomFactor_Changed(value) {
            var ss = $("#ss").wijspread("spread");
            var sheet = ss.getActiveSheet();
            sheet.zoom(value);
        }
        function selectionChanged(sender, args) {
            var spread = $("#ss").wijspread("spread");
            var sheet = spread.getActiveSheet();
            var content = sheet.getFormula(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $.wijmo.wijspread.SheetArea.viewport);
            if (!content)
                content = sheet.getValue(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $.wijmo.wijspread.SheetArea.viewport);
            else
                content = "=" + content;
            $("#formulabox").val(content);
            var position = sheet.getText(0, sheet.getActiveColumnIndex(), $.wijmo.wijspread.SheetArea.colHeader) + sheet.getText(sheet.getActiveRowIndex(), 0, $.wijmo.wijspread.SheetArea.rowHeader);
            $("#positionbox").val(position);
        }
        function formulaboxBlur() {
            var spread = $("#ss").wijspread("spread");
            var sheet = spread.getActiveSheet();
            var input = $("#formulabox").val();
            if (input.substr(0, 1) == "=")
                sheet.setFormula(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $("#formulabox").val(), $.wijmo.wijspread.SheetArea.viewport);
            else
                sheet.setValue(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $("#formulabox").val(), $.wijmo.wijspread.SheetArea.viewport);
        }
        function formulaboxKeyDown() {
            var x;
            if (window.event) // IE8 and earlier
            {
                x = event.keyCode;
            }
            else if (event.which) // IE9/Firefox/Chrome/Opera/Safari
            {
                x = event.which;
            }
            if (x == 13) {
                formulaboxBlur();
            }
        }
        var validatorType;
        $(document).ready(function () {
            validatorType = "AnyValidator";
            $("#validatorTab1").hide();
            $("#validatorTab2").hide();
            $("#isIntTab").hide();
            $("#isTimeTab").hide();

            var barcolor = "#00FF00";
            var barbordercolor = "#008000";
            var barnegativefillcolor = "#008000";
            var barnegativebordercolor = "#000000";
            var baraxiscolor = "#0000FF";
            var mincolor = "red";
            var midcolor = "yellow";
            var maxcolor = "green";
            var selCellType = "TextCell";
            $('.premium-themes a, .jqueryui-themes a').click(function () {
                $("link[title='rocket-jqueryui']").attr("href", $(this).attr("href"));
                setTimeout(
                        function () {
                            var ss = $("#ss").wijspread("spread");
                            ss.repaint();
                        }, 100);

                return false;
            });

            $("#openFile").bind('change', function () {
                var file = $("#openFile").get(0).files[0];
                if (file) {
                    var reader = new FileReader();

                    reader.onprogress = function (evt) {
                        if (evt.lengthComputable) {
                            var loaded = (evt.loaded / evt.total);
                            if (loaded < 1) {
                            }
                        }
                    };
                    reader.onload = function (evt) {
                        // Obtain the read file data (string)
                        var fileString = evt.target.result;
                        // Handle UTF-8 file dump
                        var spread = $("#ss").wijspread("spread");
                        spread.isPaintSuspended(true);
                        try {
                            if (!spread.id) spread.id = spread.name;
                            var dataField = document.getElementById(spread.id + "_data");
                            if (!dataField) {
                                dataField = document.createElement("input");
                                dataField.id = spread.id + "_data";
                                dataField.type = "hidden";
                                document.body.appendChild(dataField);
                            }

                            dataField.value = fileString;
                            spread._loadData();
                            //if (JSON && JSON.parse) {
                            //    var sd = JSON.parse(fileString);
                            //    spread.fromJSON(sd);
                            //}
                        } catch (e) {
                            alert("Load file \"" + $("#openFile").val()
                                    + "\" failed, please check opened file is a valid SpreadJS JSON file");
                        } finally {
                            spread.isPaintSuspended(false);
                        }
                    };
                    reader.onerror = function (evt) {

                    };

                    // Read file into memory as UTF-8
                    reader.readAsText(file, "UTF-8");
                }
            });

            $("#ribbon").wijribbon({
                click: function (e, cmd) {
                    var spread = $("#ss").wijspread("spread");
                    var sheet = spread.getActiveSheet();
                    sheet.isPaintSuspended(true);
                    switch (cmd.commandName) {
                        case "import":
                            var evt = document.createEvent("MouseEvents");
                            evt.initEvent("click", true, false);
                            document.getElementById("openFile").dispatchEvent(evt);
                            break;
                        case "cut":
                            $.wijmo.wijspread.SpreadActions.cut.call(sheet);
                            break;
                        case "copy":
                            $.wijmo.wijspread.SpreadActions.copy.call(sheet);
                            break;
                        case "paste":
                            $.wijmo.wijspread.SpreadActions.paste.call(sheet);
                            break;
                        case "borders":
                            $("#borderdialog").dialog("open");
                            break;
                        case "highlightcell":
                            $("#Rule1 option:eq(0)").attr("selected", "selected");
                            $("#Rule1").empty();
                            $("#Rule1").show();
                            $("#Rule1").append("<option value='0'>Cell Value</option>");
                            $("#Rule1").append("<option value='1'>Specific Text</option>");
                            $("#Rule1").append("<option value='2'>Date Occurring</option>");
                            $("#Rule1").append("<option value='5'>Unique</option>");
                            $("#Rule1").append("<option value='6'>Duplicate</option>");
                            var rule = "0";
                            var type = $("#ComparisonOperator1");
                            setEnumTypeOfCF(rule, type);
                            $("#conditionalformatdialog").dialog("open");
                            break;
                        case "topbottom":
                            $("#Rule1 option:eq(1)").attr("selected", "selected");
                            $("#Rule1").empty();
                            $("#Rule1").show();
                            $("#Rule1").append("<option value='4'>Top10</option>");
                            $("#Rule1").append("<option value='7'>Average</option>");
                            var rule = "4";
                            var type = $("#ComparisonOperator1");
                            setEnumTypeOfCF(rule, type);
                            $("#conditionalformatdialog").dialog("open");
                            break;
                        case "colorscale":
                            $("#Rule1 option:eq(2)").attr("selected", "selected");
                            $("#Rule1").empty();
                            $("#Rule1").show();
                            $("#Rule1").append("<option value='8'>2-Color Scale</option>");
                            $("#Rule1").append("<option value='9'>3-Color Scale</option>");
                            var rule = "8";
                            var type = $("#ComparisonOperator1");
                            setEnumTypeOfCF(rule, type);
                            $("#conditionalformatdialog").dialog("open");
                            break;
                        case "otherofconditionalformat":
                            $("#Rule1 option:eq(3)").attr("selected", "selected");
                            $("#Rule1").empty();
                            $("#Rule1").show();
                            $("#Rule1").append("<option value='3'>FormulaRule</option>");
                            var rule = "3";
                            var type = $("#ComparisonOperator1");
                            setEnumTypeOfCF(rule, type);
                            $("#conditionalformatdialog").dialog("open");
                            break;
                        case "databar":
                            $("#databardialog").dialog("open");
                            break;
                        case "iconset":
                            createIconCriteriaDOM();
                            $("#iconsetdialog").dialog("open");
                            break;
                        case "removeconditionalformats":
                            var cfs = sheet.getConditionalFormats();
                            var row = sheet.getActiveRowIndex(), col = sheet.getActiveColumnIndex();
                            var rules = cfs.getRules(row, col);
                            $.each(rules, function (i, v) {
                                cfs.removeRule(v);
                            });
                            sheet.isPaintSuspended(false);
                            break;
                        case "textboxcelltype":
                            $("#comboCellOptions").hide();
                            $("#checkBoxCellOptions").hide();
                            $("#buttonCellOptions").hide();
                            $("#hyperlinkCellOptions").hide();
                            var cellType = new $.wijmo.wijspread.TextCellType();
                            sheet.isPaintSuspended(true);
                            sheet.suspendEvent();
                            var sels = sheet.getSelections();
                            for (var i = 0; i < sels.length; i++) {
                                var sel = sheet._getActualRange(sels[i]);
                                for (var r = 0; r < sel.rowCount; r++) {
                                    for (var c = 0; c < sel.colCount; c++) {
                                        sheet.setCellType(sel.row + r, sel.col + c, cellType, $.wijmo.wijspread.SheetArea.viewport);
                                    }
                                }
                            }
                            sheet.resumeEvent();
                            sheet.isPaintSuspended(false);
                            break;
                        case "comboboxcelltype":
                            $("#comboCellOptions").show();
                            $("#checkBoxCellOptions").hide();
                            $("#buttonCellOptions").hide();
                            $("#hyperlinkCellOptions").hide();
                            $("#celltypedialog").dialog("open");
                            selCellType = "ComboCell";
                            break;
                        case "checkboxcelltype":
                            $("#comboCellOptions").hide();
                            $("#checkBoxCellOptions").show();
                            $("#buttonCellOptions").hide();
                            $("#hyperlinkCellOptions").hide();
                            $("#celltypedialog").dialog("open");
                            selCellType = "CheckBoxCell";
                            break;
                        case "buttoncelltype":
                            $("#comboCellOptions").hide();
                            $("#checkBoxCellOptions").hide();
                            $("#buttonCellOptions").show();
                            $("#hyperlinkCellOptions").hide();
                            $("#celltypedialog").dialog("open");
                            selCellType = "ButtonCell";
                            break;
                        case "hyperlinkcelltype":
                            $("#comboCellOptions").hide();
                            $("#checkBoxCellOptions").hide();
                            $("#buttonCellOptions").hide();
                            $("#hyperlinkCellOptions").show();
                            $("#celltypedialog").dialog("open");
                            selCellType = "HyperLinkCell";
                            break;
                        case "alloweditorreservedlocations":
                            sheet.allowEditorReservedLocations($("#alleditorreserved").attr("checked") === "checked" ? true : false);
                            break;
                        case "ClearCellType":
                            var sels = sheet.getSelections();
                            for (var i = 0; i < sels.length; i++) {
                                var sel = sheet._getActualRange(sels[i]);
                                sheet.clear(sel.row, sel.col, sel.rowCount, sel.colCount, $.wijmo.wijspread.SheetArea.viewport, $.wijmo.wijspread.StorageType.Style);
                            }
                            sheet.isPaintSuspended(false);
                            break;
                        case "datavalidation":
                            $("#datavalidationdialog").dialog("open");
                            break;
                        case "circleinvaliddata":
                            spread.highlightInvalidData(true);
                            break;
                        case "clearvalidationcircles":
                            spread.highlightInvalidData(false);
                            break;
                        case "isr1c1":
                            var result = $("#isR1C1").prop("checked");
                            if (result) {
                                spread.referenceStyle($.wijmo.wijspread.ReferenceStyle.R1C1);
                            }
                            else {
                                spread.referenceStyle($.wijmo.wijspread.ReferenceStyle.A1);
                            }
                            break;
                        case "canuserdragdrop":
                            var result = $("#canUserDragDrop").prop("checked");
                            sheet.canUserDragDrop(result);
                            sheet.isPaintSuspended(false);
                            break;
                        case "canuserdragfill":
                            var result = $("#canUserDragFill").prop("checked");
                            sheet.canUserDragFill(result);
                            sheet.isPaintSuspended(false);
                            break;
                        case "canusereditformula":
                            var result = $("#canUserEditFormula").prop("checked");
                            spread.canUserEditFormula(result);
                            break;
                        case "merge":
                            var sels = sheet.getSelections();
                            var hasSpan = false;
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                if (sheet.getSpans(sel, $.wijmo.wijspread.SheetArea.viewport).length > 0) {
                                    for (var i = 0; i < sel.rowCount; i++) {
                                        for (var j = 0; j < sel.colCount; j++) {
                                            sheet.removeSpan(i + sel.row, j + sel.col);
                                        }
                                    }
                                    hasSpan = true;
                                }
                            }
                            if (!hasSpan) {
                                for (var n = 0; n < sels.length; n++) {
                                    var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                    sheet.addSpan(sel.row, sel.col, sel.rowCount, sel.colCount);
                                }
                            }
                            break;
                        case "justifyleft":
                        case "justifycenter":
                        case "justifyright":
                            var align = $.wijmo.wijspread.HorizontalAlign.left;
                            if (cmd.commandName == "justifycenter") align = $.wijmo.wijspread.HorizontalAlign.center;
                            if (cmd.commandName == "justifyright") align = $.wijmo.wijspread.HorizontalAlign.right;
                            var sels = sheet.getSelections();
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).hAlign(align);
                            }
                            break;
                        case "increaseindent":
                        case "decreaseindent":
                            var sels = sheet.getSelections();
                            var offset = 1;
                            if (cmd.commandName == "decreaseindent") offset = -1;
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                for (var i = 0; i < sel.rowCount; i++) {
                                    for (var j = 0; j < sel.colCount; j++) {
                                        var indent = sheet.getCell(i + sel.row, j + sel.col, $.wijmo.wijspread.SheetArea.viewport).textIndent();
                                        if (isNaN(indent)) indent = 0;
                                        sheet.getCell(i + sel.row, j + sel.col, $.wijmo.wijspread.SheetArea.viewport).textIndent(indent + offset);
                                    }
                                }
                            }
                            break;
                        case "backcolor":
                        case "fontcolor":
                            $("#colordialog").dialog({ title: cmd.commandName == "backcolor" ? "Back Color" : "Fore Color" });
                            $("#colordialog").dialog("open");
                            break;
                        case "tabStripColor":
                            $("#colordialog").dialog({ title: "Tab Strip Color" });
                            $("#colordialog").dialog("open");
                            break;
                        case "frozenlinecolor":
                            $("#colordialog").dialog({ title: "FrozenLineColor" });
                            $("#colordialog").dialog("open");
                            break;
                        case "bold":
                        case "italic":
                            var styleEle = document.getElementById("colorSample");
                            var font = sheet.getCell(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $.wijmo.wijspread.SheetArea.viewport).font();
                            if (font != undefined) {
                                styleEle.style.font = font;
                            } else {
                                styleEle.style.font = "10pt Arial";
                            }
                            if (cmd.commandName == "bold") {
                                if (styleEle.style.fontWeight == "bold") {
                                    styleEle.style.fontWeight = "";
                                } else {
                                    styleEle.style.fontWeight = "bold";
                                }
                            } else if (cmd.commandName == "italic") {
                                if (styleEle.style.fontStyle == "italic") {
                                    styleEle.style.fontStyle = "";
                                } else {
                                    styleEle.style.fontStyle = "italic";
                                }
                            }
                            var sels = sheet.getSelections();
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                //sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).font(document.defaultView.getComputedStyle(styleEle, "").font);
                                sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).font(styleEle.style.font);
                            }
                            break;
                        case "insertcol":
                            sheet.addColumns(sheet.getActiveColumnIndex(), 1);
                            break;
                        case "insertrow":
                            sheet.addRows(sheet.getActiveRowIndex(), 1);
                            break;
                        case "deleterow":
                            sheet.deleteRows(sheet.getActiveRowIndex(), 1);
                            break;
                        case "deletecol":
                            sheet.deleteColumns(sheet.getActiveColumnIndex(), 1);
                            break;
                        case "sortaz":
                        case "sortza":
                            var sels = sheet.getSelections();
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                sheet.sortRange(sel.row, sel.col, sel.rowCount, sel.colCount, true, [
                                    { index: sel.col, ascending: cmd.commandName == "sortaz" }
                                ]);
                            }
                            break;
                        case "filter":
                            if (sheet.rowFilter()) {
                                sheet.rowFilter(null);
                            } else {
                                var sels = sheet.getSelections();
                                if (sels.length > 0) {
                                    var sel = sels[0];
                                    sheet.rowFilter(new $.wijmo.wijspread.HideRowFilter(sel));
                                }
                            }
                            break;
                        case "group":
                            var sels = sheet.getSelections();
                            var sel = sels[0];

                            if (sel.col == -1) // row selection
                            {
                                var groupExtent = new $.wijmo.wijspread.UndoRedo.GroupExtent(sel.row, sel.rowCount);
                                var action = new $.wijmo.wijspread.UndoRedo.RowGroupUndoAction(sheet, groupExtent);
                                spread.doCommand(action);
                                $("#showRowGroup").attr("disabled", false);
                                if ($("#showRowGroup").attr("value") == "true") {
                                    $("#showRowGroupLabel").unbind("mouseup", rowlabelnoactive);
                                }
                                $("#showRowGroup").attr("value", "true");
                                $("#showRowGroupLabel").attr("disabled", false);
                                $("#showRowGroupLabel").removeClass("ui-state-disabled");
                                $("#showRowGroupLabel").unbind("mouseup", rowlabelactive);
                                sheet.showRowRangeGroup(true);
                            }
                            else if (sel.row == -1) // column selection
                            {
                                var groupExtent = new $.wijmo.wijspread.UndoRedo.GroupExtent(sel.col, sel.colCount);
                                var action = new $.wijmo.wijspread.UndoRedo.ColumnGroupUndoAction(sheet, groupExtent);
                                spread.doCommand(action);
                                $("#showColGroup").attr("disabled", false);
                                if ($("#showColGroup").attr("value") == "true") {
                                    $("#showColGroupLabel").unbind("mouseup", collabelnoactive);
                                }
                                $("#showColGroup").attr("value", "true");
                                $("#showColGroupLabel").attr("disabled", false);
                                $("#showColGroupLabel").removeClass("ui-state-disabled");
                                $("#showColGroupLabel").unbind("mouseup", collabelactive);
                                sheet.showColumnRangeGroup(true);
                            }
                            else // cell range selection
                            {
                                alert("please select a range of row or col");
                            }
                            break;
                        case "ungroup":
                            var sels = sheet.getSelections();
                            var sel = sels[0];

                            if (sel.col == -1 && sel.row == -1) // sheet selection
                            {
                                sheet.rowRangeGroup.ungroup(0, sheet.getRowCount());
                                sheet.colRangeGroup.ungroup(0, sheet.getColumnCount());
                            }
                            else if (sel.col == -1) // row selection
                            {
                                //sheet.rowRangeGroup.ungroup(sel.row, sel.rowCount);
                                var groupExtent = new $.wijmo.wijspread.UndoRedo.GroupExtent(sel.row, sel.rowCount);
                                var action = new $.wijmo.wijspread.UndoRedo.RowUngroupUndoAction(sheet, groupExtent);
                                spread.doCommand(action);
                                if (sheet.rowRangeGroup.getMaxLevel() < 0) {
                                    $("#showRowGroup").attr("disabled", true);
                                    $("#showRowGroup").attr("value", "true");
                                    $("#showRowGroupLabel").attr("disabled", true);
                                    $("#showRowGroupLabel").addClass("ui-state-disabled");
                                    $("#showRowGroupLabel").bind("mouseup", rowlabelnoactive);
                                }
                            }
                            else if (sel.row == -1) // column selection
                            {
                                //sheet.colRangeGroup.ungroup(sel.col, sel.colCount);
                                var groupExtent = new $.wijmo.wijspread.UndoRedo.GroupExtent(sel.col, sel.colCount);
                                var action = new $.wijmo.wijspread.UndoRedo.ColumnUngroupUndoAction(sheet, groupExtent);
                                spread.doCommand(action);
                                if (sheet.colRangeGroup.getMaxLevel() < 0) {
                                    $("#showColGroup").attr("disabled", true);
                                    $("#showColGroup").attr("value", "true");
                                    $("#showColGroupLabel").attr("disabled", true);
                                    $("#showColGroupLabel").addClass("ui-state-disabled");
                                    $("#showColGroupLabel").bind("mouseup", collabelnoactive);
                                }
                            }
                            else // cell range selection
                            {
                                alert("please select a range of row or col");
                            }
                            break;
                        case "showdetail":
                        case "hidedetail":
                            var sels = sheet.getSelections();
                            var sel = sels[0];

                            if (sel.col == -1 && sel.row == -1) // sheet selection
                            {
                            }
                            else if (sel.col == -1) // row selection
                            {
                                for (var i = 0; i < sel.rowCount; i++) {
                                    var rgi = sheet.rowRangeGroup.find(sel.row + i, 0);
                                    if (rgi) {
                                        sheet.rowRangeGroup.expand(rgi.level, cmd.commandName == "showdetail");
                                    }
                                }
                            }
                            else if (sel.row == -1) // column selection
                            {
                                for (var i = 0; i < sel.colCount; i++) {
                                    var rgi = sheet.colRangeGroup.find(sel.col + i, 0);
                                    if (rgi) {
                                        sheet.colRangeGroup.expand(rgi.level, cmd.commandName == "showdetail");
                                    }
                                }
                            }
                            else // cell range selection
                            {
                            }
                            break;
                        case "showrowrangegroup":
                            if (!$("#showRowGroup").attr("disabled")) {
                                sheet.showRowRangeGroup($("#showRowGroup").prop("checked"));
                            }
                            break;
                        case "showcolrangegroup":
                            if (!$("#showColGroup").attr("disabled")) {
                                sheet.showColumnRangeGroup($("#showColGroup").prop("checked"));
                            }
                            break;
                        case "enableUseWijmoTheme":
                            if ($("#wijmothemecheck").prop("checked")) {
                                $("#setTabStripColor").attr("disabled", false);
                                $("#setTabStripColor").removeClass("ui-state-disabled");
                                spread.useWijmoTheme = false;
                                spread.repaint();
                            } else {
                                $("#setTabStripColor").attr("disabled", true);
                                $("#setTabStripColor").addClass("ui-state-disabled");
                                spread.useWijmoTheme = true;
                                spread.repaint();
                            }
                            break;
                        case "rowheader":
                            //For Support IE7/8
                            //sheet.rowHeaderVisible = !sheet.rowHeaderVisible;
                            sheet.setRowHeaderVisible(!sheet.getRowHeaderVisible());
                            break;
                        case "columnheader":
                            //For Support IE7/8
                            //sheet.colHeaderVisible = !sheet.colHeaderVisible;
                            sheet.setColumnHeaderVisible(!sheet.getColumnHeaderVisible());
                            break;
                        case "vgridline":
                            //For Support IE7/8
                            //sheet.gridline.showVerticalGridline = !sheet.gridline.showVerticalGridline;
                            var vGridLine = sheet.gridline.showVerticalGridline;
                            var hGridLine = sheet.gridline.showHorizontalGridline;
                            sheet.setGridlineOptions({ showVerticalGridline: !vGridLine, showHorizontalGridline: hGridLine });
                            break;
                        case "hgridline":
                            //For Support IE7/8
                            //sheet.gridline.showHorizontalGridline = !sheet.gridline.showHorizontalGridline;
                            var hGridLine = sheet.gridline.showHorizontalGridline;
                            var vGridLine = sheet.gridline.showVerticalGridline;
                            sheet.setGridlineOptions({ showVerticalGridline: vGridLine, showHorizontalGridline: !hGridLine });
                            break;
                        case "tabstrip":
                            spread.tabStripVisible(!spread.tabStripVisible());
                            break;
                        case "newtab":
                            spread.newTabVisible(!spread.newTabVisible());
                            break;
                        case "freezepane":
                            sheet.setFrozenCount(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex());
                            break;
                        case "freezerows":
                            sheet.setFrozenCount(sheet.getActiveRowIndex(), 0);
                            break;
                        case "freezecolumns":
                            sheet.setFrozenCount(0, sheet.getActiveColumnIndex());
                            break;
                        case "unfreeze":
                            sheet.setFrozenCount(0, 0);
                            break;
                        default:
                            if (cmd.commandName.substr(0, 1) == "f") {
                                var styleEle = document.getElementById("colorSample");
                                var font = sheet.getCell(sheet.getActiveRowIndex(), sheet.getActiveColumnIndex(), $.wijmo.wijspread.SheetArea.viewport).font();
                                if (font) {
                                    styleEle.style.font = font;
                                }
                                else {
                                    styleEle.style.font = "10pt Arial";
                                }

                                if (cmd.commandName.substr(0, 2) == "fn")
                                    styleEle.style.fontFamily = document.getElementById(cmd.commandName)["title"];
                                if (cmd.commandName.substr(0, 2) == "fs")
                                    styleEle.style.fontSize = document.getElementById(cmd.commandName)["title"];
                                var sels = sheet.getSelections();
                                for (var n = 0; n < sels.length; n++) {
                                    var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                    //sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).font(document.defaultView.getComputedStyle(styleEle, "").font);
                                    sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).font("10pt " + styleEle.style.font);
                                }
                            } else if (cmd.commandName.substr(0, 1) == "s") {
                                if (cmd.commandName.substr(0, 2) == "sp") {
                                    var policy = 2;
                                    if (document.getElementById(cmd.commandName)["title"] == "Single") {
                                        policy = 0;
                                    } else if (document.getElementById(cmd.commandName)["title"] == "Range") {
                                        policy = 1;
                                    } else if (document.getElementById(cmd.commandName)["title"] == "MultiRange") {
                                        policy = 2;
                                    }
                                    sheet.selectionPolicy(policy);
                                }
                                if (cmd.commandName.substr(0, 2) == "su") {
                                    var unit = 0;
                                    if (document.getElementById(cmd.commandName)["title"] == "Cell") {
                                        unit = 0;
                                    } else if (document.getElementById(cmd.commandName)["title"] == "Row") {
                                        unit = 1;
                                    } else if (document.getElementById(cmd.commandName)["title"] == "Column") {
                                        unit = 2;
                                    }
                                    sheet.selectionUnit(unit);
                                }
                            } else {
                                alert(cmd.commandName);
                            }

                            break;
                    }
                    sheet.isPaintSuspended(false);
                }
            });
            $("#datavalidationdialog").wijribbon();
            $("#validationInputMessge").bind("mousedown", IsRangeSet);
            $("#validationErrorAlert").bind("mousedown", IsRangeSet);
            $("#validatorTypes").change(function () {
                validatorType = $(this).val();
                switch (validatorType) {
                    case "AnyValidator":
                        $("#validatorTab1").hide();
                        $("#isTimeTab").hide();
                        $("#isIntTab").hide();
                        $("#validatorTab2").hide();
                        break;
                    case "DateValidator":
                        $("#validatorTab1").show();
                        $("#isTimeTab").show();
                        $("#isIntTab").hide();
                        $("#validatorTab2").hide();
                        $("#rangeStart").text("Start date:");
                        $("#rangeEnd").text("End date:");
                        break;
                    case "FormulaListValidator":
                        $("#validatorTab2").show();
                        $("#validatorTab1").hide();
                        $("#rangevalidator").text("Formula:");
                        $("#prompt").text("(e.g: E5:I5)");
                        $("#datatitle").hide();
                        break;
                    case "FormulaValidator":
                        $("#validatorTab2").show();
                        $("#validatorTab1").hide();
                        $("#rangevalidator").text("Formula:");
                        $("#prompt").text("(e.g: =ISERROR(FIND(\" \",A1)))");
                        $("#datatitle").hide();
                        break;
                    case "ListValidator":
                        $("#validatorTab2").show();
                        $("#validatorTab1").hide();
                        $("#rangevalidator").text("Source");
                        $("#prompt").text("(e.g: 1,2,3)");
                        $("#datatitle").hide();
                        break;
                    case "NumberValidator":
                        $("#validatorTab1").show();
                        $("#isTimeTab").hide();
                        $("#isIntTab").show();
                        $("#validatorTab2").hide();
                        $("#rangeStart").text("Minimum:");
                        $("#rangeEnd").text("Maximum:");
                        break;
                    case "TextLengthValidator":
                        $("#validatorTab1").show();
                        $("#isTimeTab").hide();
                        $("#isIntTab").hide();
                        $("#validatorTab2").hide();
                        $("#rangeStart").text("Minimum:");
                        $("#rangeEnd").text("Maximum:");
                        break;
                }
            });
            $("#chkValidatorIgnoreBlank").change(function () {
                var ss = $("#ss").wijspread("spread");
                var sheet = ss.getActiveSheet();
                var sels = sheet.getSelections();
                for (var i = 0; i < sels.length; i++) {
                    var sel = sheet._getActualRange(sels[i]);
                    for (var r = 0; r < sel.rowCount; r++) {
                        for (var c = 0; c < sel.colCount; c++) {
                            var dv = sheet.getDataValidator(sel.row + r, sel.col + c);
                            if (dv) {
                                dv.ignoreBlank = $(this).prop("checked");
                            }
                        }
                    }
                }
            });

            $("#chkShowError").change(function () {
                var ss = $("#ss").wijspread("spread");
                var sheet = ss.getActiveSheet();
                var checked = $("#chkShowError").prop("checked");
                if (checked) {
                    ss.bind($.wijmo.wijspread.Events.ValidationError, function (event, data) {
                        var dv = data.validator;
                        if (dv) {
                            alert(dv.errorMessage);
                        }
                    });
                } else {
                    ss.unbind($.wijmo.wijspread.Events.ValidationError);
                }
            });
            //  Conditional formatting rules
            $("#Rule1").bind("change", function () {
                var rule = $("#Rule1").val();
                var type = $("#ComparisonOperator1");
                setEnumTypeOfCF(rule, type);
            });
            $("#ComparisonOperator1").bind("change", function () {
                var type = $("#ComparisonOperator1").val();
                switch (type) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                        $("#andtext").hide();
                        $("#value2").hide();
                        break;
                    case "6":
                    case "7":
                        $("#andtext").show();
                        $("#andtext").text("and");
                        $("#value2").show();
                        break;
                    default:
                        $("#andtext").hide();
                        $("#value2").hide();
                        break;
                }
            });
            $("#conditionalformatdialog").dialog({
                autoOpen: false,
                height: 400,
                width: 500,
                modal: true,
                resizable: false,
                buttons: {
                    OK: function () {
                        var spread1 = $("#ss").wijspread("spread");
                        var sheet = spread1.getActiveSheet();
                        var sels = sheet.getSelections();
                        var style = new $.wijmo.wijspread.Style();
                        style.backColor = "red";
                        style.foreColor = "green";
                        var value1 = $("#value1").val();
                        var value2 = $("#value2").val();
                        var cfs = sheet.getConditionalFormats();
                        var rule = $("#Rule1").val();
                        var operator = parseInt($("#ComparisonOperator1").val());

                        var minType = parseInt($("#minType").val());
                        var midType = parseInt($("#midType").val());
                        var maxType = parseInt($("#maxType").val());
                        var midColor = midcolor;
                        var minColor = mincolor;
                        var maxColor = maxcolor;
                        var midValue = $("#midValue").val();
                        var maxValue = $("#maxValue").val();
                        var minValue = $("#minValue").val();

                        switch (rule) {
                            case "0":
                                var doubleValue1 = parseFloat(value1);
                                var doubleValue2 = parseFloat(value2);
                                cfs.addCellValueRule(operator, isNaN(doubleValue1) ? value1 : doubleValue1, isNaN(doubleValue2) ? value2 : doubleValue2, style, sels);
                                break;
                            case "1":
                                cfs.addSpecificTextRule(operator, value1, style, sels);
                                break;
                            case "2":
                                cfs.addDateOccurringRule(operator, style, sels);
                                break;
                            case "3":
                                try {
                                    cfs.addFormulaRule(value1, style, sels);
                                }
                                catch (e) {
                                    cfs.removeRule(cfs.getRule(cfs.count() - 1));
                                    alert("Invalid Formula");
                                }
                                break;
                            case "4":
                                cfs.addTop10Rule(operator, parseInt(value1), style, sels);
                                break;
                            case "5":
                                cfs.addUniqueRule(style, sels);
                                break;
                            case "6":
                                cfs.addDuplicateRule(style, sels);
                                break;
                            case "7":
                                cfs.addAverageRule(operator, style, sels);
                                break;
                            case "8":
                                cfs.add2ScaleRule(minType, minValue, minColor, maxType, maxValue, maxColor, sels);
                                break;
                            case "9":
                                cfs.add3ScaleRule(minType, minValue, minColor, midType, midValue, midColor, maxType, maxValue, maxColor, sels);
                                break;
                            default:
                                var doubleValue1 = parseFloat(value1);
                                var doubleValue2 = parseFloat(value2);
                                cfs.addCellValueRule(operator, isNaN(doubleValue1) ? value1 : doubleValue1, isNaN(doubleValue2) ? value2 : doubleValue2, style, sels);
                                break;
                        }
                        $(this).dialog("close");
                        sheet.repaint();
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            //  databar
            $("#databardialog").dialog({
                autoOpen: false,
                height: 500,
                width: 420,
                modal: true,
                resizable: false,
                buttons: {
                    OK: function () {
                        var spread1 = $("#ss").wijspread("spread");
                        var sheet = spread1.getActiveSheet();
                        sheet.isPaintSuspended(true);

                        var selections = sheet.getSelections();
                        if (selections) {
                            var ranges = [];
                            $.each(selections, function (i, v) {
                                ranges.push(new $.wijmo.wijspread.Range(v.row, v.col, v.rowCount, v.colCount));
                            });
                            var cfs = sheet.getConditionalFormats();
                            var dataBarRule = new $.wijmo.wijspread.DataBarRule();
                            dataBarRule.ranges = ranges;
                            dataBarRule.minimumType(parseInt($("#minimumType").val()));
                            dataBarRule.minimumValue(parseValue($("#minimumValue").val()));
                            dataBarRule.maximumType(parseInt($("#maximumType").val()));
                            dataBarRule.maximumValue(parseValue($("#maximumValue").val()));
                            dataBarRule.gradient($("#gradient").prop("checked"));
                            dataBarRule.color(barcolor);
                            dataBarRule.showBorder($("#showBorder").prop("checked"));
                            dataBarRule.borderColor(barbordercolor);
                            dataBarRule.dataBarDirection(parseInt($("#dataBarDirection").val()));
                            dataBarRule.negativeFillColor(barnegativefillcolor);
                            dataBarRule.useNegativeFillColor($("#useNegativeFillColor").prop("checked"));
                            dataBarRule.negativeBorderColor(barnegativebordercolor);
                            dataBarRule.useNegativeBorderColor($("#useNegativeBorderColor").prop("checked"));
                            dataBarRule.axisPosition(parseInt($("#axisPosition").val()));
                            dataBarRule.axisColor(baraxiscolor);
                            dataBarRule.showBarOnly($("#showBarOnly").prop("checked"));
                            cfs.addRule(dataBarRule);
                        }

                        sheet.isPaintSuspended(false);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            //  iconset
            $("#iconSetType").bind("change", function () {
                createIconCriteriaDOM();
            });
            $("#iconsetdialog").dialog({
                autoOpen: false,
                height: 400,
                width: 400,
                modal: true,
                resizable: false,
                buttons: {
                    OK: function () {
                        var spread1 = $("#ss").wijspread("spread");
                        var sheet = spread1.getActiveSheet();
                        sheet.isPaintSuspended(true);

                        var selections = sheet.getSelections();
                        if (selections) {
                            var ranges = [];
                            $.each(selections, function (i, v) {
                                ranges.push(new $.wijmo.wijspread.Range(v.row, v.col, v.rowCount, v.colCount));
                            });
                            var cfs = sheet.getConditionalFormats();
                            var iconSetRule = new $.wijmo.wijspread.IconSetRule();
                            iconSetRule.ranges = ranges;
                            iconSetRule.iconSetType(parseInt($("#iconSetType").val()));
                            var $divs = $("#iconCriteriaSetting div");
                            var iconCriteria = iconSetRule.iconCriteria();
                            $.each($divs, function (i, v) {
                                var isGreaterThanOrEqualTo = parseInt($(v.children[0]).val()) === 1;
                                var iconValueType = parseInt($(v.children[2]).val());
                                var iconValue = $(v.children[1]).val();
                                if (iconValueType !== $.wijmo.wijspread.IconValueType.Formula) {
                                    iconValue = parseInt(iconValue);
                                }
                                iconCriteria[i] = new $.wijmo.wijspread.IconCriterion(isGreaterThanOrEqualTo, iconValueType, iconValue);
                            });
                            iconSetRule.reverseIconOrder($("#reverseIconOrder").prop("checked"));
                            iconSetRule.showIconOnly($("#showIconOnly").prop("checked"));
                            cfs.addRule(iconSetRule);
                        }

                        sheet.isPaintSuspended(false);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#datavalidationdialog").dialog({
                autoOpen: false,
                height: 400,
                width: 400,
                modal: true,
                resizable: false,
                buttons: {
                    ClearValidator: function () {
                        var ss = $("#ss").wijspread("spread");
                        var sheet = ss.getActiveSheet();
                        var sels = sheet.getSelections();
                        for (var i = 0; i < sels.length; i++) {
                            var sel = sheet._getActualRange(sels[i]);
                            for (var r = 0; r < sel.rowCount; r++) {
                                for (var c = 0; c < sel.colCount; c++) {
                                    sheet.setDataValidator(sel.row + r, sel.col + c, null);
                                }
                            }
                        }
                        $("#validatorTypes").val("AnyValidator");
                        $("#txtValidatorValue1").val("");
                        $("#txtValidatorValue2").val("");
                        $("#txtValidatorValue").val("");
                        $("#validatorTab1").hide();
                        $("#isTimeTab").hide();
                        $("#isIntTab").hide();
                        $("#validatorTab2").hide();
                        $("#txtMessageTitle").val("");
                        $("#txtMessageMessage").val("");
                        $("#validatorErrorStyles").val("0");
                        $("#txtErrorTitle").val("");
                        $("#txtErrorMessage").val("");
                        validatorType = "AnyValidator";
                        $(this).dialog("close");
                    },
                    OK: function () {
                        //setvalidator
                        var gcdv = $.wijmo.wijspread.DefaultDataValidator;

                        var ddv = null;
                        var v1 = $("#txtValidatorValue1").val();
                        var v2 = $("#txtValidatorValue2").val();
                        switch (validatorType) {
                            case "AnyValidator":
                                ddv = new $.wijmo.wijspread.DefaultDataValidator();
                                break;
                            case "DateValidator":
                                if ($("#chkIsTime").prop("checked")) {
                                    ddv = gcdv.createDateValidator(parseInt($("#validatorComparisonOperator").val()),
                                            isNaN(v1) ? v1 : new Date(v1),
                                            isNaN(v2) ? v2 : new Date(v2),
                                            true);
                                } else {
                                    ddv = gcdv.createDateValidator(parseInt($("#validatorComparisonOperator").val()),
                                            isNaN(v1) ? v1 : new Date(v1),
                                            isNaN(v2) ? v2 : new Date(v2),
                                            false);
                                }
                                break;
                            case "FormulaListValidator":
                                ddv = gcdv.createFormulaListValidator($("#txtValidatorValue").val());
                                break;
                            case "FormulaValidator":
                                ddv = gcdv.createFormulaValidator($("#txtValidatorValue").val());
                                break;
                            case "ListValidator":
                                ddv = gcdv.createListValidator($("#txtValidatorValue").val());
                                break;
                            case "NumberValidator":
                                if ($("#chkIsInteger").prop("checked")) {
                                    ddv = gcdv.createNumberValidator(parseInt($("#validatorComparisonOperator").val()),
                                            isNaN(v1) ? v1 : parseInt(v1),
                                            isNaN(v2) ? v2 : parseInt(v2),
                                            true);
                                } else {
                                    ddv = gcdv.createNumberValidator(parseInt($("#validatorComparisonOperator").val()),
                                            isNaN(v1) ? v1 : parseFloat(v1),
                                            isNaN(v2) ? v2 : parseFloat(v2),
                                            false);
                                }
                                break;
                            case "TextLengthValidator":
                                ddv = gcdv.createTextLengthValidator(parseInt($("#validatorComparisonOperator").val()),
                                        isNaN(v1) ? v1 : parseInt(v1),
                                        isNaN(v2) ? v2 : parseInt(v2));
                                break;
                        }

                        if (ddv != null) {
                            ddv.errorMessage = $("#txtErrorMessage").val();
                            ddv.errorStyle = parseInt($("#validatorErrorStyles").val());
                            ddv.errorTitle = $("#txtErrorTitle").val();
                            ddv.showErrorMessage = $("#chkShowError").prop("checked");
                            ddv.ignoreBlank = $("#chkValidatorIgnoreBlank").prop("checked");
                            var checked = $("#chkShowMessage").prop("checked");
                            if (checked) {
                                ddv.inputTitle = $("#txtMessageTitle").val();
                                ddv.inputMessage = $("#txtMessageMessage").val();
                            }

                            var ss = $("#ss").wijspread("spread");
                            var sheet = ss.getActiveSheet();
                            sheet.isPaintSuspended(true);
                            var sels = sheet.getSelections();
                            for (var i = 0; i < sels.length; i++) {
                                var sel = sheet._getActualRange(sels[i]);
                                for (var r = 0; r < sel.rowCount; r++) {
                                    for (var c = 0; c < sel.colCount; c++) {
                                        sheet.setDataValidator(sel.row + r, sel.col + c, ddv);
                                    }
                                }
                            }
                            sheet.isPaintSuspended(false);
                        }
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            //Cell Type

            $("#celltypedialog").dialog({
                autoOpen: false,
                height: 350,
                width: 350,
                modal: true,
                resizable: false,
                buttons: {
                    Set: function () {
                        var spread = $("#ss").wijspread("spread");
                        var sheet = spread.getActiveSheet();
                        var cellType;
                        switch (selCellType) {
                            case "TextCell":
                                cellType = new $.wijmo.wijspread.TextCellType();
                                break;
                            case "ComboCell":
                                cellType = new $.wijmo.wijspread.ComboBoxCellType();
                                cellType.editorValueType(parseInt($("#selComboCellEditorValueType").val()));
                                var itemsText = $("#txtComboCellItemsText").val().split(",");
                                var itemsValue = $("#txtComboCellItemsValue").val().split(",");
                                var itemsLength = itemsText.length > itemsValue.length ? itemsText.length : itemsValue.length;
                                var items = [];
                                for (var count = 0; count < itemsLength; count++) {
                                    var t = itemsText.length > count && itemsText[0] != "" ? itemsText[count] : undefined;
                                    var v = itemsValue.length > count && itemsValue[0] != "" ? itemsValue[count] : undefined;
                                    if (t != undefined && v != undefined) {
                                        items[count] = { text: t, value: v };
                                    }
                                    else if (t != undefined) {
                                        items[count] = { text: t };
                                    } else if (v != undefined) {
                                        items[count] = { value: v };
                                    }
                                }
                                cellType.items(items);
                                break;
                            case "CheckBoxCell":
                                cellType = new $.wijmo.wijspread.CheckBoxCellType();
                                if ($("#txtCheckBoxCellTextCaption").val() != "") {
                                    cellType.caption($("#txtCheckBoxCellTextCaption").val());
                                }
                                if ($("#txtCheckBoxCellTextTrue").val() != "") {
                                    cellType.textTrue($("#txtCheckBoxCellTextTrue").val());
                                }
                                if ($("#txtCheckBoxCellTextIndeterminate").val() != "") {
                                    cellType.textIndeterminate($("#txtCheckBoxCellTextIndeterminate").val());
                                }
                                if ($("#txtCheckBoxCellTextFalse").val() != "") {
                                    cellType.textFalse($("#txtCheckBoxCellTextFalse").val());
                                }
                                cellType.textAlign(parseInt($("#selCheckBoxCellAlign").val()));
                                cellType.isThreeState($("#ckbCheckBoxCellIsThreeState").attr("checked") === "checked" ? true : false);
                                break;
                            case "ButtonCell":
                                cellType = new $.wijmo.wijspread.ButtonCellType();
                                if ($("#txtButtonCellMarginLeft").val() != "") {
                                    cellType.marginLeft(parseFloat($("#txtButtonCellMarginLeft").val()));
                                }
                                if ($("#txtButtonCellMarginTop").val() != "") {
                                    cellType.marginTop(parseFloat($("#txtButtonCellMarginTop").val()));
                                }
                                if ($("#txtButtonCellMarginRight").val() != "") {
                                    cellType.marginRight(parseFloat($("#txtButtonCellMarginRight").val()));
                                }
                                if ($("#txtButtonCellMarginBottom").val() != "") {
                                    cellType.marginBottom(parseFloat($("#txtButtonCellMarginBottom").val()));
                                }
                                if ($("#txtButtonCellText").val() != "") {
                                    cellType.text($("#txtButtonCellText").val());
                                }
                                if ($("#txtButtonCellBackColor").val() != "") {
                                    cellType.buttonBackColor($("#txtButtonCellBackColor").val());
                                }
                                break;
                            case "HyperLinkCell":
                                cellType = new $.wijmo.wijspread.HyperLinkCellType();
                                if ($("#txtHyperLinkCellLinkColor").val() != "") {
                                    cellType.linkColor($("#txtHyperLinkCellLinkColor").val());
                                }
                                if ($("#txtHyperLinkCellVisitedLinkColor").val() != "") {
                                    cellType.visitedLinkColor($("#txtHyperLinkCellVisitedLinkColor").val());
                                }
                                if ($("#txtHyperLinkCellText").val() != "") {
                                    cellType.text($("#txtHyperLinkCellText").val());
                                }
                                if ($("#txtHyperLinkCellToolTip").val() != "") {
                                    cellType.linkToolTip($("#txtHyperLinkCellToolTip").val());
                                }
                                break;
                        }
                        sheet.isPaintSuspended(true);
                        sheet.suspendEvent();
                        var sels = sheet.getSelections();
                        for (var i = 0; i < sels.length; i++) {
                            var sel = sheet._getActualRange(sels[i]);
                            for (var r = 0; r < sel.rowCount; r++) {
                                for (var c = 0; c < sel.colCount; c++) {
                                    sheet.setCellType(sel.row + r, sel.col + c, cellType, $.wijmo.wijspread.SheetArea.viewport);
                                }
                            }
                        }
                        sheet.resumeEvent();
                        sheet.isPaintSuspended(false);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#colordialog").dialog({
                autoOpen: false,
                height: 420,
                width: 485,
                modal: true,
                resizable: false,
                buttons: {
                    Select: function () {
                        var spread = $("#ss").wijspread("spread");
                        var sheet = spread.getActiveSheet();
                        sheet.isPaintSuspended(true);
                        var title = $("#colordialog").dialog("option", "title");
                        if (title == "Back Color") {
                            var sels = sheet.getSelections();
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).backColor($("#selectedColor").val());
                            }
                        } else if (title == "Fore Color") {
                            var sels = sheet.getSelections();
                            for (var n = 0; n < sels.length; n++) {
                                var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                                sheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).foreColor($("#selectedColor").val());
                            }
                        } else if (title == "Tab Strip Color") {
                            if (sheet) {
                                var color = $("#selectedColor").val();
                                sheet.sheetTabColor(color);
                            }
                        } else if (title == "FrozenLineColor") {
                            if (sheet) {
                                var color = $("#selectedColor").val();
                                sheet.frozenlineColor(color);
                            }
                        } else if (title == "Bar Color") {
                            barcolor = $("#selectedColor").val();
                            $("#color").css("background", barcolor);
                        } else if (title == "Bar Border Color") {
                            barbordercolor = $("#selectedColor").val();
                            $("#borderColor").css("background", barbordercolor);
                        } else if (title == "Bar Negative Fill Color") {
                            barnegativefillcolor = $("#selectedColor").val();
                            $("#negativeFillColor").css("background", barnegativefillcolor);
                        } else if (title == "Bar Negative Border Color") {
                            barnegativebordercolor = $("#selectedColor").val();
                            $("#negativeBorderColor").css("background", barnegativebordercolor);
                        } else if (title == "Bar Axis Color") {
                            baraxiscolor = $("#selectedColor").val();
                            $("#axisColor").css("background", baraxiscolor);
                        } else if (title == "Min Color") {
                            mincolor = $("#selectedColor").val();
                            $("#minColor").css("background", mincolor);
                        } else if (title == "Mid Color") {
                            midcolor = $("#selectedColor").val();
                            $("#midColor").css("background", mincolor);
                        } else if (title == "Max Color") {
                            maxcolor = $("#selectedColor").val();
                            $("#maxColor").css("background", maxcolor);
                        }
                        sheet.isPaintSuspended(false);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#leftSide").change(function () {
                var checked = $("#leftSide").prop("checked");
                if (checked) {
                    $("#border11").css("border-left", "1px solid gray");
                    $("#border21").css("border-left", "1px solid gray");
                    $("#bordertable").css("border-left", "1px solid gray");
                } else {
                    $("#border11").css("border-left", "none");
                    $("#border21").css("border-left", "none");
                    $("#bordertable").css("border-left", "none");
                }
            });
            $("#topSide").change(function () {
                var checked = $("#topSide").prop("checked");
                if (checked) {
                    $("#border11").css("border-top", "1px solid gray");
                    $("#border12").css("border-top", "1px solid gray");
                    $("#bordertable").css("border-top", "1px solid gray");
                } else {
                    $("#border11").css("border-top", "none");
                    $("#border12").css("border-top", "none");
                    $("#bordertable").css("border-top", "none");
                }
            });
            $("#rightSide").change(function () {
                var checked = $("#rightSide").prop("checked");
                if (checked) {
                    $("#border12").css("border-right", "1px solid gray");
                    $("#border22").css("border-right", "1px solid gray");
                    $("#bordertable").css("border-right", "1px solid gray");
                } else {
                    $("#border12").css("border-right", "none");
                    $("#border22").css("border-right", "none");
                    $("#bordertable").css("border-right", "none");
                }
            });
            $("#bottomSide").change(function () {
                var checked = $("#bottomSide").prop("checked");
                if (checked) {
                    $("#border21").css("border-bottom", "1px solid gray");
                    $("#border22").css("border-bottom", "1px solid gray");
                    $("#bordertable").css("border-bottom", "1px solid gray");
                } else {
                    $("#border21").css("border-bottom", "none");
                    $("#border22").css("border-bottom", "none");
                    $("#bordertable").css("border-bottom", "none");
                }
            });
            $("#hInside").change(function () {
                var checked = $("#hInside").prop("checked");
                if (checked) {
                    $("#border11").css("border-bottom", "1px solid gray");
                    $("#border12").css("border-bottom", "1px solid gray");
                    $("#border21").css("border-top", "1px solid gray");
                    $("#border22").css("border-top", "1px solid gray");
                } else {
                    $("#border11").css("border-bottom", "none");
                    $("#border12").css("border-bottom", "none");
                    $("#border21").css("border-top", "none");
                    $("#border22").css("border-top", "none");
                }
            });
            $("#vInside").change(function () {
                var checked = $("#vInside").prop("checked");
                if (checked) {
                    $("#border11").css("border-right", "1px solid gray");
                    $("#border12").css("border-left", "1px solid gray");
                    $("#border21").css("border-right", "1px solid gray");
                    $("#border22").css("border-left", "1px solid gray");
                } else {
                    $("#border11").css("border-right", "none");
                    $("#border12").css("border-left", "none");
                    $("#border21").css("border-right", "none");
                    $("#border22").css("border-left", "none");
                }
            });
            $("#borderdialog").dialog({
                autoOpen: false,
                height: 450,
                width: 450,
                modal: true,
                resizable: false,
                buttons: {
                    OK: function () {
                        var spread = $("#ss").wijspread("spread");
                        var sheet = spread.getActiveSheet();
                        sheet.isPaintSuspended(true);
                        var sels = sheet.getSelections();
                        //var lineBorder = new $.wijmo.wijspread.LineBorder($("#lineColor").val(), $("#lineStyle").val());
                        var lineBorder = new $.wijmo.wijspread.LineBorder($("#lineColor").val(), $.wijmo.wijspread.LineStyle[$("#lineStyle").val()]);
                        for (var n = 0; n < sels.length; n++) {
                            var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount());
                            sheet.setBorder(sel, lineBorder, {
                                left: $("#leftSide").attr("checked") == "checked",
                                top: $("#topSide").attr("checked") == "checked",
                                right: $("#rightSide").attr("checked") == "checked",
                                bottom: $("#bottomSide").attr("checked") == "checked",
                                innerHorizontal: $("#hInside").attr("checked") == "checked",
                                innerVertical: $("#vInside").attr("checked") == "checked"
                            });
                        }
                        sheet.isPaintSuspended(false);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });

            $("#ss").wijspread({ sheetCount: 2 }); // create wijspread control
            if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
                //run for ie7/8
                var spread = $("#ss").wijspread("spread");
                spread.bind("SpreadsheetObjectLoaded", function () {
                    initSpread();
                });
            } else {
                initSpread();
            }
        });

        function initSpread() {
            var spread = $("#ss").wijspread("spread"); // get instance of wijspread control
            var sheet = spread.getActiveSheet(); // get active worksheet of the wijspread control
            spread.useWijmoTheme = true;
            spread.repaint();
            spread.bind("EnterCell", selectionChanged);
            //show range group
            $("#showRowGroup").attr("disabled", true);
            $("#showColGroup").attr("disabled", true);
            $("#showRowGroupLabel").attr("disabled", true);
            $("#showColGroupLabel").attr("disabled", true);
            $("#showRowGroup").attr("value", "false");
            $("#showColGroup").attr("value", "false");
            $("#showRowGroupLabel").addClass("ui-state-disabled");
            $("#showColGroupLabel").addClass("ui-state-disabled");
            $("#showRowGroupLabel").bind("mouseup", rowlabelactive);
            $("#showColGroupLabel").bind("mouseup", collabelactive);

            $("#setTabStripColor").attr("disabled", true);
            $("#setTabStripColor").addClass("ui-state-disabled");
            //rapid input mode start
            setstatus(sheet);
            sheet.bind($.wijmo.wijspread.Events.EditorStatusChanged, function () {
                setstatus(sheet);
            });
            //rapid input mode end
            sheet.isPaintSuspended(true);
            sheet.addSpan(0, 1, 1, 10);
            sheet.setRowHeight(0, 40);
            sheet.setValue(0, 1, "Sale Data Analysis");
            sheet.getCell(0, 1).font("bold 30px arial");
            sheet.getCell(0, 1).vAlign($.wijmo.wijspread.VerticalAlign.center);
            sheet.addSpan(1, 1, 1, 3);
            sheet.setValue(1, 1, "Store");
            sheet.addSpan(1, 4, 1, 7);
            sheet.setValue(1, 4, "Goods");
            sheet.addSpan(2, 1, 1, 2);
            sheet.setValue(2, 1, "Area");
            sheet.addSpan(2, 3, 2, 1);
            sheet.setValue(2, 3, "ID");
            sheet.addSpan(2, 4, 1, 2);
            sheet.setValue(2, 4, "Fruits");
            sheet.addSpan(2, 6, 1, 2);
            sheet.setValue(2, 6, "Vegetables");
            sheet.addSpan(2, 8, 1, 2);
            sheet.setValue(2, 8, "Foods");
            sheet.addSpan(2, 10, 2, 1);
            sheet.setValue(2, 10, "Total");
            sheet.setValue(3, 1, "State");
            sheet.setValue(3, 2, "City");
            sheet.setValue(3, 4, "Grape");
            sheet.setValue(3, 5, "Apple");
            sheet.setValue(3, 6, "Potato");
            sheet.setValue(3, 7, "Tomato");
            sheet.setValue(3, 8, "Sandwich");
            sheet.setValue(3, 9, "Hamburger");

            sheet.addSpan(4, 1, 7, 1);
            sheet.addSpan(4, 2, 3, 1);
            sheet.addSpan(7, 2, 3, 1);
            sheet.addSpan(10, 2, 1, 2);
            sheet.setValue(10, 2, "Sub Total:");
            sheet.addSpan(11, 1, 7, 1);
            sheet.addSpan(11, 2, 3, 1);
            sheet.addSpan(14, 2, 3, 1);
            sheet.addSpan(17, 2, 1, 2);
            sheet.setValue(17, 2, "Sub Total:");
            sheet.addSpan(18, 1, 1, 3);
            sheet.setValue(18, 1, "Total:");

            sheet.setValue(4, 1, "NC");
            sheet.setValue(4, 2, "Raleigh");
            sheet.setValue(7, 2, "Charlotte");
            sheet.setValue(4, 3, "001");
            sheet.setValue(5, 3, "002");
            sheet.setValue(6, 3, "003");
            sheet.setValue(7, 3, "004");
            sheet.setValue(8, 3, "005");
            sheet.setValue(9, 3, "006");
            sheet.setValue(11, 1, "PA");
            sheet.setValue(11, 2, "Philadelphia");
            sheet.setValue(14, 2, "Pittsburgh");
            sheet.setValue(11, 3, "007");
            sheet.setValue(12, 3, "008");
            sheet.setValue(13, 3, "009");
            sheet.setValue(14, 3, "010");
            sheet.setValue(15, 3, "011");
            sheet.setValue(16, 3, "012");

            sheet.setFormula(10, 4, "=SUM(E5:E10)");
            sheet.setFormula(10, 5, "=SUM(F5:F10)");
            sheet.setFormula(10, 6, "=SUM(G5:G10)");
            sheet.setFormula(10, 7, "=SUM(H5:H10)");
            sheet.setFormula(10, 8, "=SUM(I5:I10)");
            sheet.setFormula(10, 9, "=SUM(J5:J10)");

            sheet.setFormula(17, 4, "=SUM(E12:E17)");
            sheet.setFormula(17, 5, "=SUM(F12:F17)");
            sheet.setFormula(17, 6, "=SUM(G12:G17)");
            sheet.setFormula(17, 7, "=SUM(H12:H17)");
            sheet.setFormula(17, 8, "=SUM(I12:I17)");
            sheet.setFormula(17, 9, "=SUM(J12:J17)");

            for (var i = 0; i < 14; i++) {
                sheet.setFormula(4 + i, 10, "=SUM(E" + (5 + i).toString() + ":J" + (5 + i).toString() + ")");
            }

            sheet.setFormula(18, 4, "=E11+E18");
            sheet.setFormula(18, 5, "=F11+F18");
            sheet.setFormula(18, 6, "=G11+G18");
            sheet.setFormula(18, 7, "=H11+H18");
            sheet.setFormula(18, 8, "=I11+I18");
            sheet.setFormula(18, 9, "=J11+J18");
            sheet.setFormula(18, 10, "=K11+K18");

            sheet.getCells(1, 1, 3, 10).backColor("#D9D9FF");
            sheet.getCells(4, 1, 18, 3).backColor("#D9FFD9");
            sheet.getCells(1, 1, 3, 10).hAlign($.wijmo.wijspread.HorizontalAlign.center);

            sheet.setBorder(new $.wijmo.wijspread.Range(1, 1, 18, 10), new $.wijmo.wijspread.LineBorder("Black", $.wijmo.wijspread.LineStyle.thin), { all: true });
            sheet.setBorder(new $.wijmo.wijspread.Range(4, 4, 3, 6), new $.wijmo.wijspread.LineBorder("Green", $.wijmo.wijspread.LineStyle.dotted), { innerHorizontal: true });
            sheet.setBorder(new $.wijmo.wijspread.Range(7, 4, 3, 6), new $.wijmo.wijspread.LineBorder("Green", $.wijmo.wijspread.LineStyle.dotted), { innerHorizontal: true });
            sheet.setBorder(new $.wijmo.wijspread.Range(11, 4, 3, 6), new $.wijmo.wijspread.LineBorder("Green", $.wijmo.wijspread.LineStyle.dotted), { innerHorizontal: true });
            sheet.setBorder(new $.wijmo.wijspread.Range(14, 4, 3, 6), new $.wijmo.wijspread.LineBorder("Green", $.wijmo.wijspread.LineStyle.dotted), { innerHorizontal: true });

            fillSampleData(sheet, new $.wijmo.wijspread.Range(4, 4, 6, 6));
            fillSampleData(sheet, new $.wijmo.wijspread.Range(11, 4, 6, 6));

            sheet.isPaintSuspended(false);
        }
        ;
        function rowlabelactive() {
            $("#showRowGroupLabel").addClass("ui-state-active");
        }
        function rowlabelnoactive() {
            $("#showRowGroupLabel").removeClass("ui-state-active");
        }
        function collabelactive() {
            $("#showColGroupLabel").addClass("ui-state-active");
        }
        function collabelnoactive() {
            $("#showColGroupLabel").removeClass("ui-state-active");
        }
        function IsRangeSet() {
            if (validatorType == "NumberValidator" || validatorType == "TextLengthValidator") {
                if ($("#txtValidatorValue1").val().length == 0 || $("#txtValidatorValue2").val().length == 0) {
                    alert("You must enter both a Maximum and Minimum.");
                }
            } else if (validatorType == "DateValidator") {
                if ($("#txtValidatorValue1").val().length == 0 || $("#txtValidatorValue2").val().length == 0) {
                    alert("You must enter both a End Date and a Start Date.");
                }
            } else if (validatorType == "ListValidator") {
                if ($("#txtValidatorValue").val().length == 0) {
                    alert("You must enter a Source.");
                }
            } else if (validatorType == "FormulaListValidator" || validatorType == "FormulaValidator") {
                if ($("#txtValidatorValue").val().length == 0) {
                    alert("You must enter a Formula.");
                }
            }
        }
        function setstatus(sheet) {
            var statusnow = sheet.editorStatus();
            if (statusnow === $.wijmo.wijspread.EditorStatus.Ready) {
                $("#rapidInputMode").val("Ready");
            } else if (statusnow === $.wijmo.wijspread.EditorStatus.Enter) {
                $("#rapidInputMode").val("Enter");
            } else if (statusnow === $.wijmo.wijspread.EditorStatus.Edit) {
                $("#rapidInputMode").val("Edit");
            }
        }
        function setBarColor() {
            $("#colordialog").dialog({ title: "Bar Color" });
            $("#colordialog").dialog("open");
        }
        function setBarBorderColor() {
            $("#colordialog").dialog({ title: "Bar Border Color" });
            $("#colordialog").dialog("open");
        }
        function setBarNegativeFillColor() {
            $("#colordialog").dialog({ title: "Bar Negative Fill Color" });
            $("#colordialog").dialog("open");
        }
        function setBarNegativeBorderColor() {
            $("#colordialog").dialog({ title: "Bar Negative Border Color" });
            $("#colordialog").dialog("open");
        }
        function setBarAxisColor() {
            $("#colordialog").dialog({ title: "Bar Axis Color" });
            $("#colordialog").dialog("open");
        }

        function setminColor() {
            $("#colordialog").dialog({ title: "Min Color" });
            $("#colordialog").dialog("open");
        }
        function setmidColor() {
            $("#colordialog").dialog({ title: "Mid Color" });
            $("#colordialog").dialog("open");
        }
        function setmaxColor() {
            $("#colordialog").dialog({ title: "Max Color" });
            $("#colordialog").dialog("open");
        }
        function parseValue(value) {
            if (!isNaN(value) && isFinite(value)) {
                return parseFloat(value);
            } else {
                return value;
            }
        }
        function getSelected(v1, v2) {
            return v1 === v2 ? "selected='selected'" : "";
        }
        function createIconCriteriaDOM() {
            var IconSetType = $.wijmo.wijspread.IconSetType,
                    IconCriterion = $.wijmo.wijspread.IconCriterion,
                    IconValueType = $.wijmo.wijspread.IconValueType;
            var iconSetType = parseInt($("#iconSetType").val());
            var iconCriteria = [];
            if (iconSetType >= IconSetType.ThreeArrowsColored &&
                    iconSetType <= IconSetType.ThreeSymbolsUncircled) {
                iconCriteria = new Array(2);
                iconCriteria[0] = new IconCriterion(true, IconValueType.Percent, 33);
                iconCriteria[1] = new IconCriterion(true, IconValueType.Percent, 67);
            }
            else if (iconSetType >= IconSetType.FourArrowsColored &&
                    iconSetType <= IconSetType.FourTrafficLights) {
                iconCriteria = new Array(3);
                iconCriteria[0] = new IconCriterion(true, IconValueType.Percent, 25);
                iconCriteria[1] = new IconCriterion(true, IconValueType.Percent, 50);
                iconCriteria[2] = new IconCriterion(true, IconValueType.Percent, 75);
            }
            else if (iconSetType >= IconSetType.FiveArrowsColored &&
                    iconSetType <= IconSetType.FiveBoxes) {
                iconCriteria = new Array(4);
                iconCriteria[0] = new IconCriterion(true, IconValueType.Percent, 20);
                iconCriteria[1] = new IconCriterion(true, IconValueType.Percent, 40);
                iconCriteria[2] = new IconCriterion(true, IconValueType.Percent, 60);
                iconCriteria[3] = new IconCriterion(true, IconValueType.Percent, 80);
            }

            $("#iconCriteriaSetting").empty();
            $.each(iconCriteria, function (i, v) {
                var $div = $("<div style='margin-top: 10px'></div>"),
                        $selectOperator = $("<select></select>"),
                        $input = $("<input style='margin-left: 10px'/>"),
                        $selectType = $("<select style='margin-left: 10px'></select>");
                $selectOperator.html("<option value=1 " + getSelected(v.isGreaterThanOrEqualTo, true) + ">>=</option>" +
                        "<option value=0 " + getSelected(v.isGreaterThanOrEqualTo, false) + ">></option>");
                $input.val(v.iconValue);
                $selectType.html("<option value=1 " + getSelected(v.iconValueType, 1) + ">Number</option>" +
                        "<option value=4 " + getSelected(v.iconValueType, 4) + ">Percent</option>" +
                        "<option value=7 " + getSelected(v.iconValueType, 7) + ">Formula</option>" +
                        "<option value=5 " + getSelected(v.iconValueType, 5) + ">Percentile</option>");
                $div.append($selectOperator).append($input).append($selectType);
                $("#iconCriteriaSetting").append($div);
            });
        }
        function noborderclick() {
            $(":checkbox").removeAttr("checked");
            $("#border11").css("border-right", "none");
            $("#border11").css("border-bottom", "none");
            $("#border11").css("border-left", "none");
            $("#border11").css("border-top", "none");
            $("#border12").css("border-right", "none");
            $("#border12").css("border-bottom", "none");
            $("#border12").css("border-left", "none");
            $("#border12").css("border-top", "none");
            $("#border21").css("border-right", "none");
            $("#border21").css("border-bottom", "none");
            $("#border21").css("border-left", "none");
            $("#border21").css("border-top", "none");
            $("#border22").css("border-right", "none");
            $("#border22").css("border-bottom", "none");
            $("#border22").css("border-left", "none");
            $("#border22").css("border-top", "none");
            $("#bordertable").css("border", "none");
        }
        function outlineborderclick() {
            $(":checkbox").removeAttr("checked");
            $("[id*=Side]").attr("checked", true);
            $("#border11").css("border-left", "1px solid gray");
            $("#border11").css("border-top", "1px solid gray");
            $("#border12").css("border-right", "1px solid gray");
            $("#border12").css("border-top", "1px solid gray");
            $("#border21").css("border-left", "1px solid gray");
            $("#border21").css("border-bottom", "1px solid gray");
            $("#border22").css("border-bottom", "1px solid gray");
            $("#border22").css("border-right", "1px solid gray");
            $("#border11").css("border-right", "none");
            $("#border11").css("border-bottom", "none");
            $("#border12").css("border-left", "none");
            $("#border12").css("border-bottom", "none");
            $("#border21").css("border-right", "none");
            $("#border21").css("border-top", "none");
            $("#border22").css("border-top", "none");
            $("#border22").css("border-left", "none");
            $("#bordertable").css("border", "1px solid gray");
        }

        function allborderclick() {
            $(":checkbox").attr("checked", true);
            $("#border11").css("border-right", "1px solid gray");
            $("#border11").css("border-bottom", "1px solid gray");
            $("#border11").css("border-left", "1px solid gray");
            $("#border11").css("border-top", "1px solid gray");
            $("#border12").css("border-right", "1px solid gray");
            $("#border12").css("border-bottom", "1px solid gray");
            $("#border12").css("border-left", "1px solid gray");
            $("#border12").css("border-top", "1px solid gray");
            $("#border21").css("border-right", "1px solid gray");
            $("#border21").css("border-bottom", "1px solid gray");
            $("#border21").css("border-left", "1px solid gray");
            $("#border21").css("border-top", "1px solid gray");
            $("#border22").css("border-right", "1px solid gray");
            $("#border22").css("border-bottom", "1px solid gray");
            $("#border22").css("border-left", "1px solid gray");
            $("#border22").css("border-top", "1px solid gray");
            $("#bordertable").css("border", "1px solid gray");
        }

        function setEnumTypeOfCF(rule, type) {
            switch (rule) {
                case "0":
                    $("#ruletext").text("Format only cells with:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    type.show();
                    $("#ComparisonOperator1").show();
                    $("#value1").show();
                    $("#value1").val("");
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>EqualsTo</option>");
                    type.append("<option value='1'>NotEqualsTo</option>");
                    type.append("<option value='2'>GreaterThan</option>");
                    type.append("<option value='3'>GreaterThanOrEqualsTo</option>");
                    type.append("<option value='4'>LessThan</option>");
                    type.append("<option value='5'>LessThanOrEqualsTo</option>");
                    type.append("<option value='6'>Between</option>");
                    type.append("<option value='7'>NotBetween</option>");
                    break;
                case "1":
                    $("#ruletext").text("Format only cells with:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    type.show();
                    $("#ComparisonOperator1").show();
                    $("#value1").show();
                    $("#value1").val("");
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>Contains</option>");
                    type.append("<option value='1'>DoesNotContain</option>");
                    type.append("<option value='2'>BeginsWith</option>");
                    type.append("<option value='3'>EndsWith</option>");
                    break;
                case "2":
                    $("#ruletext").text("Format only cells with:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    type.show();
                    $("#ComparisonOperator1").show();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>Today</option>");
                    type.append("<option value='1'>Yesterday</option>");
                    type.append("<option value='2'>Tomorrow</option>");
                    type.append("<option value='3'>Last7Days</option>");
                    type.append("<option value='4'>ThisMonth</option>");
                    type.append("<option value='5'>LastMonth</option>");
                    type.append("<option value='6'>NextMonth</option>");
                    type.append("<option value='7'>ThisWeek</option>");
                    type.append("<option value='8'>LastWeek</option>");
                    type.append("<option value='9'>NextWeek</option>");
                    break;
                case "3":
                    $("#ruletext").text("Format values where this formula is true:");
                    $("#andtext").hide();
                    $("#formattext").show();
                    $("#formattext").text("eg:=COUNTIF($B$1:$B$5,A1).");
                    type.empty();
                    $("#ComparisonOperator1").hide();
                    $("#value1").show();
                    $("#value1").val("");
                    $("#value2").hide();
                    $("#colorScale").hide();
                    break;
                case "4":
                    $("#ruletext").text("Format values that rank in the:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    $("#ComparisonOperator1").show();
                    $("#value1").show();
                    $("#value1").val("10");
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>Top</option>");
                    type.append("<option value='1'>Bottom</option>");
                    break;
                case "5":
                    $("#ruletext").text("Format all:");
                    $("#andtext").hide();
                    $("#formattext").show();
                    $("#formattext").text("values in the selected range.");
                    type.empty();
                    $("#ComparisonOperator1").hide();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").hide();
                    break;
                case "6":
                    $("#ruletext").text("Format all:");
                    $("#andtext").hide();
                    $("#formattext").show();
                    $("#formattext").text("values in the selected range.");
                    type.empty();
                    $("#ComparisonOperator1").hide();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").hide();
                    break;
                case "7":
                    $("#ruletext").text("Format values that are:");
                    $("#andtext").hide();
                    $("#formattext").show();
                    $("#formattext").text("the average for selected range.");
                    type.empty();
                    type.show();
                    $("#ComparisonOperator1").show();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>Above</option>");
                    type.append("<option value='1'>Below</option>");
                    type.append("<option value='2'>EqualOrAbove</option>");
                    type.append("<option value='3'>EqualOrBelow</option>");
                    type.append("<option value='4'>Above1StdDev</option>");
                    type.append("<option value='5'>Below1StdDev</option>");
                    type.append("<option value='6'>Above2StdDev</option>");
                    type.append("<option value='7'>Below2StdDev</option>");
                    type.append("<option value='8'>Above3StdDev</option>");
                    type.append("<option value='9'>Below3StdDev</option>");
                    break;
                case "8":
                    $("#ruletext").text("Format all cells based on their values:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    type.hide();
                    $("#ComparisonOperator1").hide();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").show();
                    $("#midpoint").hide();
                    $("#midType").hide();
                    $("#midValue").hide();
                    $("#midColor").hide();
                    break;
                case "9":
                    $("#ruletext").text("Format all cells based on their values:");
                    $("#andtext").hide();
                    $("#formattext").hide();
                    type.empty();
                    type.hide();
                    $("#ComparisonOperator1").hide();
                    $("#value1").hide();
                    $("#value2").hide();
                    $("#colorScale").show();
                    $("#midpoint").show();
                    $("#midType").show();
                    $("#midValue").show();
                    $("#midColor").show();
                    break;
                default:
                    $("#andtext").hide();
                    type.empty();
                    type.show();
                    $("#ComparisonOperator1").show();
                    $("#value1").show();
                    $("#value1").val("");
                    $("#value2").hide();
                    $("#colorScale").hide();
                    type.append("<option value='0'>EqualsTo</option>");
                    type.append("<option value='1'>NotEqualsTo</option>");
                    type.append("<option value='2'>GreaterThan</option>");
                    type.append("<option value='3'>GreaterThanOrEqualsTo</option>");
                    type.append("<option value='4'>LessThan</option>");
                    type.append("<option value='5'>LessThanOrEqualsTo</option>");
                    type.append("<option value='6'>Between</option>");
                    type.append("<option value='7'>NotBetween</option>");
                    break;
            }
        }

        function fillSampleData(sheet, range) {
            for (var i = 0; i < range.rowCount; i++) {
                for (var j = 0; j < range.colCount; j++) {
                    sheet.setValue(range.row + i, range.col + j, Math.ceil(Math.random() * 300));
                }
            }
        }

        function colorSelected(event) {
            var event = event || window.event;
            var target = event.srcElement || event.target;
            $("#selectedColor").val(target.bgColor);
            $("#colorSample").attr("style", "background-color:" + target.bgColor);
        }

        function getActualCellRange(cellRange, rowCount, columnCount) {
            if (cellRange.row == -1 && cellRange.col == -1) {
                return new $.wijmo.wijspread.Range(0, 0, rowCount, columnCount);
            }
            else if (cellRange.row == -1) {
                return new $.wijmo.wijspread.Range(0, cellRange.col, rowCount, cellRange.colCount);
            }
            else if (cellRange.col == -1) {
                return new $.wijmo.wijspread.Range(cellRange.row, 0, cellRange.rowCount, columnCount);
            }

            return cellRange;
        }
        

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-208280-14']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();