var ServiceURLS = {
	newestQuestionsUrl:'/restserver/index.php/api/testcenter/list_questions/format/json',
	updateMultipleChoiceQuestionUrl:'/restserver/index.php/api/testcenter/update_mc_question',
	deleteQuestionUrl:'/restserver/index.php/api/testcenter/delete_question/format/json/',
	loadQuestionUrl:'/restserver/index.php/api/testcenter/list_questions/format/json/',
	newChoiceUrl:'/restserver/index.php/api/testcenter/set_choice/format/json/',
	deleteChoiceUrl:'/restserver/index.php/api/testcenter/delete_choice/format/json/',
	listQuestionsByUserUrl:'/restserver/index.php/api/testcenter/question_ids/format/json/'
};
var BrowseSearchModule = angular.module("BrowseSearchModule", ['ui.bootstrap', 'ngSanitize', "TopicsModule"]).
  config(function($routeProvider) {
  	$routeProvider
	  	.when('/questions/newest',
	        {
	            controller: 'BrowseSearchController',
	            templateUrl: 'views/form-browse-search.php'
	        })
	  	.when('/question/multiple-choice/edit/:qID',
		  	{
				controller:'MultipleChoiceEditController',
				templateUrl:'views/form-edit-multiple-choice.php'	
		  	})
	    .otherwise({ redirectTo: '/questions/newest' });
  });

BrowseSearchModule.service("QuestionsFeed", function($q, $http, $location, TopicService) {
	this.returnToListAfterSave = true;
	this.lastUpdated = null;
	this.lastDeleted = null;
	this.topics = TopicService;
	var self = this;

	this.alerts = [];

	this.getNewestQuestions = function() {
		var promise = $q.defer();
		$http({method:'GET', url: ServiceURLS.newestQuestionsUrl})
			.success(function (data, status, headers, config) {
				self.questions = data.questions;
				promise.resolve(self.questions);
			});
		return promise.promise;
	};
	this.submitUpdate = function() {
		self.lastDeleted = null;
		self.lastUpdated = self.editQuestion.id;
		self.alerts = [{type:"alert-info", message:"Updating question..."}];
		return $http.post(ServiceURLS.updateMultipleChoiceQuestionUrl, {"question":self.editQuestion})
			.success(function (data, status, headers, config) {
				//self.lastUpdated = data.id;
				if(self.returnToListAfterSave) {
					$location.path('/questions/newest');
					self.alerts = [];
				} else {
					self.alerts = [{type:"alert-success", message:"Question updated successfully."}];
				}
			});
	};
	this.deleteQuestion = function() {
		self.alerts = [{type:"alert-info", message:"Deleting question..."}];
		$http.get(ServiceURLS.deleteQuestionUrl + "?q=" + self.editQuestion.id)
			.success(function (data, status, headers, config) {
				//self.lastUpdated = data.id;
				if(self.feedMode == "default") {
					for(var i = 0; i < self.questions.length ;i++) {
						if(self.questions[i].id == self.editQuestion.id) {
							self.questions.splice(i, 1);
						}
					}
					self.lastUpdated = null;
					self.lastDeleted = self.editQuestion.id;
					$location.path('/questions/newest');
					self.alerts = [];
				} else {
					var tmpIndex = self.listIndex;
					self.loadUserQuestions(self.currentUserList);
					self.listIndex = tmpIndex;
				}
			});
	};

	this.loadQuestion = function(qID) {
		var promise = $q.defer();
		self.alerts = [{type:"alert-info", message:"Loading question..."}];
		$http({method:'GET', url: ServiceURLS.loadQuestionUrl + "?edit=" + qID}).success(function (data, status, headers, config) {
				self.editQuestion = data.questions[0];
				/*for(var i = 0; i < self.questions.length ;i++) {
					if(self.questions[i].id == qID) {
						self.editQuestion = self.questions[i];*/
				if(self.editQuestion.topics.length != null) {
					self.topics.setTopics(self.editQuestion.topics);
				}
						self.alerts = [];
						promise.resolve(self.editQuestion);
					/*}
				}*/
			}).error(function() {
				self.alerts = [{type:"alert-danger", message:"Could not load this question."}];
			});
		return promise.promise;
	};

	this.addChoice = function(choice) {
		self.alerts = [{type:"alert-info", message:"Saving choice..."}];
		$http.post(ServiceURLS.newChoiceUrl, {"choice":choice, "questionID":self.editQuestion.id})
			.success(function (data, status, headers, config) {
				var newChoice= data.choice[0];
				self.editQuestion.choices.push(newChoice);
				self.submitUpdate();
			});
	}

	this.deleteChoice = function(choiceID) {
		$http({method:'GET', url: ServiceURLS.deleteChoiceUrl + "?id=" + choiceID}).success(function (data, status, headers, config) {
				for(var i = 0; i < self.editQuestion.choices.length; i++) {
					if(self.editQuestion.choices[i].id == choiceID) {
						self.editQuestion.choices.splice(i, 1);
						break;
						self.alerts = [{type:"alert-success", message:".Choice deleted."}];
					}
				}
			});
	};

	this.feedMode = "default";
	this.userlist = [];
	this.listIndex = 0;
	this.currentUserList = "List by Username";


	this.loadUserQuestions = function(name) {
		self.currentUserList = name;
		self.alerts = [{type:"alert-info", message:"Fetching user's question list..."}];
		$http({method:'GET', url: ServiceURLS.listQuestionsByUserUrl + "?username=" + name}).success(function (data, status, headers, config) {
				self.userlist = data.questions;
				self.questions = self.userlist;
				self.editQuestion = null;
				self.listIndex = 0;
				self.alerts = [{type:"alert-success", message:"User list loaded."}];
				$location.path('/question/multiple-choice/edit/' + self.userlist[self.listIndex].id);
			});
	};
	this.previousQuestionInUserList = function() {
		if(self.listIndex > 0 ) {
			self.listIndex--;
			self.editQuestion = null;
			$location.path('/question/multiple-choice/edit/' + self.userlist[self.listIndex].id);
		}
	};
	this.nextQuestionInUserList = function() {
		if(self.listIndex < self.userlist.length ) {
			self.listIndex++;
			self.editQuestion = null;
			$location.path('/question/multiple-choice/edit/' + self.userlist[self.listIndex].id);
		}
	};
	this.jumpToPosition = function() {
		self.editQuestion = null;
		$location.path('/question/multiple-choice/edit/' + self.userlist[self.listIndex].id);
	};
/*
	this.typeFilter = [{"label": "Multiple Choice", "type":"multuple-choice"},
						{"label": "Multiple Choice", "type":"multuple-choice"},
						];

						*/


});


BrowseSearchModule.controller("BrowseSearchController", function($scope, $location, QuestionsFeed) {
	//$scope.model = {};

	$scope.feed = QuestionsFeed;
	if($scope.feed.questions == null) {
		$scope.questions = QuestionsFeed.getNewestQuestions();
	} else {
		$scope.questions = $scope.feed.questions;
	}

	$scope.editQuestion = function(question) {
		$scope.feed.editQuestion = question;
		$scope.feed.editQuestion.document.title = $scope.feed.editQuestion.document.url.split("\/testmodule-admin\/docs\/")[1];
		$location.path("/question/multiple-choice/edit/" + question.id);
	};
});

BrowseSearchModule.controller("MultipleChoiceEditController", function($scope, $rootScope, $routeParams, 
																	$location, $dialog, $templateCache, QuestionsFeed, TopicService) {
	$scope.question = QuestionsFeed.editQuestion;
	$scope.topics = TopicService;
	if($scope.question == null) {
		var qID = $routeParams.qID;
		$scope.question = QuestionsFeed.loadQuestion(qID);
		QuestionsFeed.returnToListAfterSave = false;
	}

	$scope.feed = QuestionsFeed;
	$scope.deprecateQuestion = function() {
		$scope.question.deprecated = !$scope.question.deprecated;
	}
	$scope.isLoading = false;
	$scope.$watch(function() {
		return QuestionsFeed.editQuestion;
	}, function(n) {
		if(n != null && n.$$v == null) {
			$scope.question = QuestionsFeed.editQuestion;
			$scope.question.document.title = $scope.question.document.url.split("\/testmodule-admin\/docs\/")[1];
			$scope.initEditor();
		}
	});

	$scope.launchInQuiz = function() {
		var data = {"questions":{"multipleChoice":[$scope.question.id],"tbs":[]},
				"topics": $scope.question.topics
				};
		data = JSON.stringify(data);
		if ($scope.question.id)
		window.open("/testmodule/setup.php?data=" + encodeURIComponent(data) + "#/preview/" + $scope.question.type + "/" + $scope.question.id, "_blank");
	};

	$scope.initEditor = function() {

		setTimeout(function() {
console.log('pear');
			tinymce.init({
			    selector: "#questionText",
			    theme: "modern",
			    plugins: [
			        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
			        "searchreplace wordcount visualblocks visualchars code fullscreen",
			        "insertdatetime media nonbreaking save table contextmenu directionality",
			        "emoticons template paste textcolor jbimages"
			    ],
			    statusbar:false,
			    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | forecolor backcolor emoticons",
			    image_advtab: true,
			    entity_encoding: "raw",
			    setup:function(ed) {
			    	ed.on("init", function(){
						$scope.qText = ed;
						$scope.qText.setContent($scope.feed.editQuestion.question);
			    	});
			    }	
			});
		}, 100);
	}

	$scope.showChoiceEditForm = function(c) {
		$scope.feed.editChoice = c;
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/form-edit-multiplechoice-option.php',
		    controller: 'EditChoiceController'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};

	$scope.showNewChoiceForm = function () {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/form-edit-multiplechoice-option.php',
		    controller: 'NewChoiceController'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};

	$scope.updateQuestion = function() {
		$scope.feed.editQuestion.question = $scope.qText.getContent();
		$scope.feed.editQuestion.topics = $scope.topics.questionTopics;
		$scope.isLoading = true;
		$scope.feed.submitUpdate().then(function() {
			$scope.isLoading = false;
		});
	};



	$scope.listByUser = function(name) {
		$scope.currentUserList = name;
		$scope.feed.feedMode = "list-by-user";
		$scope.feed.loadUserQuestions(name);
	};


	$('.dropdown-menu li input, .dropdown-menu li label').click(function(e) {
	    e.stopPropagation();
	});

});


BrowseSearchModule.controller("NewChoiceController", function($scope, QuestionsFeed, $templateCache, dialog) {
	$scope.feed = QuestionsFeed;
	$scope.idSuffix = new Date().getTime();
	$scope.choice = {};
	$scope.choice.is_correct = '0';
	setTimeout(function() {
console.log('777');
		tinymce.init({
		    selector: "#choiceText",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function() {
					$scope.cText = ed;
					//$scope.cText.setContent($scope.feed.editChoice.choice);
		    	});
		    }
		});
	}, 100);

	setTimeout(function() {
console.log('101010');
		tinymce.init({
		    selector: "#explainText",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.eText = ed;
					//$scope.eText.setContent($scope.feed.editChoice.explanation);
		    	});
		    }
		});
	}, 100);
	$scope.closeOptionBox = function() {
		dialog.close();
		$templateCache.remove('views/form-edit-multiplechoice-option.php');
	}

	$scope.closeAndSaveOptionBox = function() {
	    $scope.choice.explanation = $scope.eText.getContent();
	    $scope.choice.choice = $scope.cText.getContent();
	    QuestionsFeed.addChoice($scope.choice);
		dialog.close();
		$templateCache.remove('views/form-edit-multiplechoice-option.php');
	};

	$scope.setOptionIsCorrect = function(v) {
		if(v == '1') {
			angular.forEach($scope.feed.editQuestion.choices, function(c) {
				if(c.is_correct == '1') {
					c.is_correct = '0';
				}
			});
		}
		$scope.choice.is_correct = v;
	};
});



BrowseSearchModule.controller("EditChoiceController", function($scope, QuestionsFeed, $templateCache, dialog) {
	$scope.feed = QuestionsFeed;
	$scope.idSuffix = new Date().getTime();
	$scope.choice = QuestionsFeed.editChoice;
	setTimeout(function() {
console.log('8888');
		tinymce.init({
		    selector: "#choiceText",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.cText = ed;
					$scope.cText.setContent($scope.choice.choice);
		    	});
		    }
		});
	}, 100);

	setTimeout(function() {
console.log('9999');
		tinymce.init({
		    selector: "#explainText",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.eText = ed;
					$scope.eText.setContent($scope.choice.explanation);
		    	});
		    }
		});
	}, 100);

	$scope.closeOptionBox = function() {
		dialog.close();
		$templateCache.remove('views/form-edit-multiplechoice-option.php');
	}

	$scope.closeAndSaveOptionBox = function() {
	    $scope.choice.explanation = $scope.eText.getContent();
	    $scope.choice.choice = $scope.cText.getContent();
	    QuestionsFeed.editChoice = $scope.choice;
		dialog.close();
		$templateCache.remove('views/form-edit-multiplechoice-option.php');
	};

	$scope.setOptionIsCorrect = function(v) {
		if(v == '1') {
			angular.forEach($scope.feed.editQuestion.choices, function(c) {
				if(c.is_correct == '1') {
					c.is_correct = '0';
				}
			});
		}
		$scope.choice.is_correct = v;
	};



});

Array.prototype.remove = function(item)
{
    for(var i = 0; i < this.length; i++)
        if(this[i] === item)
        {
            this.splice(i,1);
            return
        }
};
Array.prototype.contains = function(item)
{
    for(var i = 0; i < this.length; i++)
        if(this[i] === item)
            return true;
    return false
};
Array.prototype.indexOf = function(item, index)
{
    if(index === undefined || index === null || isNaN(index))
        index = 0;
    for(var i = index; i < this.length; i++)
        if(this[i] === item)
            return i;
    return-1
};

if(console == null) {
	console = {};
	console.log = function(){};
	console.dir = function(){};
}
