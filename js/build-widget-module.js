var ServiceURLS = {
	/*topics : "/restserver/index.php/api/testcenter/chapters/format/json",
	quizConfig: "/restserver/index.php/api/testcenter/quizlets/format/json"*/
	saveWidgetQuestionUrl:"/restserver/index.php/api/testcenter/save_tbs_question/format/json/",
	loadQuestionUrl:'/restserver/index.php/api/testcenter/tbs_questions/format/json/',
	updateQuestionUrl:'/restserver/index.php/api/testcenter/update_tbs_question/format/json/',
	listQuestionsByUserUrl:'/restserver/index.php/api/testcenter/tbs_questions/format/json/',
	getDocsUrl:'/restserver/index.php/api/manage_docs/getdocs/format/json?type=tbs-journal',
	getAssignedDocUrl:'/restserver/index.php/api/testcenter/content_document/format/json/',
	deleteQuestionUrl:'/restserver/index.php/api/testcenter/delete_tbs_question/format/json?id=',
	
};
var BuildWidgetQuestionModule = angular.module("BuildWidgetQuestionModule", ["ui.bootstrap", "wijmo", "TopicsModule", "NavModule"])
			.config(function($routeProvider) {
				$routeProvider.when(
						'/new', {
							//action:'new'
						}).when( '/edit/:qID', {
							//action:"load"
						}).otherwise({
							redirectTo:'/new'
						});
			});

BuildWidgetQuestionModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("answerkeyGridblock",
    "<h4>AnswerKey</h4><div class='answerKeyContainer'><answerkey-gridblock ng-model='key'></answerkey-gridblock></div>");
}]);
BuildWidgetQuestionModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("questionTextblock", "<div class='edit-toolbar'>"
										   		+"<div class='btn-group btn-group-vertical'>"
										   			+"<button class='btn btn-link' ng-click='deleteWidget(template)'><i class='fa fa-times'></i></button>"
										    		+"<button class='btn btn-link' ng-click='editTextBlock(template)'><i class='fa fa-edit'></i></button>"
										    	+"</div>"
										    +"</div>"
										    +"<question-textblock ng-model='template.model'></question-textblock>");
}]);
BuildWidgetQuestionModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("questionGridblock", "<div class='edit-toolbar'>"
										   		+"<div class='btn-group btn-group-vertical'>"
										   			+"<button class='btn btn-link' ng-click='deleteWidget(template)'><i class='fa fa-times'></i></button>"
										    		+"<button class='btn btn-link' ng-click='editGridBlock(template)'><i class='fa fa-edit'></i></button>"
										    	+"</div>"
										    +"</div>"
										    +"<div class='widgetQuestion-gridContainer'><question-gridblock ng-model='template'></question-gridblock></div>");
}]);
BuildWidgetQuestionModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("spreadjs",
    "<spreadjs ng-model='template.model'></spreadjs>");
}]);

BuildWidgetQuestionModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("researchInput", "<div class='edit-toolbar'>"
										   		+"<div class='btn-group btn-group-vertical'>"
										   			+"<button class='btn btn-link' ng-click='deleteWidget(template)'><i class='fa fa-times'></i></button>"
										    		+"<button class='btn btn-link' ng-click='editResearch(template)'><i class='fa fa-edit'></i></button>"
										    	+"</div>"
										    +"</div>"
										    +"<research-input ng-model='template'></research-input>");
}]);


BuildWidgetQuestionModule.directive('questionTextblock', ['$compile', function($compile) {
	return {
		restrict:"E", 
		template:'',
		transclude : true,
		scope:true,
		replace: false,
	    require: '?ngModel',
	    link: function($scope, elem, attr, ctrl) {
		   $scope.model = $scope.$parent.$eval(attr.ngModel);
		   elem.html($scope.model.content);

		   $scope.$parent.$watch(function() {
		   	return $scope.model.content;
		   }, function(n) {
		   	elem.html($scope.model.content);
		   });

		   $compile(elem.contents())($scope);
	    }
	};
}]);

BuildWidgetQuestionModule.directive('researchInput', [ '$compile', function() {
	return {
		restrict:"E",
		require:"ngModel",
		scope:false,
		template:'<span class="research-badge"><strong>{{model.model.content.type.type}}</strong></span><input type="text" ng-repeat="clause in maskParts" class="span1" value="{{clause}}" id="researchMask_{{$index}}" readonly/>',
		link:function($scope, $element, $attrs) {
			$scope.model = $scope.$parent.$eval($attrs.ngModel);
			$scope.content = $scope.model.model.content;
			$scope.maskParts = $scope.content.mask.split("-");

			$scope.$watch("model.model.content.mask", function(n) {
				if(n != null) {
					$scope.maskParts = $scope.content.mask.split("-");
				}
			});
		}

	}
}]);


BuildWidgetQuestionModule.directive('questionGridblock', ['$compile', '$templateCache', '$timeout', '$dialog', 'WidgetQuestion', function($compile, $templateCache, $timeout, $dialog, WidgetQuestion) {
	return {
		restrict:"E", 
		template:'<table></table>',

			
		transclude : true,
		scope:{ngModel:"="},
		replace: true,
	    require: 'ngModel',
	    link: function($scope, elem, attr, ctrl) {
		    $scope.model = $scope.$parent.$eval(attr.ngModel);
		    

		    attr.$set('id',$scope.model.model.id);

		    WidgetQuestion.errors.linkError = "";
		    $scope.getData = function() {
			    return $scope.model.model.content;
			};
			$scope.getColumns = function() {
			  	return $scope.model.model.columns;
			};
			$scope.pickCell = function(e, args) {

			};
			$scope.preprocessAnswerButtons = function() {
				for( var j = 0; j < $scope.model.model.content.length; j++) {
					var row = $scope.model.model.content[j];
					for (var k in row) {
						var col = row[k];
						if(String(col).indexOf("answerlink") > -1) {
							
							var dataList = col.split("data-list='")[1];
							dataList = dataList.split("'")[0];

							var dataAnswer = col.split("data-answer='")[1];
							dataAnswer = dataAnswer.split("'")[0];
							dataAnswer = parseInt(dataAnswer);
							col = col.split("just-updated").join("");
							col = col.split("ng-click='clickAnswerKeyLink'").join("data-grid='" + $scope.model.model.id + "' data-row='" + j + "' data-col='" + k + "'");
							col = col.split("<a ").join("<button ");
							col = col.split("</a>").join("</button>");

						}
						row[k] = col;
					}
				}
			};
			$scope.initAnswerButtons = function() {
				$timeout(function() {
					
					var links = $("#" + $scope.model.model.id + " button.button.answerlink").toArray();
					for(var i = 0; i < links.length; i++) {
						var inner = $(links[i]).html();
						$(links[i]).html(inner.split(" :")[0]);
						$(links[i]).attr("id", "link_" + i);
						$(links[i]).data("grid", $scope.model.modelw.id);
					}
					$("#" + $scope.model.model.id + " button.btn.answerlink").off("click");	
					$("#" + $scope.model.model.id + " button.btn.answerlink").on("click", function(e) {
						e.preventDefault();
						e.stopPropagation();
						var answerButton = $(this);
				        //var scope = angular.element("#question-region").scope();
						$scope.openAnswerKeyWindow( answerButton );
					});
				});
			};
			$scope.initGrid = function() {
				$scope.preprocessAnswerButtons();
				if($scope.grid != null) {
					$scope.grid.wijgrid("destroy");
				}
				$scope.grid =  $("#" + $scope.model.model.id).wijgrid({ data:$scope.model.model.content, 
																		columns:$scope.model.model.columns,
																		selectionMode:"singleCell",
																		highlightOnHover:false,
																		cellStyleFormatter:function(args) {
																			var style = $scope.model.model.styles["cell_" + args.column.dataIndex  + "_" + args.row.dataItemIndex];//getDictionaryEntryByFormatter(args);
																 			if(style != null) {
																 				angular.forEach(style.css, function(rule) {
																 					var cssKey;
																 					var val;
																 					for(key in rule) {
																 						cssKey = key;
																 						val = rule[key];
																 						args.$cell.css(cssKey, val);
																 					}
																 				});
																 			}
																		}
																	});
				var cols = $scope.grid.wijgrid("columns");
				for(var i = 0; i < cols.length; i++) {
					if($scope.model.model.columns[i].width != null) {
						cols[i].option("width", parseInt($scope.model.model.columns[i].width));
					}
				}
				$scope.initAnswerButtons();
					 
			};
			$scope.getAnswerkeyRow = function(key, row) {
				var answer = null;
				for(var i = 0; i < WidgetQuestion.model.question.answerkeys.length; i++) {
					var a = WidgetQuestion.model.question.answerkeys[i];
					if(a.model.id == key) {
						answer = angular.copy(a.model.content[row]);
						answer.keyID = a.model.id;
						answer.list = a.model.name;
					}
				}
				return answer;
			}
			$scope.openAnswerKeyWindow = function(answerButton) {
				var answerRow = $scope.getAnswerkeyRow(answerButton.data("list"), answerButton.data("answer"));
				if(answerRow != null) {
					answerRow.rowIndex = answerButton.data("answer");
				}
				$timeout(function() {
					var opts = {
					    backdrop: true,
					    keyboard: true,
					    backdropClick: false,
					    templateUrl: 'views/widgetquestion-answerkeys-window.php',
					    controller: 'AnswerKeysWindowController',
					    dialogClass: 'modal answerWindow',
					    resolve:{
					    	answer:function (){
					    		
								return answerRow;
					    	}
					    }
					    
					  };
				    var d = $dialog.dialog(opts);
				    d.open().then(function(answer) {
				    	if(answer == null) return;

		    			var row = answerButton.data("row");
		    			var col = answerButton.data("col");
			    		$scope.model.model.content[row][col] = "<button class='btn btn-info answerlink just-updated' data-list='" + answer.keyID + "' data-answer='" + answer.rowIndex + "' data-grid='" + $scope.model.model.id + "' data-row='" + row + "' data-col='" + col + "' ng-click='clickAnswerKeyLink'> " + answer.list + " : " + answer.letter + "</button>";
				   		
				 		$timeout(function() {
				 			$scope.initGrid();
				 			$scope.initAnswerButtons();
				 			$("button.just-updated").fadeTo('fast',0).fadeTo('fast',1).fadeTo('fast',0).fadeTo('fast',1);
				 		
				 		});
				    });
				});
			};
	      	$scope.initGrid();
	    }//end link
	};
}]);


window.letters = [
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z"
	];




