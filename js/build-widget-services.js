BuildWidgetQuestionModule.service("WidgetQuestion", function($http, $rootScope, $location, $routeParams, TopicService) {
	this.lastDefaultAnswerKeyIndexUsed = 0;
	this.model = {};
	this.model.question = {};
	this.model.question.widgets = [];
	this.model.question.answerkeys = [];
	this.model.answermap = [];

	this.errors = {};
	this.model.newCols = 4;
	this.model.newRows = 3;

	this.questionType = "tbs-journal";

	this.saveMode = "save";

	this.topics = TopicService;
	this.deprecated = false;

	var self = this;

	this.linkMode = this.LINK_MODE_NONE;

	this.saveCMD = "<i class='icon-ok'></i> Save TBS question";

	this.getAssignedDocument = function() {
		$http.get(ServiceURLS.getAssignedDocUrl).then(function(response) {
			if(response.data.length == null) {
				self.model.document = response.data;
			} else {
				self.model.document = response.data[0];
			}
		});
	};

	this.loadQuestion = function(qID) {
		
		return $http.get(ServiceURLS.loadQuestionUrl + "?id=" + qID).
			success(function(data, status, headers, config) {
				var parseQuestion = data[0];
				self.topics.setTopics( parseQuestion.topics );
				self.model.id = parseQuestion.id;
				self.deserialize(parseQuestion);
				self.saveCMD = "<i class='icon-ok'></i> Update TBS question";
				self.deprecated = parseQuestion.deprecated || false;
				self.saveMode = "update";
			});

	};

	this.initNewAnwerKey = function() {
		self.model.newAnswerKey = {};
		self.model.newAnswerKey.answers = [];
		self.model.newAnswerKey.columns = [{type:"String", dataKey:"answer", label:"Answers"}, {type:"String", dataKey:"id", label:"ID"}];
	};

	this.initNewGrid = function() {
		//this function is replicated in build-widget-newgrid-con
		self.newGrid = {};
		self.newGrid.cols = self.model.newCols;
		self.newGrid.rows = self.model.newRows;
		self.newGrid.rowFactory = {};
		self.newGrid.columns = [];
		self.newGrid.list = [];
		for(var i = 0; i< self.newGrid.cols; i++) {
			if(i == 0) {
				self.newGrid.rowFactory["col_" + i ] = "";
			} else {
				self.newGrid.rowFactory["col_" + i ] = "";
			}
			self.newGrid.columns.push({'dataKey': "col_" + i, "headerText":"resize column", "editable":true});
		}
		for(var i = 0; i< self.newGrid.rows; i++) {	
			self.newGrid.list.push(angular.copy(self.newGrid.rowFactory ));
		}
	};
	this.isSaved = false;
	this.isDirty = false;
	var researchType = "";

	this.deleteQuetsion = function() {
		$http.get(ServiceURLS.deleteQuestionUrl + question.model.model.id).then(function(result) {
			console.dir(result);
		});
	}

	this.saveQuestion = function() {
		self.questionType = "tbs-journal";
		angular.forEach(self.model.question.widgets, function(widget) {
			if (widget.type === 'researchInput') {
				self.questionType = "tbs-research";
			}
		});
/*
		for(var i = 0; i < self.model.question.widgets; i++) {
			w = self.model.question.widgets[i];
			if(w.type == "researchInput") {
				WidgetQuestion.questionType = "tbs-research";
				break;
			} 
		}
*/
		$http.post(ServiceURLS.saveWidgetQuestionUrl, 
			{"question":encodeURIComponent(JSON.stringify({"question":self.model.question})), 
			"topics":self.topics.questionTopics, 
			"deprecated": self.deprecated,
			"type":self.questionType, 
			"researchType":researchType
			})
			.success(function (data, status, headers, config) {
				if (data.dupes) {
					self.dupes = true;
					self.isSaved = false;
				}
				else {
					self.isSaved = true;
					self.dupes = false;
					var q = JSON.parse(data.question_json);
					q.question = JSON.parse(decodeURIComponent(q.question));
					self.model.question = q.question.question;
					self.model.question.id = data.id;
					if(self.isSaved == true) {
						self.isDirty = false;
					}

					self.deprecated = q.deprecated || false;
				}
			});

	};

	this.updateQuestion = function() {
	console.log('updating question');
		angular.forEach(self.model.question.widgets, function(widget) {
			if (widget.type === 'researchInput') {
				self.questionType = "tbs-research";
			}
			else {
				self.questionType = "tbs-journal";
			}
		});
		for(var i = 0; i < self.model.question.widgets; i++) {
			w = self.model.question.widgets[i];
			if(w.type == "researchInput") {
				WidgetQuestion.questionType = "tbs-research";
				break;
			} 
		}
		var docID = "0";
		if(self.model.document.id != null) {
			docID = self.model.document.id;
		} 
		$http.post(ServiceURLS.updateQuestionUrl, {
				"question":encodeURIComponent(JSON.stringify({"question":self.model.question})), 
				"topics":self.topics.questionTopics, 
				"type":self.questionType, 
				"docID": docID,
				"deprecated": self.deprecated,
				id:self.model.id }).success(function (data, status, headers, config) {
				self.isSaved = true;
			});

	};

	this.feedMode = "default";

	this.renderModel = {};
	this.renderModel.count = 0;

	this.deserialize = function(question) {

		if(question == null) {
			return;
		}
		self.model.document = question.document;
		var dparts = self.model.document.url.split("/");
		self.model.document.title = dparts[dparts.length - 1];
		//remove all wijgrids:
		angular.forEach(window.widjgridInstances, function(grid) {
			grid.option("remove");
		});
		
		window.gridInstances = {};
		try {
			var newModel = JSON.parse(question.question_json);
		} catch(e) {
			self.model.parseError = true;
			return; 
		}
		
		
		self.topics.setTopics(newModel.topics);
		


		try {
			var question = decodeURIComponent(newModel.question);
			question = JSON.parse(question);
		} catch(e) {
			self.model.parseError = true;
			return; 
		}
			

		
		self.model.question.widgets = [];
		angular.forEach(question.question.widgets, function(widget) {
			if(widget.type == "questionGridblock") {
				widget.isDirty = true;
			}
			self.model.question.widgets.push(widget);
		});

		self.model.question.solution = question.question.solution;
		self.model.question.answerkeys = [];

		angular.forEach(question.question.answerkeys, function(key) {
			var answerkey = {};
			answerkey.model = key.model;
			if(answerkey.model.content.length > 0 && answerkey.model.content[0].letter == null) {
				for(var i = 0; i < answerkey.model.content.length; i++) {
					answerkey.model.content[i].letter = window.letters[i];
				}
			}
			answerkey.order = key.order;
			answerkey.type = key.type;
			self.model.question.answerkeys.push(answerkey);
		});
		
		self.renderModel.count++;
	};

});
