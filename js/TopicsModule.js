var TopicsModule = angular.module("TopicsModule", ["ui.bootstrap"]);
TopicsModule.service("TopicService", function($http, $q, $dialog) {
	var self = this;
	self.isReady = false;
	self.topicPromises = { AUD:$q.defer(), BEC:$q.defer(), FAR:$q.defer(), REG:$q.defer()};
	self.topics = { AUD:self.topicPromises.AUD.promise.then(function(data) {
						self.topics.AUD = data;
					}), 
					BEC:self.topicPromises.BEC.promise.then(function(data) {
						self.topics.BEC = data;
					}),  
					FAR:self.topicPromises.FAR.promise.then(function(data) {
						self.topics.FAR = data;
					}), 
					REG:self.topicPromises.REG.promise.then(function(data) {
						self.topics.REG = data;
					})};
	self.questionTopics = [];
	self.course = "";

	this.loadTopics = function(section) {
	$http({method:'GET', url: "/restserver/index.php/api/testcenter/chapters/format/json?section=" + section  }).then(function(response) {
				var chapters = [];
				var j = 0;
				angular.forEach(response.data.chapters, function(chapter) {
					chapter.include = false;
					var i = 0;
					j++;
					angular.forEach(chapter.topics, function(topic) {
						topic.chapter = chapter.chapter;
						topic.include = false;
						topic.chapterID = chapter.id;
						topic.course = section;
						i++;
						if(i == chapter.topics.length && j == response.data.chapters.length) {
							self.topicPromises[section].resolve(response.data.chapters);
						}
					});
				});
		});
	};

	this.loadTopics("AUD");
	this.loadTopics("BEC");
	this.loadTopics("FAR");
	this.loadTopics("REG");

	this.findTopic = function(chapterID, topicID, section) {
		var promise = $q.defer();
		if(section == null) {
			section = "AUD";
		}
		if(self.topics[section].length != null) {
				angular.forEach(self.topics[section], function(chapter) {
						if(chapter.id == chapterID) {
							angular.forEach(chapter.topics, function(topic) {
								if(topic.id == topicID && topic.chapterID == chapterID) {
									topic.include = true;
									promise.resolve(topic);
									self.course = section;
								}
							});
						}
					
				});
		} else {
			self.topicPromises[section].promise.then(function(data){
				angular.forEach(self.topics[section], function(chapter) {
						if(chapter.id == chapterID) {
							angular.forEach(chapter.topics, function(topic) {
								if(topic.id == topicID && topic.chapterID == chapterID) {
									topic.include = true;
									promise.resolve(topic);
									self.course = section;
								}
							});
						}
					
				});
			});
		}
		return promise.promise;
	};

	this.addTopic = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/topic-window.php',
		    controller: 'AddTopicController',
		    dialogClass: 'modal topicWindow',
		    resolve:{
		    	topics:function() {
		    		angular.forEach(self.questionTopics, function(pickedTopic) {
							angular.forEach(self.topics[self.course], function(chapter) {
								if(chapter.id == pickedTopic.chapterID) {
									angular.forEach(chapter.topics, function(chTopic) {
										if(chTopic.id == pickedTopic.topicID) {
											chTopic.include = true;
										}
									});
								}
							});
						});
		    		return self.topics;
		    	},
		    	course:function() {
		    		return self.course;
		    	},
		    	questionTopics:function() {
		    		return self.questionTopics;
		    	}
		    }
		  };
	    var d = $dialog.dialog(opts);
	    d.open().then(function(newTopics) {
	    	if(newTopics != null) {
		    	self.questionTopics = newTopics;
		    	self.course = self.questionTopics[0].course;
		    	self.questionTopics.sort(function(a, b) {
				if(Number(a.prefix) > Number(b.prefix)) {
					return 1;
				} else if(Number(a.prefix) < Number(b.prefix)) {
					return -1;
				} else {
					return 0;
				}
			});
	    	}
	    });
	};

	this.removeTopic = function(topic) {
		for(var i = 0; i < self.questionTopics.length; i++) {
			qTopic = self.questionTopics[i];
			if(qTopic.chapterID == topic.chapterID && qTopic.id == topic.id) {
				qTopic.include = false;
				self.questionTopics.splice(i, 1);
				break;
			}
		}
	}


	this.setTopics = function(topics) {

		if(topics == null) return;
		for(var i = 0; i < topics.length; i++) {
			var topic = topics[i];
			if(topic.section != null) {
				self.findTopic(topic.chapter_id, topic.topic_id, topic.section).then(function(d) {
								self.questionTopics.push( d );
							});
			}
		}
		$q.all(self.questionTopics).then(function() {
			self.questionTopics.sort(function(a, b) {
				if(Number(a.prefix) > Number(b.prefix)) {
					return 1;
				} else if(Number(a.prefix) < Number(b.prefix)) {
					return -1;
				} else {
					return 0;
				}
			});
		});
	};


});

TopicsModule.controller('AddTopicController', function ($scope, $timeout, topics, course, questionTopics, dialog) {


	$scope.tabs = {"course":course};
	$scope.questionCourse = course;
	$scope.sections = topics;
	$scope.questionTopics = questionTopics;

	$scope.pickSection = function(section) {
		$scope.topics = $scope.sections[section].topics;
		$scope.tabs.course = section;
		$scope.initLayout();
		
	};

	$scope.initLayout = function() {
		$timeout(function() {
			 var container = $('.pin-container');
		      if(container != null && container.length > 0) {
		        var msnry = new Masonry( container[0], {itemSelector: '.pin'});
		      }
		});
	};

	$scope.toggleTopic = function(topic, override) {

		var hasTopic = override || false;

		if(topic.include == true) {
			topic.include = false;
			var len = $scope.questionTopics.length;
			for(var i = 0; i < len; i++) {
				var obj = $scope.questionTopics[i];
				if(obj.topicID == topic.topicID && obj.chapterID == topic.chapterID) {
					$scope.questionTopics.splice(i, 1);
					break;
				}
			}
		} else {
			topic.include = true;
			$scope.questionTopics.push(topic);
		}
	};

	$scope.close = function() {
		dialog.close();
	}

	$scope.saveAndClose = function() {
		dialog.close($scope.questionTopics);
	}

	$scope.initLayout();
});

//TopicsModule.directive("Topics")

TopicsModule.controller("ConfirmWindowController", function($scope, dialog, confirmMsg, confirmHeading) {
	$scope.confirmMsg = confirmMsg;
	$scope.confirmHeading = confirmHeading;

	$scope.cancel = function() {
		dialog.close(false);
	}
	$scope.confirm = function() {
		dialog.close(true);
	}
});


TopicsModule.service("DocSearchService", function($http, $q, $dialog) {
	var self = this;
	self.searchType = "and";

	this.getDocs = function(type) {
		self.type = type;
		self.docs = $http({method:'GET', url: '/restserver/index.php/api/manage_docs/getdocs/format/json?type=' + self.type}).success(function (data, status, headers, config) {
				for(var i = 0; i < data.documents.length; i++) {
					var doc = data.documents[i];
					var dparts = doc.title.split("/");
					doc.title = dparts[dparts.lenth-1];
				}

				self.docs = data.documents;
			});
		return self.docs.promise;
	};


	this.search = function(query, type, searchType) {
		if(type != self.type || self.docs == null) {
			self.getDocs(type);
		}
		self.searchType = searchType;
		query = S(this.stripTags(query)).collapseWhitespace().s;

		query = query.split(" ").splice(3, 5).join(" ");
		var template = "views/window-docsearch-tbsjournal.php";
		if(this.type == "tbs-journal") {
			self.matches = self.searchTBSJournalDocs(query);
			template = "views/window-docsearch-tbsjournal.php";
		}

		var docPromise = $q.defer();
		var opts = {
			templateUrl:template,
			backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    controller: 'DocMatchesWindowController',
		    dialogClass: 'modal docsearchWindow',
		    resolve: {
		    	matches:function(){
		    		return self.matches;
		    	}
		    }
		}

		var d = $dialog.dialog(opts);
		d.open().then(function(docMatch) {

			dparts = docMatch.split("/");
			docMatch = dparts[dparts.length-1];
			var doc = {};
			for(var i = 0; i < self.docs.length; i++) {
				if(self.docs[i].url.indexOf(docMatch) != -1) {
					doc = self.docs[i];
					doc.title = docMatch;
					break;
				}
			}
			if(doc.id != null) {
				docPromise.resolve(doc);
			} else{
				docPromise.resolve(null);
			}
		});

		return docPromise.promise;

	};

	this.searchTBSJournalDocs = function(query) {
		var promise = $q.defer();
		query = query.replace(/\s+/g, ' ');
		$http({method:'GET', url: "/restserver/sphider/search.php?search=1&type=" + self.searchType + "&query=" + query}).
			success(function (data, status, headers, config) {
				for(var i = 0; i < data.qry_results.length; i++) {
					data.qry_results[i].url = data.qry_results[i].url.split(".pdf").join(".docx");
				}

				promise.resolve(data);//.response);

			});
		return promise.promise;	
	}

	this.stripTags = function(input, allowed) {

		var common = "a,about,after,ago,all,am,an,and,any,are,aren't,as,at,be,but,by,for,from,get,go,how,if,in,into,is,isn't,it,its,me,more,most,must,my,new,news,no,none,not,nothing,of,off,often,old,on,once,only,or,other,our,ours,out,over,page,she,should,small,so,some,than,true,thank,that,the,their,theirs,them,then,through,there,these,they,this,those,thus,time,times,to,too,under,until,up,upon,use,users,version,very,via,want,was,way,web,were,what,when,where,which,who,whom,whose,why,wide,will,with,within,without,would,yes,yet,you,your,yours";
		common = common.split(",");
		input = input.toLowerCase();
		for(var i = 0; i < common.length; i++) {
			input.split
		}
		allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
		var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
		var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
		var answer = input.replace(commentsAndPhpTags, '').replace(tags, function($0, $1) {
		      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
		    }).replace(/[\-=_!"#%&'*{},.\/:;?\(\)\[\]@\\$\^*+<>~`\u00a1\u00a7\u00b6\u00b7\u00bf\u037e\u0387\u055a-\u055f\u0589\u05c0\u05c3\u05c6\u05f3\u05f4\u0609\u060a\u060c\u060d\u061b\u061e\u061f\u066a-\u066d\u06d4\u0700-\u070d\u07f7-\u07f9\u0830-\u083e\u085e\u0964\u0965\u0970\u0af0\u0df4\u0e4f\u0e5a\u0e5b\u0f04-\u0f12\u0f14\u0f85\u0fd0-\u0fd4\u0fd9\u0fda\u104a-\u104f\u10fb\u1360-\u1368\u166d\u166e\u16eb-\u16ed\u1735\u1736\u17d4-\u17d6\u17d8-\u17da\u1800-\u1805\u1807-\u180a\u1944\u1945\u1a1e\u1a1f\u1aa0-\u1aa6\u1aa8-\u1aad\u1b5a-\u1b60\u1bfc-\u1bff\u1c3b-\u1c3f\u1c7e\u1c7f\u1cc0-\u1cc7\u1cd3\u2016\u2017\u2020-\u2027\u2030-\u2038\u203b-\u203e\u2041-\u2043\u2047-\u2051\u2053\u2055-\u205e\u2cf9-\u2cfc\u2cfe\u2cff\u2d70\u2e00\u2e01\u2e06-\u2e08\u2e0b\u2e0e-\u2e16\u2e18\u2e19\u2e1b\u2e1e\u2e1f\u2e2a-\u2e2e\u2e30-\u2e39\u3001-\u3003\u303d\u30fb\ua4fe\ua4ff\ua60d-\ua60f\ua673\ua67e\ua6f2-\ua6f7\ua874-\ua877\ua8ce\ua8cf\ua8f8-\ua8fa\ua92e\ua92f\ua95f\ua9c1-\ua9cd\ua9de\ua9df\uaa5c-\uaa5f\uaade\uaadf\uaaf0\uaaf1\uabeb\ufe10-\ufe16\ufe19\ufe30\ufe45\ufe46\ufe49-\ufe4c\ufe50-\ufe52\ufe54-\ufe57\ufe5f-\ufe61\ufe68\ufe6a\ufe6b\uff01-\uff03\uff05-\uff07\uff0a\uff0c\uff0e\uff0f\uff1a\uff1b\uff1f\uff20\uff3c\uff61\uff64\uff65]+/g,"");
		
		answer = answer.toLowerCase();
		answer = answer.replace(/[0-9]/g, "");
		answer = answer.replace(/\r/, "");
		answer = answer.replace(/[\u2019]s/, "");
		for(var i = 0; i < common.length; i++) {
			answer = answer.replace(new RegExp("\\b" + common[i] +"\\b", "g"), "");
			//answer = answer.split(" " + common[i] + " ").join(" ");
		}
		return answer;
	};

});

TopicsModule.controller("DocMatchesWindowController", function($scope, dialog, matches, DocSearchService) {
	
	$scope.result = {};
	$scope.result.matches = matches;

	$scope.assign = {};


	$scope.assignDocument = function(doc) {
		$scope.assign.pickedDocument = doc;
		var dparts = doc.split("/");
		$scope.assign.title = dparts[dparts.length -1];
	};

	$scope.close = function() {
		dialog.close();
	};

	$scope.save =function() {
		dialog.close($scope.assign.pickedDocument);
	};


});




