
BuildWidgetQuestionModule.controller("SaveController", function($scope, $dialog, WidgetQuestion, TopicService) {
	$scope.question = WidgetQuestion;
	
	$scope.topics = TopicService;
	$scope.msg = {};
	$scope.msg.cmdMessage = WidgetQuestion.saveCMD;

	$scope.$watch(function() {
		return WidgetQuestion.saveCMD;
	}, function(n) {
		if(n == null) return;
		$scope.msg.cmdMessage = WidgetQuestion.saveCMD;
	})

	$scope.$watch(function() {
		return WidgetQuestion.isSaved;
	}, function(n) {
		if(n == null) return;
		if(n == true) {
			$scope.msg.cmdMessage = "<i class='icon-ok'></i> Update TBS question";
		}
	});

	$scope.deprecateQuestion = function() {
		$scope.question.deprecated = !$scope.question.deprecated;
	};

	$scope.saveQuestion = function() {
		if($scope.question.saveMode == "update") {
			$scope.question.updateQuestion();
			$scope.cmdMessage = "Updating...";
		} else {
			$scope.question.saveQuestion();
			$scope.cmdMessage = "Saving...";
		}
		
	};
	$('.dropdown-menu li input, .dropdown-menu li label').click(function(e) {
	    e.stopPropagation();
	});
});

BuildWidgetQuestionModule.controller("ToolBarController", function($scope, $dialog, WidgetQuestion, TopicService, DocSearchService, $route, $routeParams ) {
	$scope.question = WidgetQuestion; 
	$scope.isEditingQuestion = function() {
		return (window.location.href.indexOf("edit") >= 0);
	};
	$scope.$watch(function() {
		return WidgetQuestion.model.id;
	}, function(n) {
		//console.log(n);
		//$scope.$digest();
	});

	$scope.searchForDocMatches = function() {
		var query = "";
		for(var i = 0; i < $scope.question.model.question.widgets.length; i++) {
			var w = $scope.question.model.question.widgets[i];
			if(w.type == "questionTextblock") {
				query =   w.model.content;
			}
		}
		DocSearchService.search(query, "tbs-journal", "and").then(function(doc){
			$scope.question.model.document = doc;
		});
	};

	$scope.newRichTextWidget = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-richtext.php',
		    controller: 'NewRichtextController',
		    dialogClass: 'modal questionWindow'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};
	
	$scope.newResearchLink = function (type) {
		var opts = {
			
			backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-researchkey.php',
		    controller: 'ResearchInputController',
		    dialogClass: 'modal research-key-input'
		};

		var d = $dialog.dialog(opts);
	    d.open();
	};


	$scope.newGridWidget = function() {
		WidgetQuestion.model.newCols = 4;
		WidgetQuestion.model.newRows = 3;
		WidgetQuestion.initNewGrid();
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-grid.php',
		    controller: 'NewGridController',
		    dialogClass: 'modal questionWindow',
		    resolve:{
		    	gridWidget:function(){
		    		return {
		    			model:{
		    				content:angular.copy(WidgetQuestion.newGrid.list),
			    			columns:angular.copy(WidgetQuestion.newGrid.columns),
			    			styles:{},
			    			id:"newGrid"
		    			},
		    			type:"questionGridblock"
		    		};
		    	}
		    }
		  };
		
	    var d = $dialog.dialog(opts);
	    d.open().then(function(gridWidget){

	    });
	}

	$scope.newSpreadsheetWidget = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-spreadsheet.php',
		    controller: 'NewSpreadJSController',
		    dialogClass: 'modal questionWindow'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};	

	$scope.newAnswerKey = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-answerkey.php',
		    controller: 'NewAnswerkeyController'
		  };
		  WidgetQuestion.initNewAnwerKey();
	    var d = $dialog.dialog(opts);
	    d.open();
	};
	$scope.showSolutionScreen = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/form-tbs-widget-solution.php',
		    controller: 'NewSolutionController',
		    dialogClass: 'modal questionWindow'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};

	$scope.deleteQuestion = function() {
		//TODO: confirm dialog
		question.deleteQuestion();
	}

	$scope.resetAndRebuild = function() {
		window.open($scope.question.model.document.url, "_blank");
		$scope.question.model.question.widgets = [];
		$scope.question.model.question.solution = null;
		$scope.question.model.parseError = false;
	}

	$scope.launchInQuiz = function() {
		var data = {"questions":{"multipleChoice":[],"tbs":[WidgetQuestion.model.id]},
				"topics":TopicService.questionTopics
				};
		data = JSON.stringify(data);
		if(WidgetQuestion.model.id)
		window.open("/testmodule/setup.php?data=" + encodeURIComponent(data) + "#/preview/" + WidgetQuestion.questionType + "/" + WidgetQuestion.model.id, "_blank");

	};
});


BuildWidgetQuestionModule.controller("ResearchInputController", function($scope, dialog, WidgetQuestion) {

	$scope.researchTypes = [
		{id:"1", type:"GAAS", url:"http://pcaobus.org/Standards/Auditing/Pages/AU150.aspx"},
		{id:"2", type:"FASB ASC", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"3", type:"IRC", url:"http://www.law.cornell.edu/uscode/text/26"}, 
		{id:"4", type:"PCAOB", url:"http://pcaobus.org/Standards/Auditing/Pages/default.aspx"},
		{id:"5", type:"NASBA", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"6", type:"AU-C", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"7", type:"AT", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"8", type:"AR-C", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"9", type:"ET", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"10", type:"BL", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"11", type:"VS", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"12", type:"CS", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"13", type:"PFP", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"14", type:"CPE", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"15", type:"QC", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"16", type:"TC 230", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"}
	];

	$scope.pickedType = $scope.researchTypes[0];
	$scope.close = function() {
		dialog.close();
	};

	$scope.saveAndClose = function() {
		
		WidgetQuestion.model.question.widgets.push(
			{ type: "researchInput", 
				"model":{"content":{
				type:$scope.pickedType, 
				mask: $scope.mask}, 
				id:"researchAnswer"
			}, 
				order:WidgetQuestion.model.question.answerkeys.length 
		});
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};

	$scope.setType = function(type) {
		$scope.pickedType = type;
	}
});


BuildWidgetQuestionModule.controller("EditResearchInputController", function($scope, dialog, research, WidgetQuestion) {

	$scope.researchTypes = [
		{id:"1", type:"GAAS", url:"http://pcaobus.org/Standards/Auditing/Pages/AU150.aspx"},
		{id:"2", type:"FASB ASC", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"3", type:"IRC", url:"http://www.law.cornell.edu/uscode/text/26"}, 
		{id:"4", type:"PCAOB", url:"http://pcaobus.org/Standards/Auditing/Pages/default.aspx"},
		{id:"5", type:"NASBA", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"6", type:"AU-C", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"7", type:"AT", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"8", type:"AR", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"9", type:"ET", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"10", type:"BL", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"11", type:"VS", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"12", type:"CS", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"13", type:"PFP", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"14", type:"CPE", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"},
		{id:"15", type:"QC", url:"https://www2.nasba.org/NASBAWeb.nsf/ENCD"}
	];

	$scope.research = research;

	$scope.pickedType = research.model.content.type;//$scope.researchTypes[0];
	$scope.mask = research.model.content.mask;

	$scope.close = function() {
		dialog.close();
	};

	$scope.saveAndClose = function() {
		angular.forEach(WidgetQuestion.model.question.widgets, function(w) {
			if(w.model.id == $scope.research.model.id) {
				w.model.content.type = $scope.pickedType;
				w.model.content.mask = $scope.mask;
			}
		});
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};

	$scope.setType = function(type) {
		$scope.pickedType = type;
	}
});


BuildWidgetQuestionModule.controller("QuestionRenderer", function($scope, $routeParams, $dialog, $timeout, WidgetQuestion, TopicService) {
	$scope.model = {};
	$scope.model
	$scope.model.question = WidgetQuestion.model.question;
	$scope.model.answermap = WidgetQuestion.model.answermap;
	$scope.question = WidgetQuestion;
	$scope.topics = TopicService;
	if($scope.model.question.widgets == null || $scope.model.question.widgets.length == 0) {
		var hash = document.location.hash;
		if(hash.indexOf("edit") != -1) {
			var qid = hash.split("/edit/")[1];
			$scope.question.loadQuestion(qid).then(function() {
				$scope.model.question = WidgetQuestion.model.question;
			
			});
		} else {
			var tbsTopics = JSON.parse(decodeURIComponent($.cookie('tbs_topics')));
			$scope.topics.setTopics( tbsTopics.topics );
			WidgetQuestion.getAssignedDocument();
		}
		
	}
	window.questionRenderer = $scope;
	$scope.rendered = function(args) {
	};

	window.styleDictionary = {};

	$scope.deleteWidget = function(widget) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/window-confirm.php',
		    controller: 'ConfirmWindowController', //TopicsModule
		    dialogClass: 'modal',
		    resolve:{
		    	confirmHeading:function (){
					return "Are you sure?";
		    	},
		    	confirmMsg:function (){
					return "<p>You are about to delete this widget.</p><p>This change will not be permanent until you save this question.</p>";
		    	}
		    }
		  };
		var d = $dialog.dialog(opts);
	   	d.open().then(function(confirm) {
	    	if(confirm == true) {
				for(var i = 0; i < WidgetQuestion.model.question.widgets.length; i++) {
					var aKey = WidgetQuestion.model.question.widgets[i];
					if(aKey.blank == null && aKey.model.id == widget.model.id) {
						WidgetQuestion.model.question.widgets.splice(i, 1);
					}
				}
			}
		});
			
	};

	$scope.deleteSolution = function( ) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/window-confirm.php',
		    controller: 'ConfirmWindowController', //TopicsModule
		    dialogClass: 'modal',
		    resolve:{
		    	confirmHeading:function (){
					return "Are you sure?";
		    	},
		    	confirmMsg:function (){
					return "<p>You are about to delete the solution.</p><p>This change will not be permanent until you save this question.</p>";
		    	}
		    }
		  };
		var d = $dialog.dialog(opts);
	    d.open().then(function(confirm) {
	    	if(confirm == true) {
				WidgetQuestion.model.question.solution = null;
			}
		});
			
	};

	$scope.deleteAnswerkey = function(key) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/window-confirm.php',
		    controller: 'ConfirmWindowController', //TopicsModule
		    dialogClass: 'modal',
		    resolve:{
		    	confirmHeading:function (){
					return "Are you sure?";
		    	},
		    	confirmMsg:function (){
					return "<p>You are about to delete this answer key. You will have to reset all the grid answers that have been assigned to answers from this answer key.</p><p>This change will not be permanent until you save this question.</p>";
		    	}
		    }
		  };
		var d = $dialog.dialog(opts);
	    d.open().then(function(confirm) {
	    	if(confirm == true) {
				for(var i = 0; i < WidgetQuestion.model.question.answerkeys.length; i++) {
					var aKey = WidgetQuestion.model.question.answerkeys[i];
					if(aKey.model.id == key.model.id) {
						WidgetQuestion.model.question.answerkeys.splice(i, 1);
					}
				}
			}
		});
	};

	$scope.addStyleDictionary = function(id, style) {
		window.styleDictionary[id] = style;
	}



	$scope.editTextBlock = function(textblock) {
		//$scope.removeGridInstances();
		$scope.question.editTextBlock = textblock;
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-richtext.php',
		    controller: 'NewRichtextController',
		    dialogClass: 'modal questionWindow'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};

	$scope.editResearch = function(research) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-researchkey.php',
		    controller: 'EditResearchInputController',
		    dialogClass: 'modal questionWindow',
		    resolve:{
		    	research:function() {
		    		return research;
		    	}
		    }
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	}

	$scope.editGridBlock = function(gridWidget) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-grid.php',
		    controller: 'NewGridController',
		    dialogClass: 'modal questionWindow',
		    resolve: {
		    	gridWidget:function() {
		    		return gridWidget;
		    	}
		    }
		  };
	    var d = $dialog.dialog(opts);
	    d.open().then(function(widget) {
	    	//do nothing, model is already updated in the directive
	    });
	};

	$scope.editSolution = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/form-tbs-widget-solution.php',
		    controller: 'NewSolutionController',
		    dialogClass: 'modal questionWindow'
		}
		  WidgetQuestion.editSolution = true;
	    var d = $dialog.dialog(opts);
	    d.open();
	};

	$scope.removeGridInstances = function() {
		for(var key in window.gridInstances) {
			$("#" + key).wijgrid("destroy");
			delete window.gridInstances[key];
			for(var i = 0; i < $scope.model.question.widgets.length; i++) {
				if($scope.model.question.widgets[i].model.id == key) {
					$scope.model.question.widgets.splice(i, 1);
				}
			}
		}
	};


});

BuildWidgetQuestionModule.controller("AnswerKeyListController", function($scope, WidgetQuestion) {
	//$scope
});

BuildWidgetQuestionModule.controller("NewRichtextController", function($scope, $timeout, dialog, WidgetQuestion) {
/* "Text block" text editor (TBS-Research) */
	$timeout(function() {
		tinymce.init({
		    selector: "#newTextBlockEditor",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor jbimages"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.editor = ed;
					if(WidgetQuestion.editTextBlock != null) {
						$scope.editor.setContent(WidgetQuestion.editTextBlock.model.content);
					}
		    	});
		    }
		});
	});

	$scope.close = function() {
		dialog.close();
	};

	$scope.saveAndClose = function() {
		if(WidgetQuestion.editTextBlock != null) {
			angular.forEach(WidgetQuestion.model.question.widgets, function(w) {
				if(WidgetQuestion.editTextBlock != null && w.model.id == WidgetQuestion.editTextBlock.model.id) {
					w.model.content = $scope.editor.getContent();

					WidgetQuestion.editTextBlock = null;
				
				}
			});
		} else {
			WidgetQuestion.model.question.widgets.push({ type: "questionTextblock", "model":{"content":$scope.editor.getContent(), "id":"qText_" + WidgetQuestion.model.question.widgets.length}, order:WidgetQuestion.model.question.widgets.length })
		}
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};
});

BuildWidgetQuestionModule.controller("NewSolutionController", function($scope, $timeout, dialog, WidgetQuestion) {
/* Solution text editor */
	$timeout(function() {
		tinymce.init({
		    selector: "#newSolutionEditor",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor jbimages"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.editor = ed;
					if(WidgetQuestion.editSolution == true || WidgetQuestion.model.question.solution != null) {
						$scope.editor.setContent(WidgetQuestion.model.question.solution);
					}
					
		    	});
		    }
		});
	});
	$scope.close = function() {
		WidgetQuestion.editSolution = false;
		dialog.close();
	}
	$scope.saveAndClose = function() {
		WidgetQuestion.model.question.solution = $scope.editor.getContent();
		WidgetQuestion.editSolution = false;
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};
});


BuildWidgetQuestionModule.controller("NewAnswerkeyController", function($scope, dialog, WidgetQuestion) {
	
	
	$scope.model = WidgetQuestion;
	$scope.newAnswer = {};
	$scope.newAnswer.text  = "";
	$scope.defaultAnswerkeyNames = [
		"List A",
		"List B",
		"List C",
		"List D",
		"List E",
		"List F",
		"List G",
		"List H",
		"List I",
		"List J",
		"List K",
		"List L",
		"List M",
		"List N",
		"List O",
		"List P",
		"List Q",
		"List R",
		"List S",
		"List T",
		"List U",
		"List V",
		"List W",
		"List X",
		"List Y",
		"List Z"
	];
	$scope.answerkeyName = $scope.defaultAnswerkeyNames[WidgetQuestion.lastDefaultAnswerKeyIndexUsed];



	$scope.addAnswer = function() {
		if($scope.newAnswer.text == "") return;
		var temp = angular.copy($scope.model.model.newAnswerKey.answers);
		temp.push({"id":$scope.model.model.newAnswerKey.answers.length, "answer":$scope.newAnswer.text});
		$scope.model.model.newAnswerKey.answers = angular.copy(temp);//push({"id":$scope.model.model.newAnswerKey.answers.length, "answer":$scope.newAnswer.text});
		$scope.newAnswer.text = "";
	};


	$scope.close = function() {
		dialog.close();
	};
	$scope.saveAndClose = function() {
		if($scope.answerkeyName == "") {
			$scope.answerkeyName = $scope.defaultAnswerkeyNames[WidgetQuestion.lastDefaultAnswerKeyIndexUsed];
		}
		if($scope.answerkeyName == $scope.defaultAnswerkeyNames[WidgetQuestion.lastDefaultAnswerKeyIndexUsed]) {
			WidgetQuestion.lastDefaultAnswerKeyIndexUsed++;
		}
		var gridData = $("#answerKeyGrid").wijgrid("data");

		var letter = 0;
		angular.forEach(gridData, function(g) {
			g.letter = window.letters[letter++];

		});

		WidgetQuestion.model.question.answerkeys.push({ type: "answerKey", "model":{"content":gridData, id:"aKey_" + WidgetQuestion.model.question.answerkeys.length, name:$scope.answerkeyName}, order:WidgetQuestion.model.question.answerkeys.length });
		
		if(WidgetQuestion.isSaved == true) {
			WidgetQuestion.isDirty = true;
		}
		WidgetQuestion.renderModel.count++;
		dialog.close();
	};


 	$scope.currentCellChanging = function(e, args) {
 		var col = args.cellIndex;
        var row = args.rowIndex;
 	};



});


BuildWidgetQuestionModule.directive('spreadjs', ['$compile', '$templateCache', 'WidgetQuestion', function($compile, $templateCache, WidgetQuestion) {
	return {
		restrict:"E", 
		template:'<div class="spreadjs-container"></div>',
		transclude : true,
		scope:{ngModel:"="},
		replace: true,
	    require: 'ngModel',
	    link: function($scope, elem, attr, ctrl) {
		    $scope.model = $scope.$parent.$eval(attr.ngModel);
		    attr.$set('id',$scope.model.model.id);
		    WidgetQuestion.errors.linkError = "";
		      
			$scope.pickCell = function(e, args) {
				WidgetQuestion.errors.linkError = "";
				if(WidgetQuestion.linkMode == WidgetQuestion.LINK_MODE_PICK_GRID) {
					var targetID = e.target.parentNode.parentNode.id;
					WidgetQuestion.setAnswerSource(targetID, args.cellIndex, args.rowIndex, e.target);
				}
			};

			setTimeout(function() {

				$("#" + $scope.model.model.id).wijspread({ sheetCount: 1 });
				$scope.spreadjs = $("#" + $scope.model.model.id).wijspread("spread");

				$scope.sheet = $scope.spreadjs.getActiveSheet();
				//$scope.sheet.isPaintSuspended(true);
				$scope.sheet.fromJSON(JSON.parse($scope.model.model.content)); 
				//$scope.sheet.isPaintSuspended(false);
				$("#" + $scope.model.model.id).css("z-index", 100);
			},80);
	      	$compile(elem.contents())($scope);
	      
	    }
	};
}]);

BuildWidgetQuestionModule.directive('answerkeyGridblock', ['$compile', '$templateCache', 'WidgetQuestion', function($compile, $templateCache, WidgetQuestion) {
	return {
		restrict:"E", 
		template:'<wij-grid data="getData()" allowediting="true" showfooter="false" showheader="false"  columns-autogeneration-mode="none"  selection-mode="singleCell" ' +
					 	' currentCellChanging="pickCell" rendered="rendered"> ' +
					   	'<columns> ' +
					   		'<column data-key="letter" header-text=""></column>' +
					       '<column data-key="answer" header-text="Answers"></column> ' +
					     '</columns> ' +
				 	'</wij-grid> ',
		transclude : true,
		scope:true,
		replace: false,
	    require: 'ngModel',
	    
	    link: function($scope, elem, attr, ctrl) {
	    	WidgetQuestion.errors.linkError = "";
	      	$scope.model = $scope.$parent.$eval(attr.ngModel);
	      	attr.$set('id', $scope.model.model.id);
	      	$scope.getData = function() {
		    	return $scope.model.model.content;
		  	};
		  	$scope.pickCell = function(e, args) {
		  	
			};
			
	    	$compile(elem.contents())($scope);
	    }
	};
}]);


BuildWidgetQuestionModule.directive('answerkeyGridblockTabpane', ['$compile', '$templateCache', '$timeout','WidgetQuestion', function($compile, $timeout, $templateCache, WidgetQuestion) {
	return {
		restrict:"E", 
		template:'<table></table>'
				//	+ '<div class="span2">'
			+'<form novalidate ng-submit="addAnswer()" style="width:100%">'
			  			+ '<input class="input-block-level type="text" placeholder="Answer text" ng-model="newAnswer.text">'
							
					+ '</form>',
				//	+ '</div>',
		transclude : true,
		scope:{ngModel:"="},
		replace: false,
	    require: 'ngModel',
	    
	    link: function($scope, elem, attr, ctrl) {
	    	WidgetQuestion.errors.linkError = "";
	      	$scope.model = $scope.$parent.$eval(attr.ngModel);
	      	$scope.newAnswer = {};
	      	$scope.$on("show-answerkey-tab", function($event, obj) {
	      		if(obj.key == $scope.model.model.name) {
	      			$scope.initGrid();
	      			var selection = $scope.grid.wijgrid("selection");
	      			selection.addRows(obj.answer.rowIndex);
	      		}
	      	});
	      	if(attr.parentId != null)  {
	      		$scope.id = attr.parentId + $scope.model.model.id;
				attr.$set('id', attr.parentId + $scope.model.model.id);
	      	} else {
	      		attr.$set('id', $scope.model.model.id);
	      	}
	      	
		  	$scope.pickCell = function (e, args) {
     			
     			var pickedRow = angular.copy($scope.model.model.content[args.cell._ri]);
     			pickedRow.rowIndex = args.cell._ri;
     			$scope.$emit("picked-answer", pickedRow);
        	};
			
			$scope.initGrid = function() {
				$scope.grid = $("#" + $scope.id + " table").wijgrid({  
														data:$scope.model.model.content, 
														columns:[{type:"String", dataKey:"letter", label:"Letter"}, {type:"String", dataKey:"answer", label:"Answers"}],
														//currentCellChanging:$scope.pickCell, 
														 cellClicked : $scope.pickCell,
														allowColSizing:false, 
														selectionMode:"singleRow",
														ensureControl:true,
														allowEditing:true, 
														highlightOnHover:false,
														showheader:"false",
														columnsAutogenerationMode:"none"  }); 

			};
			$scope.addAnswer = function() {
				if($scope.newAnswer.text == "") return;
				$scope.model.model.content.push({"id":$scope.model.model.content.length, "answer":$scope.newAnswer.text, letter:window.letters[$scope.model.model.content.length]});
				$scope.newAnswer.text = "";
				$scope.grid.wijgrid("ensureControl", true);
			};
			$scope.initGrid();
	    	//$compile(elem.contents())($scope);
	    }
	};
}]);
