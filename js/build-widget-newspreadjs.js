
BuildWidgetQuestionModule.controller("NewSpreadJSController", function($scope, dialog, WidgetQuestion) {
	
	$scope.fontList = [
				"Arial",
				"Courier New",
				"Times New Roman",
				"Verdana",
				"Tahoma"
				];

	$scope.fontSizes = [  "10px", "12pt", "14pt", "16pt", "18pt", "20pt", "22pt", "24pt" ];

	$scope.cellStyles = [
		"whiteCell",
		"redCell",
		"greenCell",
		"blueCell",
		"blackCell"
	];

	

	$scope.style = {};
	$scope.style.font = $scope.fontList[0];
	$scope.style.fontSize = $scope.fontSizes[1];
	$scope.style.cellStyle = $scope.cellStyles[0];

	$scope.filterCurrentFont = function(f) {
		return $scope.style.font != f;
	};

	$scope.setFontSize = function(f) {
		$scope.style.fontSize = f;
	}

/*
	$scope.style = new $.wijmo.wijspread.Style();

	$scope.style.backColor = "white";
	$scope.style.borderLeft =new $.wijmo.wijspread.LineBorder("blue");
	$scope.style.borderTop = new $.wijmo.wijspread.LineBorder("blue");
	$scope.style.borderRight = new $.wijmo.wijspread.LineBorder("blue");
	$scope.style.borderBottom =  new $.wijmo.wijspread.LineBorder("blue");
	$scope.style.font = "Arial";
*/
/*
	style.borderLeft =new $.wijmo.wijspread.LineBorder("blue");
	style.borderTop = new $.wijmo.wijspread.LineBorder("blue");
	style.borderRight = new $.wijmo.wijspread.LineBorder("blue");
	style.borderBottom =  new $.wijmo.wijspread.LineBorder("blue");
*/

	$scope.model = WidgetQuestion;
	$scope.close = function() {
		dialog.close();
	};

	$scope.setFont = function(f) {
		$scope.style.font = f;
		$scope.activeSheet.isPaintSuspended(true);
		var sels = $scope.activeSheet.getSelections();
		console.dir(sels);

		for(var i = 0; i < sels.length; i++) {
			var sel = getActualCellRange(sels[i], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
			var cells = $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport);
			cells.font($scope.style.fontSize + " " + $scope.style.font);
		}
		$scope.activeSheet.isPaintSuspended(false);
	};

	$scope.setFontSize = function(f) {
		$scope.style.fontSize = f;
		$scope.activeSheet.isPaintSuspended(true);
		var sels = $scope.activeSheet.getSelections();
		for(var i = 0; i < sels.length; i++) {
			var sel = getActualCellRange(sels[i], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
			var cells = $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport);
			cells.font($scope.style.fontSize + " " + $scope.style.font);
		}
		$scope.activeSheet.isPaintSuspended(false);
	}


	$scope.merge = function() {
 		var sels = $scope.activeSheet.getSelections();
        var hasSpan = false;
        for (var n = 0; n < sels.length; n++) {
            var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
            if ($scope.activeSheet.getSpans(sel, $.wijmo.wijspread.SheetArea.viewport).length > 0) {
                for (var i = 0; i < sel.rowCount; i++) {
                    for (var j = 0; j < sel.colCount; j++) {
                        $scope.activeSheet.removeSpan(i + sel.row, j + sel.col);
                    }
                }
                hasSpan = true;
            }
        }
        if (!hasSpan) {
            for (var n = 0; n < sels.length; n++) {
                var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
                $scope.activeSheet.addSpan(sel.row, sel.col, sel.rowCount, sel.colCount);
            }
        }
    };

    $scope.alignleft = function() {
    	var align = $.wijmo.wijspread.HorizontalAlign.left;
        var sels = $scope.activeSheet.getSelections();
        for (var n = 0; n < sels.length; n++) {
            var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
            $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).hAlign(align);
        }
    };

	$scope.aligncenter = function() {
    	var align = $.wijmo.wijspread.HorizontalAlign.center;
        var sels = $scope.activeSheet.getSelections();
        for (var n = 0; n < sels.length; n++) {
            var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
            $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).hAlign(align);
        }
    };

	$scope.alignright = function() {
    	var align = $.wijmo.wijspread.HorizontalAlign.right;
        var sels = $scope.activeSheet.getSelections();
        for (var n = 0; n < sels.length; n++) {
            var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
            $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).hAlign(align);
        }
    };

	$scope.closeAndSave = function() {
		WidgetQuestion.model.question.widgets.push({ type: "spreadjs", model:{content: JSON.stringify($scope.activeSheet.toJSON()), id:"spreadjs_" + WidgetQuestion.model.question.widgets.length}, order:WidgetQuestion.model.question.widgets.length })
		if(WidgetQuestion.isSaved == true) {
			WidgetQuestion.isDirty = true;
		}
		dialog.close();
	};

	$scope.setCellStyle = function(cStyle) {
		var bgColor = "#ffffff";
		var txtColor = "#000000";
		$scope.style.cellStyle = cStyle;
		switch(cStyle) {
			case "redCell" :
				bgColor = "#F81B05";
				txtColor = "#ffffff";
				break;
			case "blueCell":
				bgColor = "#3EC3F5";
				txtColor = "#ffffff";
				break;
			case "greenCell" :
				bgColor = "#2CA322";
				txtColor = "#ffffff";
				break;
			case "whiteCell" :
				bgColor = "#ffffff";
				txtColor = "#000000";
				break;
			case "blackCell":
					bgColor = "#000000";
					txtColor = "#ffffff";
				break;
			}
		$scope.activeSheet.isPaintSuspended(true);
	    var sels = $scope.activeSheet.getSelections();
	    for (var n = 0; n < sels.length; n++) {
	        var sel = getActualCellRange(sels[n], $scope.activeSheet.getRowCount(), $scope.activeSheet.getColumnCount());
	        $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).backColor(bgColor);
	        $scope.activeSheet.getCells(sel.row, sel.col, sel.row + sel.rowCount - 1, sel.col + sel.colCount - 1, $.wijmo.wijspread.SheetArea.viewport).foreColor(txtColor);
	    }
	    $scope.activeSheet.isPaintSuspended(false);
	}


	setTimeout(function() {
		
		$("#spreadContainer").wijspread({ sheetCount: 1 });
		$scope.newSpread = $("#spreadContainer").wijspread("spread");
		$scope.activeSheet = $scope.newSpread.getActiveSheet();


	},111);
});

function getActualCellRange(cellRange, rowCount, columnCount) {
	if (cellRange.row == -1 && cellRange.col == -1) {
		return new $.wijmo.wijspread.Range(0, 0, rowCount, columnCount);
	} else if (cellRange.row == -1) {
		return new $.wijmo.wijspread.Range(0, cellRange.col, rowCount, cellRange.colCount);
	} else if (cellRange.col == -1) {
		return new $.wijmo.wijspread.Range(cellRange.row, 0, cellRange.rowCount, columnCount);
	}
	return cellRange;
} 