var ServiceURLS = {
	/*topics : "/restserver/index.php/api/testcenter/chapters/format/json",
	quizConfig: "/restserver/index.php/api/testcenter/quizlets/format/json"*/
	saveWidgetQuestionUrl:"/restserver/index.php/api/testcenter/save_tbs_question/format/json/",
	loadQuestionUrl:'/restserver/index.php/api/testcenter/tbs_questions/format/json/',
	updateQuestionUrl:'/restserver/index.php/api/testcenter/update_tbs_question/format/json/',
};
var BuildWrittenCommQuestionModule = angular.module("BuildWrittenCommQuestionModule", ["ui.bootstrap", "TopicsModule"])
			.config(function($routeProvider) {
				$routeProvider.when(
						'/new', {
							action:'new'
						}).when( '/edit/:qID', {
							//action:"load"
						}).otherwise({
							redirectTo:'/new'
						});
			});

BuildWrittenCommQuestionModule.service("QuestionModel", function($http, $rootScope, TopicService) {
	this.model = {};
	this.model.question = "";
	this.model.solution = "";
	this.model.id = "";
	this.model.deprecated = false;
	this.topics = TopicService;
	var self = this;
	self.isSaved = false;
	this.saveQuestion = function() {
		if(self.isSaved == false) {
			return $http.post(ServiceURLS.saveWidgetQuestionUrl, {	"question":encodeURIComponent(JSON.stringify({"question":self.model})), 
																	"type":"tbs-wc", 
		"deprecated": self.model.deprecated,
		"docID": $.cookie('docID'),
																	"topics":self.topics.questionTopics})
			.success(function (data, status, headers, config) {
			});
		} else {
			return $http.post(ServiceURLS.updateQuestionUrl, {
				"question":encodeURIComponent(JSON.stringify({"question":self.model})), 
				"topics":self.topics.questionTopics,
				"docID": $.cookie('docID'),
				"deprecated": self.model.deprecated,
				"type":"tbs-wc", 
				"id":self.questionID }).success(function (data, status, headers, config) {
					$rootScope.$broadcast("saveQuestionFinished", data);
					self.deserialize(data);
				

				});
		}
	};

	this.deserialize = function(data) {
			var q = JSON.parse(data.question_json);
			if(data.id != null) {
				self.questionID = data.id;
			} else if(q.id != null) {
				self.questionID = q.id;
			}
			q.question = JSON.parse(decodeURIComponent(q.question));
			self.topics.setTopics( data.topics );
			self.model = q.question.question;
		
	};

	this.loadQuestion = function(id) {
		return	$http.get(ServiceURLS.loadQuestionUrl +"?id=" + id).success(function(data, status, headers, config) {
			/*var q = JSON.parse(data[0].question_json);
			self.questionID = q.id;
			q.question = JSON.parse(decodeURIComponent(q.question));
			window.topics = q.topics.topics;
			self.model = q.question.question;
			*/
			self.deserialize(data[0]);
			self.isSaved = true;
		});
	};

});

BuildWrittenCommQuestionModule.controller("WCQuestionController", function($scope, $rootScope, $timeout, QuestionModel, TopicService) {
	$scope.getDocumentID = function() {
		if (typeof parseInt($.cookie('docID')) !== "number" || $.cookie('docID') == "0") {
			return false	
		}
		else {
			return $.cookie('docID');
		}
	}
	$scope.question = QuestionModel;
	$scope.topics = TopicService;

	$scope.deprecateQuestion = function() {
		$scope.question.deprecated = !$scope.question.deprecated;
	};
	
	$timeout(function() {
		tinymce.init({
		    selector: "#questionBlock",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor jbimages"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons jbimages",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.qeditor = ed;
					if(QuestionModel.model.question != null) {
						$scope.qeditor.setContent(QuestionModel.model.question);
					}


		    	});
		    }


		});


		tinymce.init({
		    selector: "#solutionBlock",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor jbimages"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons jbimages",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.seditor = ed;
					if(QuestionModel.model.solution != null) {
						$scope.seditor.setContent(QuestionModel.model.solution);
					}
				var hash = document.location.hash;
				if(hash.indexOf("edit") != -1) {
					var qid = hash.split("/edit/")[1];

					$scope.question.loadQuestion(qid).then(function() {
						if(QuestionModel.model.question != null) {
							$scope.qeditor.setContent(QuestionModel.model.question);
						}

						if(QuestionModel.model.solution != null) {
							$scope.seditor.setContent(QuestionModel.model.solution);
						}
					});
				} else {
					$scope.topics.setTopics( JSON.parse(decodeURIComponent($.cookie('tbs_topics'))).topics);
				}
		    	});
		    }
		});

	});




	$scope.saveQuestion = function() {
		QuestionModel.model.question = $scope.qeditor.getContent();
		QuestionModel.model.solution = $scope.seditor.getContent();
			var result = QuestionModel.saveQuestion();
			if (result.then) {
				result.then(function(obj) {
					if (obj.data) {
						if (obj.data.success) {
							if (window.location.href.indexOf("edit") >= 0) {
								if (window.location.href.indexOf("edit") >= 0) {
										$scope.alerts = [
											{"type":"alert-funtime", "message": "Question saved!"}
										];
								}
							}
							else {
								$scope.alerts = [
								{"type":"alert-funtime", "message": "Question saved. <a href='/testmodule-admin/build-wc-question.php#/edit/" + obj.data.id + "' target='_blank'>Edit question</a> or <a href='/testmodule-admin/index.php'>create new question</a>"}
								];
							}
						}
					}
				});
			}
			$scope.$on("saveQuestionFinished", function(evt, data) {
						if (data.success) {
							if (window.location.href.indexOf("edit") >= 0) {
									$scope.alerts = [
										{"type":"alert-funtime", "message": "Question saved!"}
									];
							}
							else {
								$scope.alerts = [
								{"type":"alert-funtime", "message": "Question saved. <a href='/testmodule-admin/build-wc-question.php#/edit/" + data.id + "' target='_blank'>Edit question</a> or <a href='/testmodule-admin/index.php'>create new question</a>"}
								];
							}
						}

			});
	};


	$scope.alerts = [];

});

BuildWrittenCommQuestionModule.controller("SaveController", function($scope, QuestionModel) {



});
