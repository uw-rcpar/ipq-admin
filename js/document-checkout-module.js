/*
ServiceURL's moved to the php file's <script> tag so it 
can be configured per-question type
*/
var DocumentCheckoutModule = angular.module("DocumentCheckoutModule", ['ui.bootstrap', 'ngSanitize'])
			.config(function($routeProvider) {

			});
DocumentCheckoutModule.service("DocService", function($http, $q, $rootScope) {
	var self = this;
	this.checkoutError = "";
	this.username = $.cookie("username");
	this.isAdmin = $.cookie("issuperadmin");
	//this.documents = [];
	this.hasDocument = null;


	this.getDocuments = function() {
		var promise = $q.defer();
		$http({method:'GET', url: ServiceURLS.getDocumentsUrl}).success(function (data, status, headers, config) {
				self.documents = data.documents;
				for(var i  = 0; i < self.documents.length; i++) {
					
					if(self.documents[i].checkedOutAt != "") {
						self.documents[i].checkedOutAtMoment =  moment(self.documents[i].checkedOutAt, "YYYY-M-DD, h:mm:ss a");
						self.documents[i].checkedOutAtDisplay = self.documents[i].checkedOutAtMoment.format('MM/DD/YY, h:mm:ss a');
						if(self.documents[i].checkedInAt == null) {
							self.documents[i].checkedInAt = "";
						} else if(self.documents[i].checkedInAt != "" ) {
							self.documents[i].checkedInAt =  moment(self.documents[i].checkedInAt, "YYYY-M-DD, h:mm:ss a");
							self.documents[i].checkedInAtDisplay = self.documents[i].checkedInAt.format('MM/DD/YY, h:mm:ss a');
							var now = self.documents[i].checkedInAt;
							var then = self.documents[i].checkedOutAtMoment;
							self.documents[i].duration = moment.utc(now.diff(then)).format("hh:mm:ss");
							self.documents[i].milliseconds = now.diff(then);
						}

					}
					if(self.documents[i].checkedOutBy == self.username && self.documents[i].checkedInAt == "") {
						self.hasDocument = self.documents[i];
					}
					if(self.documents[i].checkedOutBy == self.username || self.isAdmin) {
						self.documents[i].oldTitle = self.documents[i].title;
						self.documents[i].title = '<a href="/testmodule-admin/docs/' + encodeURIComponent(self.documents[i].title) + '" target="_blank">' + self.documents[i].title + "</a>";
						if(! self.isAdmin) {
							self.documents[i].isCheckedOut = true;
						}	
					} else {
						self.documents[i].isCheckedOut = false;
					}
				}
				promise.resolve(self.documents);
				
			});
		return promise.promise;
	};


	this.checkoutDocument = function(doc) {
		self.checkedOutDoc = doc;

		$http({method:'GET', url:ServiceURLS.checkoutDocumentUrl + "?id=" + doc.id}).success(function (data, status, headers, config){
				if(data.success == "1" || data.success == 1) {
					self.checkedOutDoc.checkedOutBy = self.username;
					self.checkedOutDoc.checkedOutAtMoment =  moment();
					self.checkedOutDoc.checkedOutAtDisplay = self.checkedOutDoc.checkedOutAtMoment.format('MM/DD/YY, h:mm:ss a');
					self.checkedOutDoc.oldTitle = self.checkedOutDoc.title;
					self.checkedOutDoc.title = '<a href="' + encodeURIComponent(self.checkedOutDoc.url) + '" target="_blank">' + self.checkedOutDoc.title + "</a>";
					$.cookie("docID", self.checkedOutDoc.id, "/");
					window.open(self.checkedOutDoc.url, "_blank");
				} else {
					self.checkoutError = "Someone just now checked out that document, pick another.";
				}
			});
	};


	this.checkinDocument = function(doc) {
		self.checkedInDoc = doc;

		$http({method:'GET', url:ServiceURLS.checkinDocumentUrl + "?id=" + doc.id}).success(function (data, status, headers, config){
				if(data.success == "1" || data.success == 1) {
					self.checkedInDoc.checkedInAt = moment();
					self.checkedInDoc.checkedInAtDisplay = self.checkedInDoc.checkedInAt.format('MM/DD/YY, h:mm:ss a');
						var now = self.checkedInDoc.checkedInAt;
						var then = self.checkedInDoc.checkedOutAtMoment;
						self.checkedInDoc.milliseconds = self.checkedInDoc.checkedInAt.diff(then);
						self.checkedInDoc.duration = moment.utc(self.checkedInDoc.checkedInAt.diff(then)).format("HH:mm:ss");
						self.checkedInDoc.title = self.checkedInDoc.oldTitle;
						self.hasDocument = null;
						$.removeCookie('docID');
				} else {
					self.checkoutError = "A problem occured, please contact steve or sam.";
				}
			});
	};

	this.flagDocument = function(doc) {
	
		$http.post( ServiceURLS.flagDocumentUrl, doc).success(function (data, status, headers, config){
			if(data.success == "1" || data.success == 1) {
				if(doc.report.doCheckIn) {
					self.checkinDocument(doc);
				}
			} else {
				self.checkoutError = "A problem occured, please contact steve or sam.";
				self.response = "This document has been flagged.";
			}
		});
	}

	this.search = function(query) {
		var promise = $q.defer();
		$http({method:'GET', url: ServiceURLS.searchUrl + encodeURIComponent(query)}).success(function (data, status, headers, config) {
				self.documents = data.documents;
				for(var i  = 0; i < self.documents.length; i++) {
					
					if(self.documents[i].checkedOutAt != "") {
						self.documents[i].checkedOutAtMoment =  moment(self.documents[i].checkedOutAt, "YYYY-M-DD, h:mm:ss a");
						self.documents[i].checkedOutAtDisplay = self.documents[i].checkedOutAtMoment.format('MM/DD/YY, h:mm:ss a');
						if(self.documents[i].checkedInAt == null) {
							self.documents[i].checkedInAt = "";
						} else if(self.documents[i].checkedInAt != "" ) {
							self.documents[i].checkedInAt =  moment(self.documents[i].checkedInAt, "YYYY-M-DD, h:mm:ss a");
							self.documents[i].checkedInAtDisplay = self.documents[i].checkedInAt.format('MM/DD/YY, h:mm:ss a');
							var now = self.documents[i].checkedInAt;
							var then = self.documents[i].checkedOutAtMoment;
							self.documents[i].duration = moment.utc(now.diff(then)).format("hh:mm:ss");
							self.documents[i].milliseconds = now.diff(then);
						}

					}
					if(self.documents[i].checkedOutBy == self.username && self.documents[i].checkedInAt == "") {
						self.hasDocument = self.documents[i];
					}
					//if(self.documents[i].checkedOutBy == self.username || self.isAdmin) {
						self.documents[i].oldTitle = self.documents[i].title;
						self.documents[i].title = '<a href="/testmodule-admin/docs/' + encodeURIComponent(self.documents[i].title) + '" target="_blank">' + self.documents[i].title + "</a>";
					
					if(!self.documents[i].checkedOutBy != "" ) {
							self.documents[i].isCheckedOut = true;	
					} else {
						self.documents[i].isCheckedOut = false;
					}
				}
				promise.resolve(self.documents);
				
			});
		return promise.promise;
	};

});

DocumentCheckoutModule.controller("CheckOutController", function($scope, $dialog, DocService) {

	$scope.documents = DocService.getDocuments();
	$scope.errors = {};
	$scope.username = $.cookie("username");
	$scope.isAdmin = $.cookie("issuperadmin");

	$scope.filterMode = "all";

	$scope.filterBy = function(mode) {
		$scope.filterMode = mode;
	};

	$scope.$watch(function() { return DocService.checkoutError;} , function(n) {
		if(n != undefined && n != "") {
			$scope.errors.checkoutError = n;
			$scope.documents = DocService.getDocuments();
		}
	});

	$scope.$watch(function() { return DocService.hasDocument;} , function(n) {
		$scope.currentDocument = DocService.hasDocument;
		if(n != undefined && n != "") {

			$.cookie('docID', $scope.currentDocument.id, { expires: 90, path: '/' });
		}
	});

	$scope.$watch(function() { return DocService.documents; } , function(n) {
		if(n != undefined && n != "") {
			$scope.documents = DocService.documents;
		}
	});


	$scope.checkoutDocument = function(doc) {
		DocService.checkoutDocument(doc);
	};

	$scope.checkinDocument = function(doc) {
		DocService.checkinDocument(doc);
	};

	$scope.isCheckedOut = function (doc) {
    	var answer = false;
    	if(doc.checkedOutBy != "" && doc.checkedInAt == "") {
    		answer = true;
    	}
        return answer;          
    };

    $scope.isCheckedIn = function (doc) {
    	var answer = false;
    	if(doc.checkedInAt != "") {
    		answer = true;
    	}
        return answer;          
    };

    $scope.isUnclaimed = function (doc) {
    	var answer = false;
    	if(doc.checkedOutBy == "") {
    		answer = true;
    	}
        return answer;          
    };

    $scope.reportIssue = function(doc) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/form-report-document-issue.php',
		    controller: 'ReportDocumentIssueController',
		    dialogClass: 'modal'
		};
	    var d = $dialog.dialog(opts);
	    d.open();
    };

    $scope.searchDocs = function() {
    	$scope.documents = DocService.search($scope.searchQuery);
    }
});




DocumentCheckoutModule.controller("ReportDocumentIssueController", function($scope, dialog, DocService) {
	$scope.currentDocument = DocService.hasDocument;
	$scope.currentDocument.report = {};
	$scope.currentDocument.report.doCheckIn = false;
	$scope.closeOptionBox = function(){
		dialog.close();
	};
	$scope.saveReport = function() {
		$scope.currentDocument.report.reportText = $scope.editor.getContent();
		DocService.flagDocument($scope.currentDocument);
		dialog.close();
	};

	setTimeout(function() {
		tinymce.init({
		    selector: "#reportText",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.editor = ed;
		    	});
		    }
		});
	}, 100);


});
