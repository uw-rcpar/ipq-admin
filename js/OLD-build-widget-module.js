var ServiceURLS = {
	topics : "/restserver/index.php/api/testcenter/chapters/format/json",
	quizConfig: "/restserver/index.php/api/testcenter/quizlets/format/json"
};
var BuildWidgetQuestionModule = angular.module("BuildWidgetQuestionModule", ["ui.bootstrap"])
			.config(function($routeProvider) {

			});


BuildWidgetQuestionModule.service("WidgetQuestion", function() {
	this.model = {};
	this.model.widgets = [];
});

BuildWidgetQuestionModule.controller("ToolBarController", function($scope, $dialog, WidgetQuestion) {

	$scope.newRichTextWidget = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-richtext.php',
		    controller: 'NewRichtextController'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};
	
	$scope.newGridWidget = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/widgetquestion-new-grid.php',
		    controller: 'NewGridController'
		  };
	    var d = $dialog.dialog(opts);
	    d.open();
	};
	$scope.newSpreadsheetWidget = function() {

	};	

	$scope.newAnswerKey = function() {
		
	};
});

BuildWidgetQuestionModule.controller("QuestionRenderer", function($scope, WidgetQuestion) {

	$scope.$watch(WidgetQuestion.model , function(o, n) {
		console.log(o, n);
	});
});

BuildWidgetQuestionModule.controller("NewRichtextController", function($scope, dialog, WidgetQuestion) {
	setTimeout(function() {
		tinymce.init({
		    selector: "#newTextBlockEditor",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor"
		    ],
		    statusbar:false,
		    toolbar1: "preview undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons",
		    image_advtab: true,
		    entity_encoding: "raw",
		    setup:function(ed) {
		    	ed.on("init", function(){
					$scope.editor = ed;
					//$scope.editor.setContent();
		    	});
		    }
		});
	}, 111);
	$scope.close = function() {
		dialog.close();
	}
	$scope.saveAndClose = function() {
		WidgetQuestion.model.widgets.push({ type: "richtext", content:$scope.editor.getContent(), order:WidgetQuestion.model.widgets.length })
		dialog.close();
	};
});

BuildWidgetQuestionModule.controller("NewGridController", function($scope, dialog, WidgetQuestion) {
	$scope.model = {};
	$scope.model.columns = [];
	$scope.newColumn = {"label":"", "key":"", "width":"" };
	$scope.columnForm = false;

	$scope.$watch("$scope.model.columns", function(o, n) {

	});

	$scope.toggleColumnFormVisibility = function() {
		if($scope.columnForm == false) {
			$scope.newColumn = {"label":"", "key":"", "width":"" };
		}
		$scope.columnForm = !$scope.columnForm;
	};
	$scope.addNewColumn = function() {
		$scope.model.columns.push(angular.copy($scope.newColumn));
		$scope.toggleColumnFormVisibility();
	};

	$scope.close = function() {
		dialog.close();
	};

	$scope.saveAndClose = function() {
		WidgetQuestion.model.widgets.push({ type: "grid", content:$scope.editor.setContent(), order:WidgetQuestion.model.widgets.length })
		dialog.close();
	};
});

BuildWidgetQuestionModule.directive('questionWidget', function($compileProvider) {
	return {
		restrict:"E", 

	};
});