<?php
//no cookie, no showie. Cookie set on authorize.php
if (!$_COOKIE['isadmin']) {
  //back to login
  header("Location: index.php");
  die();
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
  <link href="css/styles.css" rel="stylesheet" media="screen">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
  <script src="js/jquery.cookie.js"></script>

    <script src="js/angular.min.js"></script>
    <script src="js/angular-sanitize.js"></script>

   
  <script src="js/tinymce4/tinymce.min.js"></script>
  <script src="js/masonry.js"></script>
  <script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
  <script src="js/NavModule.js"></script>
  <script src="js/TopicsModule.js"></script>
  <script src="js/build-wc-module.js"></script>

  <script>
    
    
    angular.element(document).ready(function() {
      angular.bootstrap($("#navModule"), ["NavModule"]);
      angular.bootstrap($("#buildWCQuestionApp"), ["BuildWrittenCommQuestionModule"]);
    });
  </script>

   <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php include 'navbar.php'; ?>
<div id="buildWCQuestionApp">


    <div class="container"  ng-controller="WCQuestionController">
    <div class="row">
      <div class="span10">
        <div ng-repeat="alert in alerts" class="alert {{alert.type}}"><span ng-bind-html-unsafe="alert.message"></span></div>
      </div>
    </div>


      <div class="row">
        <p class="lead">Written Communication Builder</p>

        
      </div>
	<div class="alert alert-danger" ng-hide="getDocumentID()">You have not checked out a document. Please <a href="checkout-question-documents-widget-WC.php">check out a document before continuing</a>.</div>

	<div class="row" ng-hide="topics.course"><strong>Loading...</strong></div>

      <div class="row" id="question-region" ng-show="getDocumentID() && topics.course">
        <div class="span9">
          <p>Question:</p>
          <textarea id="questionBlock" style="width:100%;height:320px;">
          </textarea>

          <p>Solution:</p>
          <textarea id="solutionBlock" style="width:100%;height:320px;">
          </textarea>

        </div>

      </div>
      <div class="row">
      <div class="span10">
        <div ng-repeat="alert in alerts" class="alert {{alert.type}}"><span ng-bind-html-unsafe="alert.message"></span></div>
      </div>
    </div>

      
  <div class="navbar navbar-inverse navbar-fixed-bottom roger">
        <div class="navbar-inner">
          <div class="container">
          <div class="row">

        <div class="span5 pull-left">
          <div class="btn-group dropup">
                  <a class="btn btn-inverse" disabled="disabled">{{topics.course}}</a>
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown">Topics</a>
                  <ul class="dropdown-menu btn-inverse">
                    <li ng-repeat="topic in topics.questionTopics">
                      <a><i class="fa fa-times" ng-click="topics.removeTopic(topic)" tooltip="Remove this topic"></i>
                      {{topic.prefix}} : <strong>{{topic.topic}}</strong></a>
                    </li>
                    <li><a ng-click="topics.addTopic()" class="add-new"><i class="fa fa-plus"></i> Add topic</a></li>
                    
                   
                  </ul>
                </div>
        </div>
              <!--

              <div class="span5 pull-left">
                <div class="btn-group dropup">
                  <a class="btn btn-inverse">{{feed.currentUserList}}</a>
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a ng-click="listByUser('administrator')" >administrator</a></li>
                    <li><a ng-click="listByUser('dlee')">dlee</a></li>
                    <li><a ng-click="listByUser('sarahc')">sarahc</a></li>
                   
                  </ul>
                </div>
          

              <div class="btn-group dropup" ng-show="feed.feedMode != 'default'">
              <button class="btn btn-inverse" ng-click="feed.previousQuestionInUserList()" tooltip="previouse question" tooltip-trigger="mouseenter"><i class="icon-white icon-arrow-left"> </i></button>
              
              <button class="btn btn-inverse dropdown-toggle" data-toggle="dropdown"  tooltip="jump to question" tooltip-trigger="mouseenter">{{feed.listIndex}} of {{feed.userlist.length}} <i class="icon-white icon-share-alt" > </i></button>
                <ul class="dropdown-menu">
                  <li><form ng-submit="feed.jumpToPosition()"><input type="text" placeholder="list position" ng-model="feed.listIndex" /></form></li>
                 
                </ul>
              <button class="btn btn-inverse" ng-click="feed.nextQuestionInUserList()" tooltip="next question" tooltip-trigger="mouseenter"><i class="icon-white icon-arrow-right"> </i></button>

              </div>

            </div>
-->

                  <div class="span2 pull-right">
                    <div class="btn-toolbar">
                      <div class="btn-group">
                        <button class="NextButton pull-right btn btn-success" ng-click="saveQuestion()">
                          <span ng-show="question.isSaved == false">Save question</span>
                          <span ng-show="question.isSaved == true">Update question {{question.questionID}}</span>
                        </button>
                        </div>
			<div class="btn-group">
				<button class="NextButton btn" ng-hide="question.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Disable</button>  
				<button class="NextButton btn" ng-show="question.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Enable</button>  
			</div>
                      </div>
                  </div>
                </div>
                
                </div>
              </div>
          </div>
      </div>
  </div>
</div>




</body>
</html>
