<div class="navbar navbar-inverse navbar-fixed-top" id="navModule" ng-controller="NavigationController">
  <div class="navbar-inner">
  	<div class="span12">
    <ul class="nav nav-pills">	
    	<a class="brand" href="#">RogerCPAReview</a>
      <li><a href="index.php">New Question</a></li>
      <li><div class="btn-group">
            <a class="btn btn-inverse" href="{{currentdocnav.url}}">{{currentdocnav.title}}</a>
            <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" ><span class="caret"></span></a>
            <ul class="dropdown-menu">

              <li ng-repeat="nav in docnav | filter:noDivisor"  ng-show="nav.current == false"><a href="{{nav.url}}">{{nav.title}}</a></li>
              <li ng-repeat="nav in docnav | filter:useDivisor" class="divider" ng-show="nav.current == false"></li>
              <li ng-repeat="nav in docnav | filter:useDivisor" ng-show="nav.current == false"><a href="flagged-documents.php"><i class="icon icon-flag"></i> Flagged documents</a></li>
            </ul>
          </div>
</li>
      <li><a href="search-edit.php">Search & Edit Questions</a></li>
      <li><a href="/testmodule-admin/question_viewer/">Questions</a></li>
    </ul>
    <form name="questionSearch" ng-submit="doSearch()">
        <div class="control-group form-horizontal pull-right span1" style="padding-top:4px">
          <div class="controls">
            <div class="input-prepend">

              <span class="add-on"><i class="icon-search"></i></span>
              <input class="span2" id="query" name="query" type="text" ng-model="query" placeholder="Search questions">
	      <select class="span2" ng-model="searchType" ng-init="searchType = 'IDs'">
			<option selected>IDs</option>
			<option>Keyword</option>
	      </select>
            </div>
          </div>
        </div>
    </form>
	</div>
  </div>
</div>
