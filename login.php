<!DOCTYPE html>
<html lang="en">
<head>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		LoginApp = function(){};
		LoginApp.prototype = {
			user:null,
			loginUrl:'/restserver/index.php/api/auth/admin_login/format/json',
		};
		LoginApp.prototype.init = function() {
			var self = this;
			if(self.user == null) {
				//isadmin cookie set by restserver admin_login_post(). No cookie gets login screen
				self.showLogin();
			
			}
		}
		LoginApp.prototype.showLogin = function() {
			var self = this;
			$("#modal-login").modal();
			$("#modal-login .btn-primary").on('click', function(e) {
				e.preventDefault();
				self.doLogin(	$('[name=login_email]').val(), 
								$('[name=login_password]').val()	);
			});
		};


window.app = null;
$(function() {
	window.app = new LoginApp();
	window.app.init();
});
	</script>
	 <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
</head>
<body>


	<div class="container">

<form class="form-signin" method="post">
<div class="modal hide fade" id="modal-login" data-backdrop="static">
	<div class="modal-header">
    	<h3>Please Sign In</h3>
  	</div>
  	<div class="modal-body">
        <input type="text" class="input-block-level" placeholder="Username" name="login_email">
        <input type="password" class="input-block-level" placeholder="Password"  name="login_password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
  </div>
  <div class="modal-footer">
  	<input type="submit" class="btn btn-inverse pull-right" value="Log in"/>
  </div>
</div>
 </form>


</body>
</html>


