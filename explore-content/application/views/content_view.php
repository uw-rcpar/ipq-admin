<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/testmodule-admin/explore-content/"></base>
    <title>Roger CPA Review Courseware</title>


    <!-- Bootstrap -->
    <link href="<?=asset_url();?>css/normalize.css" rel="stylesheet">
    <link href="<?=asset_url();?>js/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=asset_url();?>css/styles.css" rel="stylesheet">
    <link href="<?=asset_url();?>css/fontawesome.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body ng-app="ContentModule">
		<div class="chapter-label"  ng-controller="ChapterTitleController">
			<div class="page-header">
				<h1><span class="label label-default">{{chapterPrefix}}</span> {{chapterLabel}}</h1>
			</div>
		</div>

	<div class="container-fluid">	

		<div class="row-fluid">
		  	
		<div class="panel panel-dark"  style="position:fixed;top:15px;left:15px;z-index:400" ng-controller="NavigationController">
			<!-- <div class="panel-heading">{{currentChapterTitle}}</div> -->
			<div class="panel-body">
			<ul class="nav nav-pills nav-stacked nav-pills-dark">
				<li class="dropdown" ng-repeat="section in sections" >
					<a data-toggle="dropdown" type="button" class="dropdown-toggle" ng-class="{active: section.section == currentSection}">{{section.section}} <small><i class="fa fa-chevron-right"></i></small></a>
					<ul class="dropdown-menu" role="menu" ng-class="{'double-li':section.section == 'FAR' || section.section == 'REG'}">
					    <li ng-repeat="chapter in section.chapters" ng-class="{active: chapter.chapter == currentChapterTitle}">
					    		<a href="#/content/{{section.section}}/{{chapter.order}}"><span class="label label-info">{{chapter.prefix}}</span> {{chapter.label}}</a>
					   	</li>
				  	</ul>
				  </li>
			</ul>
			</div>
		</div>



		<div class="col-lg-9 col-md-12 col-sm-12">
			
			<ng-view>

  			</ng-view>
		</div>

	</div>

</div>

  	<div class="btn-group"  ng-controller="NavigationController"  style="position:fixed;bottom:15px;right:15px;z-index:400" >
  		<button type="button" class="btn btn-dark" ng-click="prevChapter()"><i class="fa fa-arrow-circle-left"></i> Previous chapter</button>
		<button type="button" class="btn btn-dark"  ng-click="nextChapter()">Next chapter <i class="fa fa-arrow-circle-right"></i></button>
	</div>
  
    <script src="<?=asset_url();?>js/jquery.min.js"></script>
    <script src="<?=asset_url();?>js/d3-master/d3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=asset_url();?>js/angular.min.js"></script>
    <script src="<?=asset_url();?>js/angular-route.min.js"></script>
    <script src="<?=asset_url();?>js/ui-bootstrap-tpls-0.11.0.min.js"></script>
    <!---
    <script src="js/video-js/video.dev.js"></script>
    <script src="//vjs.zencdn.net/4.5/video.js"></script>
	-->
    <script src="<?=asset_url();?>js/app.js"></script>
  </body>
</html>