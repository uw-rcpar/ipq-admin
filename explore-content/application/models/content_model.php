<?php
class Content_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_questions_answers($id = 0)
	{
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->join('multiple_choice_answers', 'multiple_choice_answers.question_id = multiple_choice_questions.id');
		$this->db->where('multiple_choice_questions.id', $id);	
		
		
		$query = $this->db->get();
		return $query->result_array();

	}
	
	
	//get a number of multiple choice questions for editing
	public function get_questions($id)
	{
		//if id is not 0 then edit that question
		if ($id!=0) {
			$query = $this->db->get_where('multiple_choice_questions', array('id' => $id));
			return $query->result_array();
		}
			
			
		//get the latest 25 questions	
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->order_by('id', 'desc'); 
		$this->db->limit(25);
				
		$query = $this->db->get();
		return $query->result_array();

	}
	
	//get just the question ids for an editing screen for a particular user
	public function get_question_ids($username)
	{
		$this->db->select('id');
		$this->db->from('multiple_choice_questions');
		$this->db->where('createdBy', $username);
				
		$query = $this->db->get();
		return $query->result_array();
	}
	
	/**
	 * Get questions for quizlets
	 * @param $type must be "multiple choice" or "TBS"
	 * @param $topics must be comma delimited list 
	 * @param $filter_ids comma delimited list of ids that should NOT be returned
	 */
	public function get_quizlets_questions($type,$topics,$no_questions,$filer_ids='')
	{
			
		
					
		if ($type != 'tbs') {
			
			
			$query = $this->db->query("SELECT q.id, q.difficulty, q.topic_id, q.question 
										FROM multiple_choice_questions q, question_topics t 
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id in ($topics)
										group by q.id, q.difficulty, q.topic_id, q.question 
										ORDER BY RAND() LIMIT $no_questions");				
			
		} else {
			
			if (isset($_COOKIE['hasNASBA']) && $_COOKIE['hasNASBA']=='false') {
				//exclude tbs-reseach
				$tbs_sql= "type <> 'tbs-research' AND";
				
			} else {
			    
				$tbs_sql= '';
			
			}
			
			
			
			
			$query = $this->db->query("SELECT q.id, q.difficulty, q.topic_id, q.question_json, q.type 
										FROM task_based_questions q, question_topics t 
										where $tbs_sql q.id = t.question_id and t.question_type = 'tbs' and t.topic_id in ($topics)
										group by q.id, q.difficulty, q.topic_id, q.question_json, q.type
										ORDER BY RAND() LIMIT $no_questions");
									
			/*$query = $this->db->query("SELECT * FROM task_based_questions
									WHERE type = 'tbs-research' 
									ORDER BY RAND() LIMIT $no_questions");*/
			
		}
		
		
		return $query->result_array();

	}
	//get the quizlet answers
	//@param $question_id_list a list of question ids returned from get_quizlet_questions
	public function get_quizlets_answers($question_id_list)
	{
		$query = $this->db->query('SELECT * FROM multiple_choice_answers
									WHERE question_id
									IN ('.$question_id_list.' )');
		
		return $query->result_array();

	}
	//get topics and sub topics
	public function get_topics($section = 'far')
	{
			
		
		
		$query = $this->db->query("SELECT *, chapters.order AS chapter_prefix, topics.order AS topic_prefix 
									FROM chapters, topics 
									WHERE topics.chapter_id = chapters.id
									AND chapters.section = '$section'
									order by chapters.section, chapters.order, topics.order");	
		
		
		//$query = $this->db->get();
		return $query->result_array();

	}
	
	//get chapters and topics by id list
	public function get_topics_ids($id_list)
	{
		$query = $this->db->query('SELECT *, topics.id as topic_id FROM topics, chapters
			where (chapters.id = topics.chapter_id) and
			(topics.id IN ('.$id_list.'))');
			
		
		
		return $query->result_array();

	}
	
	//get student progress info
	public function get_student_info($student_id, $question_id=0)
	{
		$this->db->select('*');
		$this->db->from('student_progress');
		$this->db->join('student_progress_answers', 'student_progress.student_id = student_progress_answers.student_id');
		$this->db->where('student_progress_answers.question_id = student_progress.question_id'); 
		$this->db->where('student_progress.student_id', $student_id); 
		//if question id is passed
		if ($question_id != 0) {
			$this->db->where('student_progress_answers.question_id', $question_id); 
		}
		$this->db->order_by('student_progress_answers.question_id');		
		$query = $this->db->get();
		return $query->result_array();

	}
	public function update_student_info($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('students', $data); 
		
		return $result = array('success' => TRUE );

	}
	//just get the student data based on id
	public function get_student($id){
		$query = $this->db->get_where('students', array('id' => $id));
		return $query->result_array();
	}
	//just get the student data based on coldfusion id
	public function get_student_by_cfid($cfid){
		$query = $this->db->get_where('students', array('cf_student_id' => $cfid));
		return $query->result_array();
	}
	
	
	/**
	 * insert record in student_progress table
	 * data array needs these paramenters
	 * 
	 * student_id - required
	 * question_id - required
	 * answer_id - required
	 * correct_answer -required
	 * notes
	 * bookmark - default false
	 */
	public function set_student_progress($data)
	{
		$this->db->insert('student_progress', $data); 
		return $data;

	}
	/**
	 * Insert the question and return the new ID
	 */
	 public function set_question($data){
	 	
		//get the section
		$query = $this->db->query('SELECT *
									FROM topics, chapters
									WHERE topics.chapter_id = chapters.id
									AND topics.id IN ('.$data['topic_id'].')');
		$topic_data = $query->result_array();
		
		$data['section'] = $topic_data[0]['section'];
		
	 	$this->db->insert('multiple_choice_questions', $data);
		//return the new question id
		$data['question_id']=$this->db->insert_id();
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $data['question_id'],
			   'topic_id' => $topic_id,
			   'question_type' => 'mcq'
			);
			
			$this->db->insert('question_topics', $myfields);
			
		}
		
		return $data['question_id'];
		
	 }
	/**
	 * Insert the answer
	 */
	 public function set_answer($data){
	 	$this->db->insert('multiple_choice_answers', $data);
		//return the new question id
		$data['answer_id']=$this->db->insert_id();
		return $data;
		
	 }
	 /**
	 * Insert the correct answer
	 */
	 public function set_correct_answer($data, $id){
	 	$this->db->update('multiple_choice_questions', $data, "id = ".$id);
		return $data;
		
	 }
	 /**
	  * check for administrator at login
	  */
	  public function get_login($username, $password)
	{
		$this->db->select('*');
		$this->db->from('students');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		
		$query = $this->db->get();
		return $query->result_array();

	}
	/**
	  * get the number of multiple choice questions
	  */
	public function get_num_mcq($section)
	{
		$query = $this->db->query('Select count(*) as records, topic_id
									From multiple_choice_questions
									where section = "'.$section.'"
									group by topic_id');
		return $query->result_array();

	}
	/**
	  * get the number of task based simulations
	  */
	public function get_num_tbs($section)
	{
		$query = $this->db->query('Select count(*) as records, topic_id
									From task_based_questions
									where section = "'.$section.'"
									group by topic_id');
		return $query->result_array();

	}
	 /* Update multiple choice questions for admin edit
	 * @param $data is an array with updatedOn, updatedBy, question, difficulty
	 * @param $id is the questionID
	 */
	public function update_mc_question($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('multiple_choice_questions', $data); 
		
		$topic_ids=explode(",", $data['topic_id']);
		
		//blow out the old entries
		$this->db->delete('question_topics', array('question_id' => $id, 'question_type' => 'mcq')); 
		
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $id,
			   'topic_id' => $topic_id,
			   'question_type' => 'mcq'
			);
			
			$this->db->insert('question_topics', $myfields);
		}
		
		return $result = array('success' => TRUE );
		

	}
	//update the choices 
	public function update_mc_answers($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('multiple_choice_answers', $data); 
		
		return $result = array('success' => TRUE );
		

	}
	/**
	 * delete questions and answers
	 * @param $id is the questionID
	 * 
	 */
	public function delete_question($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('multiple_choice_questions');
		
		
		//delete from the answers table
		$this->db->where('question_id', $id);
		$this->db->delete('multiple_choice_answers');
		
		
		return $result = array('success' => TRUE );
		
	}
	
	public function delete_tbs_question($id)
	{
		//delete from the questions table
		$this->db->where('id', $id);
		$this->db->delete('task_based_questions');
		
		return $result = array('success' => TRUE );
		
	}
	
	//delete an answer or choice
	public function delete_choice($id)
	{
		
		//delete from the answers table
		$this->db->where('id', $id);
		$this->db->delete('multiple_choice_answers');
		
		
		return $result = array('success' => TRUE );
		
	}
	
	
	public function save_quizlet($data){
		$this->db->insert('quizlet_sessions', $data);
		
		//return the new quizlet id
		$result['quizlet_id']=$this->db->insert_id();
		return $result;
	}
	
	public function save_quizlet_details($data){
		$this->db->insert('quizlet_sessions_details', $data);
		
		//return the new quizlet id
		$result['quizlet_detail_id']=$this->db->insert_id();
		return $result;
	}
	
	//save tbs questions
	public function save_tbs_question($data){
		
		//get the section
		$query = $this->db->query('SELECT *
									FROM topics, chapters
									WHERE topics.chapter_id = chapters.id
									AND topics.id IN ('.$data['topic_id'].')');
									
		$topic_data = $query->result_array();
		
		$data['section'] = $topic_data[0]['section'];
		
		$this->db->insert('task_based_questions', $data);
		
		//return the new quizlet id
		$result['id']=$this->db->insert_id();
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $result['id'],
			   'topic_id' => $topic_id,
			   'question_type' => 'tbs'
			);
			
			$this->db->insert('question_topics', $myfields);
			
		}
		
		
		return $result;
	}
	
	//update tbs question
	public function update_tbs_question($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update('task_based_questions', $data); 
		
		//put the topics in the relational table
		$topic_ids=explode(",", $data['topic_id']);
		
		//blow out the old entries
		$this->db->delete('question_topics', array('question_id' => $id, 'question_type' => 'tbs')); 
		
		
		foreach ($topic_ids as $topic_id) {
				
			$myfields = array(
			   'question_id' => $id,
			   'topic_id' => $topic_id,
			   'question_type' => 'tbs'
			);
			
			$this->db->insert('question_topics', $myfields);
		}

		
		
		return $result = array('success' => TRUE );
		

	}
	
	//Search questions and answers
	public function search_questions_answers($search)
	{
		$this->db->select('*');
		$this->db->from('multiple_choice_questions');
		$this->db->join('multiple_choice_answers', 'multiple_choice_answers.question_id = multiple_choice_questions.id');
		$this->db->like('multiple_choice_questions.question', $search);
		$this->db->or_like('multiple_choice_answers.choice', $search); 
		
		$query = $this->db->get();
		return $query->result_array();

	}
	/**
	 * Search the TBS json
	 * @param $search must be url encoded with % escaped with a | pipe
	 */
	public function search_tbs_questions($search)
	{
				
		$query = $this->db->query("SELECT * FROM task_based_questions where question_json like '%$search%' escape '|'");	
		return $query->result_array();

	}

	public function get_tbs_questions($id=0,$type='none',$username='none')
	{
		//if passing an id get it
		if ($id !=0 ) {
			$query = $this->db->get_where('task_based_questions', array('id' => $id));
			return $query->result_array();
		}
		
		//if type is passed, get the last 25 of those
		if ($type != 'none' && $username != 'none' ) {
				
			
			$this->db->select('*');
			$this->db->from('task_based_questions');	
			//$this->db->where('type', $type);
			//$this->db->where('type', 'tbs-research');	
			//$this->db->where('type <> "tbs-wc"', $type);
			$this->db->where('createdBy', $username);	
			$this->db->order_by('id', 'desc'); 
			//$this->db->limit(25);
			
			$query = $this->db->get();
			
			return $query->result_array();
		}
		
		//get the latest 25 questions	
		$this->db->select('*');
		$this->db->from('task_based_questions');
		$this->db->order_by('id', 'desc'); 
		//$this->db->limit(25);
				
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_all_mc()
	{
		$query = $this->db->get('multiple_choice_questions');
		return $query->result_array();
	}
	
	public function get_all_tbs()
	{
		$query = $this->db->get('task_based_questions');
		return $query->result_array();
	}
	
	public function get_chapters($id)
	{
		$query = $this->db->get_where('chapters', array('id' => $id));
		return $query->result_array();
	}
	
	public function set_issue_report($data)
	{
		$this->db->insert('issue_reports', $data); 
		return $data;

	}
	
	//get saved sessions
	public function get_saved_sessions($student_id)
	{
		$data = array(
			'student_id' => $student_id, 
			'active' => 1
		);
		$this->db->order_by('id', 'desc'); 
		$query = $this->db->get_where('quizlet_sessions', $data);
		return $query->result_array();
	}
	
	public function get_count_mc($topic_id)
	{
		$query = $this->db->query("SELECT Count( * ) AS total 
								FROM multiple_choice_questions 
								WHERE find_in_set($topic_id, topic_id )");		
		$mytotal=$query->result_array();
		return $mytotal[0]['total'];
	}
	
	public function get_mc_by_ids($question_id_list)
	{
		$query = $this->db->query('SELECT * FROM multiple_choice_questions
									WHERE id IN ('.$question_id_list.' )
									ORDER BY id');
		
		return $query->result_array();

	}
	
	public function get_tbs_by_ids($question_id_list)
	{
		$query = $this->db->query('SELECT * FROM task_based_questions
									WHERE id IN ('.$question_id_list.' )
									ORDER BY id');
		
		return $query->result_array();

	}

	public function delete_session($id)
	{
		//delete from the questions table
		//$this->db->where('id', $id);
		//$this->db->delete('quizlet_sessions');
		
		//set as inactive
		$data=array(
		
			'active'=>0
		);
		$this->db->where('id', $id);
		$this->db->update('quizlet_sessions', $data);
		
		return $result = array('success' => TRUE );
		
	}
	
	public function delete_session_details($id)
	{
		//delete from the questions table
		//$this->db->where('quizlet_id', $id);
		//$this->db->delete('quizlet_sessions_details');
		//set as inactive
		$data=array(
		
			'active'=>0
		);
		$this->db->where('quizlet_id', $id);
		$this->db->update('quizlet_sessions_details',$data);
		
		return $result = array('success' => TRUE );
		
	}
	
	public function update_quizlet_session($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('quizlet_sessions', $data); 
		
		return $result = array('success' => TRUE );	
		
	}

	/*
	 * get mcqs by topicID list
	*/
	
	public function get_num_mcq_by_topic($topic_ids)
	{
		$query = $this->db->query("SELECT Count(distinct q.id) as mcq 
									FROM multiple_choice_questions q, question_topics t 
									where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id in ($topic_ids)");
									
		return $query->result_array();

	}
	
	/*
	 * get mcqs by topicID list
	*/
	
	public function get_num_tbs_by_topic($topic_ids)
	{
		$query = $this->db->query("SELECT Count(distinct q.id) as tbs 
									FROM task_based_questions q, question_topics t 
									where q.id = t.question_id and t.question_type = 'tbs' and t.topic_id in ($topic_ids)");
									
		return $query->result_array();

	}
	
	public function get_mc_by_topicids($topicids,$type='mcq')
	{
		if ($type != 'tbs') {
			
			
			$query = $this->db->query("SELECT q.*
										FROM multiple_choice_questions q, question_topics t
										where q.id = t.question_id and t.question_type = 'mcq' and t.topic_id in ($topicids)
										group by q.id, q.difficulty, q.topic_id, q.question");
			
		}
		
		return $query->result_array();
	}
	
	/*
	 * for the docs manager in manage_docs controler
	 * see if doc file exists
	 */
	 
	public function get_doc($filename, $type){
		$query = $this->db->get_where('documents', array('title' => $filename, 'type'=>$type));
		return $query->result_array();
	}
	
	//get docs by id
	public function get_document($id){
		$query = $this->db->get_where('documents', array('id' => $id));
		return $query->result_array();
	}
	
	public function get_docs($type){
		$query = $this->db->get_where('documents', array('type' => $type));
		return $query->result_array();
	}
	
	public function get_flagged_docs(){
		$this->db->where('isFlagged', 1);
		$query = $this->db->get('documents');
		return $query->result_array();
	}

	public function set_doc($data){
		$this->db->insert('documents', $data);
		//return the new question id
		$result['id']=$this->db->insert_id();
		return $result;
	}
	
	public function checkout_doc($data, $id){
		$this->db->where('id', $id);
		$this->db->update('documents', $data);
		$result=array('success'=>1);
		return $result;
	}
	
	public function flag_document($data, $id){
		$this->db->where('id', $id);
		$this->db->update('documents', $data);
		$result=array('success'=>1);
		return $result;
	}
}