<div>
  <div id='svg-tools' class='panel panel-dark panel-sm'>
    <div class='panel-heading'>Topic connections</div>
      <div class='panel-body'>  
        <div class="btn-group btn-group-sm">
          <label class="btn btn-dark" ng-model="toggleChordsModel"  btn-radio="'show'">show</label>
          <label class="btn btn-dark" ng-model="toggleChordsModel" btn-radio="'hide'">hide</label>
         </div>
         
      </div>
    </div>

  <div id='download-tools' class='panel panel-dark panel-sm'>
    <div class='panel-body'>  
      <div class="btn-group btn-group-sm">
        <button class="btn btn-dark" ng-click="downloadXLS()"><i class='fa fa-download'></i> Download XLS</button>
      </div>
    </div>
    <div class='panel-body'>  
      <div class="btn-group btn-group-sm">
        <button class="btn btn-dark" ng-click="downloadTBSXLS()"><i class='fa fa-download'></i> Download TBS XLS</button>
      </div>
    </div>
  </div>

    <div id="svg-details">
        <div class="panel panel-default">
          <div class="panel-heading">
          <h4 clas="panel-title">Content details</h4>
          </div>
          <!--
          <div class="panel-body">

          </div>-->
            <table class="table">
                <tbody>
                <tr>
                  <td>Total unique questions</td><td>{{totalUniqueQuestions}}</td>
                </tr>
                <tr>
                  <td style="text-indent:30px">Multiple choice</td><td>{{uniqueMP}}</td>
                </tr>
                <tr>
                  <td style="text-indent:30px">TBS Journal</td><td>{{uniqueTBSJournal}}</td>
                </tr>
                 <tr>
                  <td style="text-indent:30px">Written Communications</td><td>{{uniqueTBSwc}}</td>
                </tr>
                 <tr>
                  <td style="text-indent:30px">Research</td><td>{{uniqueTBSResearch}}</td>
                </tr>
                </tbody>
            </table>
          
        </div>
    </div>

  <div id='svg-container'></div>
</div>
