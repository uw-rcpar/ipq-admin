var ContentModule = angular.module("ContentModule", ['ngRoute', 'ui.bootstrap']).//, 'ngSanitize']).
config(function($routeProvider, $locationProvider) {
  //$locationProvider.html5Mode(true);
    $routeProvider
      .when('/content/:section/:chapter',
          {
              controller: 'TreeController',
              templateUrl: 'assets/partials/tree-content.php',
              
          })
      .otherwise({ redirectTo: '/content/AUD/1' });

  });

ContentModule.factory("services", function() {
	return {
		content:"/restserver/index.php/api/testcenter/allcontent/format/json"
	};
});

ContentModule.controller("TreeController", function($scope, $routeParams, $rootScope, ContentModel){
  $scope.isLoaded = false;
  ContentModel.section = $routeParams.section;
  ContentModel.chapter = $routeParams.chapter;
  $scope.content = ContentModel.getContent($routeParams.section, $routeParams.chapter).then(function(data) {
    $scope.isLoaded = true;
    return data;
  });
  $rootScope.$on('$routeChangeStart', function(event, nextLoc, currentLoc) {
    console.dir(nextLoc);
  });


});

ContentModule.controller("ChapterTitleController", function($scope, ContentModel) {
  $scope.model = ContentModel;
  $scope.$watch("model.currentChapterTitle", function(n) {
    if(n && n != "") {
     $scope.currentChapterTitle = n;
     $scope.chapterPrefix = String(n).split(":")[0].split("-").join(" ");
     $scope.chapterLabel = n.split(":")[1];
     }
   });
});
ContentModule.controller("NavigationController", function($scope, $routeParams, $q, $location,ContentModel) {
  $scope.model = ContentModel;
  $scope.$watch("model.section", function(n) {
    $scope.currentSection = n;
    console.log(n, $scope.currentSection == $scope.sections[0].section);
  });
  $scope.$watch("model.chapter", function(n) {
    $scope.currentChapterOrder = n;

  });
  $scope.$watch("model.currentChapterTitle", function(n) {
     $scope.currentChapterTitle = n;
     if($scope.chapterList != null && $scope.chapterList instanceof Array) {
        $scope.setChapterIndex();
      }
      //$scope.$digest();
  })
  $scope.sections = [ {section:"AUD", chapters:ContentModel.loadChapters("AUD").then(function(data) {
      angular.forEach(data, function(chapter){
        chapter.section ="AUD";
      });
      $scope.sections[0].chapters = data;
  }) },
      {section:"BEC", chapters:ContentModel.loadChapters("BEC").then(function(data) {
      angular.forEach(data, function(chapter){
        chapter.section ="BEC";
      });
      $scope.sections[1].chapters = data;
  })  } ,
                      {section:"FAR", chapters:ContentModel.loadChapters("FAR").then(function(data) {
     angular.forEach(data, function(chapter){
        chapter.section ="FAR";
      });
      $scope.sections[2].chapters = data;
  }) },
                      {section:"REG", chapters:ContentModel.loadChapters("REG").then(function(data) {
     angular.forEach(data, function(chapter){
        chapter.section ="REG";
      });
      $scope.sections[3].chapters = data;
  }) },
                      ];

  $q.all([$scope.sections[0].chapters,
                  $scope.sections[1].chapters,
                  $scope.sections[2].chapters,
                  $scope.sections[3].chapters 
    ]).then(function(data) {

          $scope.chapterList = $scope.sections[0].chapters
                                .concat($scope.sections[1].chapters)
                                .concat( $scope.sections[2].chapters)
                                .concat($scope.sections[3].chapters);
          console.dir($scope.chapterList);
          if($scope.currentChapterTitle != null) {
            $scope.setChapterIndex();
          }
      });
$scope.setChapterIndex = function() {
  for(var i = 0; i < $scope.chapterList.length; i++) {
    if($scope.chapterList[i].chapter == $scope.currentChapterTitle) {
      $scope.chapterIndex = i;
      // $scope.currentChapter = $scope.chapterList[i];
      break;
    }
  }
}


$scope.prevChapter = function() {
     if($scope.chapterIndex > 0 ) {
      $scope.chapterIndex--;
     } else {
      $scope.chapterIndex = $scope.chapterList.length - 1;
     }
     var ch = $scope.chapterList[$scope.chapterIndex];
     $location.path("/content/" + ch.section + "/" + ch.order);
};
$scope.nextChapter = function() {
     if($scope.chapterIndex < $scope.chapterList.length - 1 ) {
      $scope.chapterIndex++;
     } else {
      $scope.chapterIndex = 0;
     }
     var ch = $scope.chapterList[$scope.chapterIndex];
     $location.path("/content/" + ch.section + "/" + ch.order);
};

})

ContentModule.service("ContentModel", function($http, $q, $routeParams, services) {
	this.content = null;
  this.masterContent = null;
  var self = this;
	this.getContent = function(section, chapter) {
		//if(this.content == null) {
			this.content = this.loadContent(section, chapter);
		//} 
		return this.content;/*.then(function(data) {
      var section = $routeParams.section;
        if(section != "all") {
        for(var i = 0; i < data.children.length;i++) {
          if(data.children[i].name == section) {
            self.content =  data.children[i];
          }
        }
      } else {
        return data;
      }
    });*/
	}
	var self = this;
	this.loadContent = function(section, chapter) {
		var defer = $q.defer();
		$http({method:'GET', url: services.content + "?section=" + section + "&chapter=" + chapter }).then(function(response) {
        self.currentChapterTitle = response.data.name;
				defer.resolve(response.data);
			});
			return defer.promise;
	};

  this.loadChapters = function(section) {
    var defer = $q.defer();
    $http({method:'GET', url: "/restserver/index.php/api/testcenter/chapters/format/json?section=" + section  }).then(function(response) {
        var j = 1;
        for(var i = 0; i < response.data.chapters.length; i++){
          response.data.chapters[i].order = i + 1;
          response.data.chapters[i].prefix = response.data.chapters[i].chapter.split(":")[0].split("-").join(" ");
          response.data.chapters[i].label = response.data.chapters[i].chapter.split(":")[1];
        };
        defer.resolve(response.data.chapters);
    });
    return defer.promise;
  };
});


    

ContentModule.directive("contentTree", function($window, $modal, ContentModel) {
  return {
  templateUrl:"assets/partials/svg-render.php",
  restrict:"A", 
  transclude : true,
  scope:{ngModel:"="},
  replace: true,
  require: 'ngModel',
  controller:function($scope) {
    $scope.toggleChordsModel = 'show';
    $scope.downloadTBSXLS = function() {
	      document.body.innerHTML += "<iframe src='/restserver/index.php/tbscontentxls/spreadsheet/' style='display: none;' ></iframe>";
    }
    $scope.downloadXLS = function() {
	      document.body.innerHTML += "<iframe src='/restserver/index.php/contentxls/spreadsheet/" + ContentModel.section + "/" + ContentModel.chapter + "' style='display: none;' ></iframe>";
    }
  },
  link:function($scope, element, attrs) {
    

    function rightRoundedRect(x, y, width, height, radius) {
      return "M" + x + "," + y
           + "h" + (width - radius)
           + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
           + "v" + (height - 2 * radius)
           + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
           + "h" + (radius - width)
           + "z";
    }

    var elWidth = element.width();
    var scHeight = $window.innerHeight - 100;
    var minDiameter = 760;


    var diameter = Math.floor( (scHeight > elWidth ? elWidth : scHeight) * 0.6);
    if(diameter < minDiameter) {
      diameter = minDiameter;
    };
    $scope.elWidth = diameter + 80
$scope.svg = d3.select("#svg-container").append("svg")
    .attr("width", $scope.elWidth)//element.width())
    .attr("height", $scope.elWidth)//element.width())
    .append("g").attr("id", "tree")
    .attr("transform", "translate(" + $scope.elWidth / 2 + "," + $scope.elWidth / 2 + ")");



  var tree = d3.layout.tree()
      .size([360, diameter / 2])
      .separation(function(a, b) { 
        //return 4;
        return (a.parent == b.parent ? 1 : 2) / a.depth; 
      });
$scope.$on("$destroy", function($e) {
  //d3.select("svg").remove();
    $scope.node.attr("transform", null);
    $scope.node.remove();
    $scope.link.remove();
    $scope.svg.remove();
    nodes = [];
    links = [];
    $scope.tree.links([]);
    $scope.tree.nodes([]);
    delete tree;
});
      /*var win = angular.element($window);
      win.bind("resize", function(e) {
        var scHeight = $window.innerHeight - 45;
        var minDiameter = 750;
        if(minDiameter > scHeight) {
          scHeight = minDiameter;
        }
        d3.select("svg").attr("transform", "translate(" + element.width() / 2 + "," + scHeight/2 + ")");
      });*/
  $scope.badTopics = [];
  $scope.topicMap = [];
  $scope.questionMap = {};
  $scope.questionMapB = {};
$scope.tree = tree;
$scope.totalUniqueQuestions = 0;
$scope.uniqueTBSJournal = 0;
$scope.uniqueTBSwc = 0;
$scope.uniqueTBSResearch = 0;
$scope.uniqueMP = 0;
processNodeTopics = function(d) {
  d.topic_id = d.topic_id.split(",");
  d.topic_id = d.topic_id.sort(function(a, b) {
    if(a > b) {
      return -1;
    } else {
      return 1;
    }
  });
  for(var i = 0; i < d.topic_id.length; i++) {
    if($scope.topicMap.indexOf(d.topic_id[i]) == -1) {
      if($scope.badTopics.indexOf(d.topic_id[i]) == -1) {
        $scope.badTopics.push(d.topic_id[i]);
      }
      //d.y -= 12;
    }
    var qMapkey = "parent_" + d.primary_topic + "_topics_" + d.topic_id.join("_") + "_type_" + d.type.split("-").join("_");
    d.qMapkey = qMapkey;
    if($scope.questionMapB["q" +d.name] == null) {
      $scope.questionMapB["q" +d.name] = [];
      $scope.totalUniqueQuestions ++;
      if(d.type == "multiple-choice") {
        $scope.uniqueMP++;
      } else if(d.type == "tbs-journal") {
        $scope.uniqueTBSJournal ++;
      }else if(d.type == "tbs-wc") {
        $scope.uniqueTBSwc ++;
      }else if(d.type == "tbs-research") {
        $scope.uniqueTBSResearch ++;
      }
    }
    $scope.questionMapB["q" +d.name].push(d);
    d.startAngle = (d.x / 180 * Math.PI) - 0.009;
    d.endAngle = (d.x / 180 * Math.PI) + 0.009;
    d.radius = d.y;
    if($scope.questionMap[qMapkey] == null) {
      var matchKey = "topics_" + d.topic_id.join("_") + "_type_" + d.type.split("-").join("_");
      $scope.questionMap[qMapkey] = { max:null, 
                                      min:null, 
                                      primary_topic:d.primary_topic, 
                                      matches:matchKey, 
                                      radius: (diameter/2) - (9* d.topic_id.length) -3, 
                                      matchKey:matchKey};

    } 
    $scope.questionMap[qMapkey]["q_" + d.name] = d.name;
  }
}
  


var diagonal = d3.svg.diagonal.radial()
    .projection(function(d) { 
     
      if((d.children != null && d.children.length > 0) || d.topic_id == null) {
           return [d.y, d.x / 180 * Math.PI]; 
        } else {
          if(d.topic_id != null && d.topic_id instanceof Array == false) {
            processNodeTopics(d);
            return [d.y - 9 * d.topic_id.length, d.x / 180 * Math.PI];
        } else {
          return [d.y - 9 * d.topic_id.length, d.x / 180 * Math.PI];
        }

      }
    });


$scope.model = $scope.$parent.$eval(attrs.ngModel);


$scope.model.then(function(root) {
  for(var i = 0; i < root.children.length; i++) {
    $scope.topicMap.push(root.children[i].data.id);
  }
  var nodes = tree.nodes(root),
      links = tree.links(nodes);

  var link = $scope.svg.selectAll(".link")
      .data(links)
      .enter().append("path")
      .attr("class", "link")
      .attr("d", diagonal);
  var node = $scope.svg.selectAll(".node")
      .data(nodes)
      .enter().append("g")
      .attr("class", function(d) {
        if((d.children != null && d.children.length > 0) || d.topic_id == null) {
          return "node noquestion";
        } else {
          hasBadtopic = false
          for (var i = 0; i < d.topic_id.length;i++) {
            if($scope.badTopics.indexOf(d.topic_id[i]) != -1) 
              hasBadtopic = true;
              break;
            }
            d.hasBadtopic = hasBadtopic;
          return "node question question-" + d.type + " topics-" + d.topic_id.length + (hasBadtopic ? " badtopic " : " ");
        }
      })
      .attr("transform", function(d) { 
        return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; 
      }).on("click", function(d) {
        console.dir(d)
    });
    $scope.link = link;
  $scope.node = node;
    node.append("text")
      .attr("dy", ".31em")
      .attr("text-anchor", function(d) { 
          if(d.depth == 0) {
            return "middle";
          } else {
            return d.x < 180 ? "start" : "end"; 
          }
        })
      .attr("transform", function(d) { 
        if(d.qMapkey != null) {
          if($scope.questionMap[d.qMapkey].nodes == null) {
            $scope.questionMap[d.qMapkey].nodes = [];
          }
          $scope.questionMap[d.qMapkey].nodes.push(d);
          if($scope.questionMap[d.qMapkey].max != null) {
            if(d.x > $scope.questionMap[d.qMapkey].max) {
              $scope.questionMap[d.qMapkey].max = d.x;//  * (Math.PI / 180);
            }
          } else {
            $scope.questionMap[d.qMapkey].max = d.x;//  * (Math.PI / 180);
          }
          if($scope.questionMap[d.qMapkey].min != null) {
            if(d.x < $scope.questionMap[d.qMapkey].min) {
              $scope.questionMap[d.qMapkey].min = d.x ;//* (Math.PI / 180);
            }
          } else {
            $scope.questionMap[d.qMapkey].min = d.x;// * (Math.PI / 180);
          }

        }

        if(d.depth == 0) {
          return "rotate(-90)translate(-9, -16)";
        } else {
          return d.x < 180 ? "translate(1)" : "rotate(180)translate(-1)"; 
        }
      })
      .text(function(d) { 
        if(d.matchKey) {
          return d.name + " " + d.matchKey;
        } else {
          return d.name;
        }
        
      });
    node.append("circle").attr("r", 4.5).attr("cx", -8);

  
    node.append("path")
        .attr("d", function(d) {
          if(d.topic_id == null) {
            return 'M0,0 L0,4 L4,4 L4,0 Z';//rightRoundedRect(2, 2, 4, 4, 2);
          } else {
            return 'M0,-4 L' + (-9 *d.topic_id.length)+ ',-4 L' +(-9 *d.topic_id.length) + ',4 L0,4 Z';//rightRoundedRect(0, -4, -(9 * d.topic_id.length), 8, 5 );
          }
        })
        .attr("class", "rect");

    var qnodes = $scope.svg.selectAll(".node.question")
      .on("click", function(d) {
          var q = {};
          for(var k in d) { //must copy selectively to avoid circular json refrence
            if(k != "parent") {
              q[k] = d[k];
            }
          }
          var modalInstance = $modal.open({
            templateUrl: 'assets/partials/question-modal.php',
            controller: function ($scope, $modalInstance, question) {
                  $scope.question = question;
                  $scope.ok = function () {
                    window.open(question.editUrl, "_blank");
                    $modalInstance.close();
                  };

                  $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                  };
                }, 
            size: "md",
            resolve: {
              question: function () {
                return q;
              }
            }
          });
      })
    .on("mouseenter", function(d) {
      var arcs = $scope.questionMapB["q" + d.name];
      if(d.chords == null) d.chords = [];
      var prevArc = null;
      if(arcs.length > 1) {
        for(var i = 0 ;i < arcs.length; i++) {
          var arc = arcs[i];
          if(prevArc != null && arc.startAngle != prevArc.startAngle) {
            d.chords.push( $scope.svg.append("path").attr("d", d3.svg.chord().source({
              startAngle:prevArc.startAngle,
              endAngle:prevArc.endAngle,
              radius:prevArc.radius
            }).target({
              startAngle:arc.startAngle,
              endAngle:arc.endAngle,
              radius:arc.radius
            })).attr("class", "chord-" + d.type));
          }
          prevArc = arc;

          if(i == arcs.length - 1) {
            arc = arcs[0];
            d.chords.push( $scope.svg.append("path").attr("d", d3.svg.chord().source({
              startAngle:prevArc.startAngle,
              endAngle:prevArc.endAngle,
              radius:prevArc.radius
            }).target({
              startAngle:arc.startAngle,
              endAngle:arc.endAngle,
              radius:arc.radius
            })).attr("class", "chord-" + d.type));
          }
          
        }
      }
    })
    .on("mouseleave", function(d) {
      if(d.chords != null) {
        for(var i = 0 ;i < d.chords.length; i++) {
          $scope.svg.selectAll(".chord-" + d.type).remove();
        }
        d.chords = null;
      }
    });

    $scope.svg.selectAll(".node.badtopic").append("path").attr("transform", "translate(-4, 0)rotate(270)" ).attr("d", d3.svg.symbol().size(18)
      .type(function(d) {
        return "triangle-up";
      })).attr("stroke", "#ffffff").attr("fill", "#ffffff").attr("opacity",1);
    $scope.renderChords();
});

    $scope.$watch("toggleChordsModel", function(n) {
      if(n == "show") {
        $scope.svg.selectAll(".chord").remove();
        $scope.renderChords();
      } else {
        $scope.svg.selectAll(".chord").remove();
      }

    });

      $scope.toggleChords = function() {

        $scope.showChords = ! $scope.showChords;
        if($scope.showChords) {
          $scope.renderChords();
        } else {
          $scope.hideChords();
        }
     };
      $scope.renderChords = function() {
       
        var sorted = [];
        var matrix = [];
        for(var k in $scope.questionMap) {
          matrix.push([]);
          sorted.push($scope.questionMap[k]);
        }

        sorted.sort(function(a, b) {
          
          if(a.primary_topic < b.primary_topic) {
            return -1;
          } else {
            return 1;
          }
        });

        var groups = {};
        for(var i = 0; i < sorted.length; i++) {
            var arcdata = sorted[i];
              arcdata.min -= 0.5;
              arcdata.max += 0.5;
              arcdata.range = arcdata.max - arcdata.min;
              
            arcdata.arc = d3.svg.arc()
                        .innerRadius(arcdata.radius)
                        .outerRadius(arcdata.radius+4)
                        .startAngle(arcdata.min * (Math.PI / 180))
                        .endAngle(arcdata.max * (Math.PI / 180));
            arcdata.geometry = [$scope.svg.append("path").attr("d", arcdata.arc).attr("fill", "#5bc0de").attr("opacity", 0.7)];
                        //.attr("transform", "translate(" + 10 + "," + 10 + ")");
                        //.attr("transform", function(d) { return "translate(" + arcdata.min + ")"; });

            if(groups[arcdata.matchKey] == null) {
              groups[arcdata.matchKey] = [];
            }
            groups[arcdata.matchKey].push(arcdata);
        }

        colors=["#428bca",
                "#5cb85c",
                "#5bc0de",
                "#f0ad4e",
                "#d9534f",
                  "#008bca",
                "#00b85c",
                "#00c0de",
                "#00ad4e",
                "#00534f"]
                color_i = 0;
        for(var k in groups) {
          var group = groups[k];
          prevArc = null;

          if(group.length > 1) {
            for(var i = 0; i < group.length; i++) {
              arc = group[i];
              // $scope.svg.append("path");
              if(prevArc) {
                
                //y = R sin t    and    x = R cos t 
                //var chord = $scope.svg.append("g");
                var chord = $scope.svg.append("path").attr("d", d3.svg.chord()
                    .source({
                        startAngle:arc.min * (Math.PI / 180),
                        endAngle:arc.max* (Math.PI / 180),
                        radius:arc.radius
                      })
                    .target({
                        startAngle:prevArc.min* (Math.PI / 180),
                        endAngle:prevArc.max* (Math.PI / 180),
                        radius:prevArc.radius 
                    })).attr("stroke",  colors[color_i]).attr("opacity", 0.3).attr("fill", colors[color_i]);
                color_i ++;
                chord.attr("class", "chord");
                chord.on("click", function(d) {
                  console.dir(arc);
                });
              }
              prevArc = arc;
            }
          } else {
            arc = group[0]
            var chord = $scope.svg.append("path").attr("d", d3.svg.chord()
                    .source({
                        startAngle:arc.min * (Math.PI / 180),
                        endAngle:arc.max* (Math.PI / 180),
                        radius:arc.radius 
                      })
                    .target({
                        startAngle:arc.min* (Math.PI / 180),
                        endAngle:arc.max* (Math.PI / 180),
                        radius:arc.radius
                    })).attr("stroke",  colors[color_i]).attr("opacity", 0.3).attr("fill", colors[color_i]);
                color_i++;
                chord.attr("class", "chord");
                chord.on("click", function(d) {
                  console.dir(arc);
                });
          }
        }


      };


    }

  };

});


ContentModule.directive("contentBreadcrumbs", function(ContentModel) {
      return {
        template:'<ol class="breadcrumb">'
                   + '<li><a href="#">{{location.course}}</a></li>'
                    + '<li><a href="#">{{chapterPrefix}} <span class="visible-md visible-lg">{{chapterTitle}}</span></a></li>'
                    + '<li class="active">{{topic.prefix}} <span class="visible-md visible-lg">{{topic.topic}}</span></li>'
                  + '</ol>',
        restrict:"E", 
        transclude : true,
        scope:{ngModel:"="},
        replace: true,
        require: 'ngModel',
        link:function ($scope, element, attrs){
            $scope.location = $scope.$eval(attrs.ngModel);
            $scope.content = ContentModel.getContent().then(function() {
            $scope.chapterPrefix = $scope.content[$scope.location.course].chapters[$scope.location.chapterIndex].chapter.split(": ")[0];
            $scope.chapterTitle = $scope.content[$scope.location.course].chapters[$scope.location.chapterIndex].chapter.split(": ")[0];
            $scope.topic = $scope.content[$scope.location.course].chapters[$scope.location.chapterIndex].topics[$scope.location.topicIndex];
          
            });
        }
    }
});
function polarToCartesion(R, t, projection) {

          return {x:R * Math.cos(t), y:R * Math.sin(t )};
          //return {x:R, y:t};
        }
