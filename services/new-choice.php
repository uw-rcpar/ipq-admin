<?php
header('Content-Type: application/json; charset=utf-8');
echo '{"choice":[{ "choice" : "<p>Releasing financial information to a local bank with the approval of the client\'s mail clerk.</p>",
              "explanation" : "<p><strong>Correct!</strong> A CPA must not reveal confidential information without client permission. A non-management employee, such as a mail clerk, is not authorized to give client permission.</p>",
              "id" : "1426",
              "is_correct" : "1"}
            ]}';