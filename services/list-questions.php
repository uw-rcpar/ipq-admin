<?php
header('Content-Type: application/json; charset=utf-8');
echo '{"questions":[
			     { "choices" : [ { "choice" : "<p>Adverse opinion or a disclaimer of opinion.</p>",
                    "explanation" : "<p>wrong</p>",
                    "id" : "43",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Qualified opinion or an adverse opinion.</p>",
                    "explanation" : "<p>If a client has not appropriately accounted for a material illegal act, the auditor\'s report must reflect this departure from GAAP by issuing a qualified or adverse report. It would not be appropriate to issue a disclaimer because the auditor issues disclaimer of opinions to reflect very material departures from Generally accepted auditing standards, not GAAP. It would also not be appropriate to issue an unmodified opinion because the nature of the illegal act is material.</p>",
                    "id" : "44",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>Disclaimer of opinion or an unmodified opinion with a separate explanatory paragraph.</p>",
                    "explanation" : "<p>just wrong</p>",
                    "id" : "45",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Unmodified opinion with a separate explanatory paragraph or a qualified opinion.</p>",
                    "explanation" : "<p>you are super wrong</p>",
                    "id" : "46",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "26",
              "question" : "<p>An auditor concludes that a client’s illegal act, which has a material effect on the financial statements, has not been properly accounted for or disclosed. Depending on the materiality of the effect on the financial statements, the auditor should express either a(n)</p>",
              "topics" : [ { "chapter_id" : "1",
                    "topic_id" : "5"
                  } ],
               "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            },
            { "choices" : [ { "choice" : "<p>The auditor is unable to determine the amounts associated with an employee fraud scheme.</p>",
                    "explanation" : "<p>its wrong</p>",
                    "id" : "39",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Management does <strong>not </strong>provide reasonable justification for a change in accounting principles.</p>",
                    "explanation" : "<p>The inability of an auditor to determine the amounts associated with employee fraud, the client refusing to permit the auditor to confirm certain accounts receivable or apply alternative procedures, and the unwillingness of the chief executive officer to sign a management representation letter all represent scope limitations. Depending on the materiality, a disclaimer of opinion may be issued. Management\'s not providing reasonable assurance for a change in accounting principles represents a p",
                    "id" : "40",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>The client refuses to permit the auditor to confirm certain accounts receivable or apply alternative procedures to verify their balances.</p>",
                    "explanation" : "<p>super wrong</p>",
                    "id" : "41",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>The chief executive officer is unwilling to sign the management representation letter.</p>",
                    "explanation" : "<p>just being wrong</p>",
                    "id" : "42",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "24",
              "question" : "<p>Under which of the following circumstances would a disclaimer of opinion not be appropriate?</p>",
              "topics" : [ { "chapter_id" : "1",
                    "topic_id" : "1"
                  } ],
                "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            },
            { "choices" : [ { "choice" : "<p>The disclosures provide reasonable assurance that the financial statements are free of material misstatement.</p>",
                    "explanation" : "<p>not correct for a reason</p>",
                    "id" : "23",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>The auditor tested the overall internal control.</p>",
                    "explanation" : "<p>still wrong</p>",
                    "id" : "24",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>An audit includes evaluating the reasonableness of significant estimates made by management.</p>",
                    "explanation" : "<p>In the Auditors responsibility paragraph, it mentions that and auditor evaluates the appropriateness of accounting policies used and the reasonableness of significant accounting estimates made by management. The requirement is to identify the statement that is included in the auditor’s standard report. Answer (c) is correct because the auditor’s standard report states that an audit includes assessing significant estimates made by management; the other replies provide information not directly ",
                    "id" : "25",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>The financial statements are consistent with those of the prior period.</p>",
                    "explanation" : "<p>its wrong</p>",
                    "id" : "26",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "16",
              "question" : "<p>Whichof the following statements is a basic element of the auditor’s standard report?</p>",
              "topics" : [ { "chapter_id" : "1",
                    "topic_id" : "5"
                  } ],
                  "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            },
            { "choices" : [ { "choice" : "<p>Report on the Financial Statements (Intro)</p>",
                    "explanation" : "<p>wrong</p>",
                    "id" : "31",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Management’s Responsibility for the Financial Statements</p>",
                    "explanation" : "<p>still wrong</p>",
                    "id" : "32",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Auditor’s Responsibility</p>",
                    "explanation" : "<p>In the Auditor’s Responsibility paragraph, it is specifically mentioned that the auditor considers internal control relevant to the entity’s preparation and fair presentation of the financial statements in order to design audit procedures that are appropriate in the circumstances, but not for the purpose of expressing an opinion, and accordingly, we express no such opinion.</p>",
                    "id" : "33",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>Opinion</p>",
                    "explanation" : "<p>nope</p>",
                    "id" : "34",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "20",
              "question" : "<p>In which paragraph of a standard unmodified report is a disclaimer on internal control mentioned?</p>",
              "topics" : [ { "chapter_id" : "3",
                    "topic_id" : "14"
                  } ],
                  "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            },
            { "choices" : [ { "choice" : "<p>Intro (report on F/S)</p>",
                    "explanation" : "<p>wrong wrong wrong</p>",
                    "id" : "27",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Management’s Responsibility</p>",
                    "explanation" : "<p>still wrong</p>",
                    "id" : "28",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>Auditor’s Responsibility</p>",
                    "explanation" : "<p>The basic unmodified audit report for a nonissuer consists of 4 paragraphs. The Introductory or Report on the financial statements paragraph, management’s responsibility paragraph, auditor’s responsibility paragraph and the opinion paragraph. Reference to the auditing standards used is made in the Auditors responsibility paragraph and reference to accordance with the principles is made in the opinion paragraph.</p>",
                    "id" : "29",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>Intro</p>",
                    "explanation" : "<p>Nope</p>",
                    "id" : "30",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "18",
              "question" : "<p>Which paragraphs of an auditor’s standard report on financial statements of a nonissuer should refer to auditing standards generally accepted in the U.S. and Principles generally accepted in the U.S.?</p>",
              "topics" : [ { "chapter_id" : "2",
                    "topic_id" : "12"
                  } ],
                  "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            },
            { "choices" : [ { "choice" : "<p>A change in the method of accounting for inventories.</p>",
                    "explanation" : "<p>wrong</p>",
                    "id" : "35",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>A change from an accounting principle that is <strong>not </strong>generally accepted to one that is generally accepted.</p>",
                    "explanation" : "<p>wrongosis</p>",
                    "id" : "36",
                    "is_correct" : "0"
                  },
                  { "choice" : "<p>A change in the useful life used to calculate the provision for depreciation expense.</p>",
                    "explanation" : "<p>Whenever there is a change in accounting principles, including a change in the method of accounting for inventories, a change from an unacceptable principle to an acceptable one, or a change that management cannot reasonably justify, the auditor\'s report will include some reference to consistency. A change in the useful life of an asset is a change in accounting estimate that does not affect consistency.</p>",
                    "id" : "37",
                    "is_correct" : "1"
                  },
                  { "choice" : "<p>Management’s lack of reasonable justification for a change in accounting principle.</p>",
                    "explanation" : "<p>just plain wrong</p>",
                    "id" : "38",
                    "is_correct" : "0"
                  }
                ],
              "difficulty" : "0",
              "id" : "22",
              "question" : "<p>For which of the following events would an auditor issue a report that omits any reference to consistency?</p>",
              "topics" : [ { "chapter_id" : "1",
                    "topic_id" : "3"
                  } ],
               "createdOn":"8999-12-31 23:59:59",
               "createdBy":"Steve Taylor",
               "updatedOn":"9999-12-31 23:59:59",
               "updatedBy":"Sam Edwards",
               "type":"multiple-choice"
            }
		]}';