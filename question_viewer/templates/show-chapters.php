<div ng-show="question.mcq.questions==null">
  		<ul class="nav nav-pills nav-stacked" ng-repeat="chap in topics.chapters">
	 		<li><a  ng-model="collapsed" ng-click="collapsed=!collapsed">{{chap.chapter}}</a></li>
	 			<ul ng-show="collapsed">
	 				<li ng-repeat="tops in chap.topics">	 				
	 					<a data="{{tops.id}}" ng-click="getQuestions(tops.id)">{{tops.topic}}</a>	 				
	 				</li>
	 			</ul>
		</ul>
</div>	


<div ng-show="question.mcq.questions.length > 0">
	
	<h2>Questions</h2>

<div>
  		
  		<ul class="nav nav-pills nav-stacked">
	 		<li ng-repeat="q in question.mcq.questions"><a ng-click="showQuestionDetail(q)" ng-bind-html-unsafe="q.question"></a>	</li>	 			
		</ul>
</div>
	
</div>