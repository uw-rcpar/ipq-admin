<?php //no cookie, no showie. Cookie set on resterver admin_login_post()
if (!$_COOKIE['isadmin']) {
	//back to login
	header("Location: /testmodule-admin/index.php");
	die();
};
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>IPQ Question Viewer</title>
		<!-- <script src="js/lib/angular-resource.min.js"></script> -->
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="//angular-ui.github.io/bootstrap/"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script src="js/lib/angular.min.js"></script>
		<script src="js/lib/angular-sanitize.js"></script>
		<script src="js/app.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="css/styles.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script type='text/javascript'>
			//<![CDATA[
			$(window).load(function() {
				$(function() {
					var window_height = $(window).height(), content_height = window_height - 200;

					$('.scroller').height(content_height);
				});

				$(window).resize(function() {
					var window_height = $(window).height(), content_height = window_height - 200;
					$('.scroller').height(content_height);
				});

			});
			//]]>

		</script>
	</head>

	<body ng-app='qViewerApp' ng-controller="ShowChaptersController">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">IPQ Viewer</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li  ng-class="{active: viewingType == 'mcq'}">
							<a ng-click="viewType('mcq')">MCQ</a>
						</li>
						<li  ng-class="{active: viewingType == 'tbs'}">
							<a ng-click="viewType('tbs')">TBS</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Section <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li>
									<a ng-click="getChapters('far')">FAR</a>
								</li>
								<li>
									<a ng-click="getChapters('aud')">AUD</a>
								</li>
								<li>
									<a ng-click="getChapters('reg')">REG</a>
								</li>
								<li>
									<a ng-click="getChapters('bec')">BEC</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" ng-model="myForm.search">
						</div>
						<select ng-model="searchType" ng-init="searchType = 'IDs'">
							<option selected>IDs</option>
							<option>Keywords</option>
						</select>
						<button type="submit" class="btn btn-default" ng-click="myForm.submitTheForm()">
							Submit
						</button>
						<span ng-show="formLoading == true"> Loading... </span>
					</form>

				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div id="wrapper">
			<div class="row">
				<div class="col-md-3 rtborder">
					<!-- left column -->

					<!-- chapters and topics -->				
					<div ng-show="topics != null" ng-hide="showQuestions()">
						<h2 style="text-transform:uppercase;">{{section}}</h2>
						<ul class="nav nav-pills nav-stacked" ng-repeat="chap in topics.chapters">
							<li>
								<a  ng-model="collapsed" ng-click="collapsed=!collapsed">{{chap.chapter}}</a>
							</li>
							<ul ng-show="collapsed">
								<li ng-repeat="tops in chap.topics">
									<a data="{{tops.id}}" ng-click="getQuestions(tops.id,tops.topic)">{{tops.topic}}</a>
								</li>
							</ul>
						</ul>
					</div>
					<!-- /end chapters and topics -->

					<!-- Question snippets -->
					<div ng-show="showQuestions()">
						<form role="form" name="qids" id="qids" action="worddoc.php" method="post">
						<div ng-show="showSearchMessage==false">							
						<h2 ><span style="text-transform:uppercase;">{{section}}</span> Questions</h2>						
						<button type="submit" class="btn btn-primary btn-sm">Export Questions</button>
						<input type="hidden" name="type" value="{{viewingType}}" />
						<input type="hidden" name="section" value="{{section}}" />
						<input type="hidden" name="topic" value="{{current_topic}}" />
						</div>
						<h2 ng-show="showSearchMessage==true">Search Results</h2>
						<p style="padding-top: 10px;">
							<a ng-click="goBack()"><span class="glyphicon glyphicon-chevron-left"></span>{{current_topic}}</a>
						</p>
						<div class="scroller">
							
								<div class="form-group">
									<ul class="nav nav-pills nav-stacked">
										<li ng-repeat="q in showQuestions()" ng-hide="q.exclude">
											<div class="checkbox">
												<input type="checkbox" name="qid[]" value="{{q.id}}" checked="checked"> 
												<label>
												<a ng-click="showQuestionDetail(q.id)" ng-bind-html-unsafe="q.question"></a>
												</label>
											</div>
										</li>
									</ul>
								</div>
							
						</div>
						</form>
					</div><!-- /end Question snippets -->

				</div><!-- /left column -->
				<div class="col-md-9">
					<!-- right column -->

					<!-- Question detail here -->
					<div ng-show="question_detail != null" >
						<h2>Question Detail</h2>
						<p>
							{{message}}
						</p>
						<p><button ng-click="adminEdit()" class="btn btn-danger"><i class="icon-white icon-pencil"></i> Edit this question in the administrator</button></p>

						<div ng-show="viewingType == 'mcq'">
							<p ng-bind-html-unsafe="question_detail.questions[0].question"></p>
							<ol type="A">
								<li ng-repeat="answers in question_detail.questions[0].choices" ng-bind-html-unsafe="answers.choice"></li>
							</ol>
							<p>
								Correct Answer: <strong>{{correct}}</strong>
							</p>
							<h3 class="btmborder"> Explanations: </h3>
							<ol type="A">
								<li ng-repeat="mychoice in question_detail.questions[0].choices">
									<div ng-bind-html-unsafe="mychoice.choice"></div>
									<div ng-bind-html-unsafe="mychoice.explanation"></div>
								</li>
							</ol>
						</div>
						<div ng-show="viewingType == 'tbs'">
							<p ng-bind-html-unsafe="question_detail.question.widgets[0].model.content"></p>
							<div ng-show="subType == 'tbs-journal'">
								<table border="2" class="col-md-12">
									<tr ng-repeat="row in lists">
										<td class="col-md-1">{{ row.model.name }}</td>
										<td><span ng-repeat="answer in row.model.content">({{answer.letter}}) {{answer.answer}}<br /></span></td>
									</tr>
								</table>
								<table border="2" class="col-md-12">
									<tr ng-repeat="row in question_detail.question.widgets[1].model.content">
										<td ng-repeat="col in row" ng-bind-html-unsafe="col"></td>
									</tr>
								</table>
							</div>
							<h3 class="btmborder"> Explanations: </h3>
							<div class="col-md-12" ng-bind-html-unsafe="question_detail.question.solution"></div>
						</div>

					</div><!-- /end Question detail here -->

				</div><!-- /right -->
			</div><!-- /row -->
		</div>

	</body>

</html>
