<?php

//var_dump($_POST['qid']);

$qids=$_POST['qid'];
$section=$_POST['section'];
$type=$_POST['type'];
$topic=$_POST['topic'];



if ('localhost' == $_SERVER['HTTP_HOST']) {
	
	//this will never be true on the production
	$con=mysqli_connect("localhost","root","","testcenter");
	
} else  if('testingsimulator.com' == $_SERVER['HTTP_HOST']) {
	$con=mysqli_connect("localhost","root","root","testcenter");

} else {
	//this is the production connection
	$con=mysqli_connect("localhost","root","bitnami","testcenter");
	
}

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit;
  
}
  
$qid_list=implode(',', $qids);
if ($type == 'mcq') {
	$mcqs = mysqli_query($con,"SELECT * FROM multiple_choice_questions WHERE id IN ($qid_list)");
}
else if ($type == 'tbs') {
	$tbs_list= mysqli_query($con,"SELECT * FROM task_based_questions WHERE id IN ($qid_list)");
}




header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=document_name.doc");

echo "<html>";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
//echo "<meta content=\"utf-8\" http-equiv=\"encoding\">";
{ ?>
<style>
  p, li, ol {font-size: 14px; font-weight: normal;}
  
  
  .btmborder{
	border-bottom: solid 1px #CCCCCC;
}

</style>	
	
<?php } 
echo "<body>";
// output the questoins and answers
echo "<h1>".strtoupper($section)."</h1>";
echo "<h2>".$topic."<h2>";
if ($tbs_list) {
	while($tbs = mysqli_fetch_array($tbs_list)) {
  		$question_id=$tbs['id'];
		//get rid of unwanted tags
		$question_detail = json_decode(urldecode(json_decode($tbs['question_json'], true)['question']), true);
		$str_question= $question_detail['question'];
		if ($str_question['question']) {
			$str_question = $str_question['question'];
		}
		else if ($str_question['widgets']) {
			$str_question = $str_question['widgets'][0]['model']['content'];
		}
		$str_question=html_entity_decode($str_question, ENT_HTML5, "UTF-8");
		echo "<h3>Question ID: $question_id</h3>";

		if ($question_detail['question']['widgets'][1]['type'] == 'researchInput') {
			echo "<p>" . htmlentities(strip_tags($question_detail['question']['widgets'][0]['model']['content'])) . "</p>";
		}
		else {	
			echo "<p>" . $str_question . "</p>";
		}

		if (isset($question_detail['question']['answerkeys']) && $question_detail['question']['widgets'][1]['type'] !== 'researchInput') {
			echo "<table border=\"2\">";
			foreach($question_detail['question']['answerkeys'] as $row) {
				echo "<tr>";
					echo "<td>" . $row['model']['name'] . "</td>";
					echo "<td>";
						foreach($row['model']['content'] as $answer) {
							echo "<span>(" . $answer['letter'] . ") " . $answer['answer'] . "</span><br />";
						}
					echo "</td>";
				echo "</tr>";
			}
			echo "</table><br /><br />";
		}
		if (isset($question_detail['question']['widgets'])) {
			if ($question_detail['question']['widgets'][1]['type'] !== 'researchInput') {
				echo "<table border='2'>";
				foreach($question_detail['question']['widgets'][1]['model']['content'] as $row) {
					echo "<tr>";
						foreach($row as $col) {
							echo "<td>" . $col . "</td>";
						}
					echo "</tr>";
				}
				echo "</table><br /><br />";
			}
		}
		echo '<h3 class="btmborder">Explanations:</h3><br /><br />';
		if ($question_detail['question']['widgets'][1]['type'] == 'researchInput') {
		echo $question_detail['question']['widgets'][1]['model']['content']['type']['type'] . " ";
		echo $question_detail['question']['widgets'][1]['model']['content']['mask'] . "<br /><br />";
		echo $question_detail['question']['widgets'][1]['model']['content']['type']['url'] . "<br /><br />";
		}
		else {
		echo $question_detail['question']['solution'];
		}
		
  };
}
if ($mcqs) {
	while($mcq = mysqli_fetch_array($mcqs)) {
  		$question_id=$mcq['id'];
		//get rid of unwanted tags
		$str_question=strip_tags($mcq['question'], '<table><tr><td>');
		//remove weird entities
		$str_question=html_entity_decode($str_question, ENT_HTML5, "UTF-8");
		$exps=array();
		$answers = mysqli_query($con,"SELECT * FROM multiple_choice_answers WHERE question_id = $question_id");
		echo "<h3>Question ID: $question_id</h3>";
		echo "<p>".$str_question."</p>";
		echo '<ol type="A">';
		//answers
		while($answer = mysqli_fetch_array($answers)){
			echo "<li>".$answer['choice']."</li>";
			$exps[]=$answer['explanation'];
		}
		echo "</ol>";
		//echo '<h3 class="btmborder">Explanations:</h3>';
		echo '<ol type="A">';
		foreach ($exps as $exp) {
			//$exp=strip_tags($exp, '<table><tr><td><ol><li>');
			$exp=html_entity_decode($exp, ENT_HTML5, "UTF-8");
			echo "<li>".$exp."</li>";
		}
		echo "</ol>";
  };
}
echo "</body>";
echo "</html>";
?>
