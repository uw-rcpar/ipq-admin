var qViewerApp = angular.module('qViewerApp',[]);

qViewerApp.controller('ShowChaptersController', function($scope, $http, QuestionService) {
     
   $scope.myForm = {};
   $scope.viewingType = 'mcq';
   $scope.formLoading=false;
   $scope.showSearchMessage=false; 
   
   $scope.viewType = function(type) {
		$scope.viewingType = type;
		$scope.question_detail = null;
	}

   $scope.getChapters=function(section){
   $http.get('/restserver/index.php/api/testcenter/chapters/format/json/?section='+section).
        success(function(data) {
            $scope.topics = data;
	    $scope.question.mcq=null;
            $scope.question.tbs=null;
            $scope.section=section;
            $scope.showSearchMessage=false;
                        
        });
     };   

    $scope.showQuestions = function() {
		if ($scope.question[$scope.viewingType]) {
//			console.log($scope.question[$scope.viewingType]);
//console.log($scope.viewingType);
			return $scope.question[$scope.viewingType]['questions'];
		}
		else {
			return false;
		}
	}

   
    $scope.question={};

    $scope.getQuestions=function(topicid,topic){
    	var request = {topicids:{"multipleChoice":[topicid],"tbs":[topicid]}}; 
    	$http.post( "/restserver/index.php/api/testcenter/questions_by_topic/format/json", request )
				.success(function(data) {
					$scope.question.mcq=data.mcq;	
					$scope.question.tbs=data.tbs;	
					$scope.current_topic=topic;
				});
		
    	
	};
	
	//returns the Chapters menu in ng-show
	$scope.goBack=function(){
    	
    	
    	$scope.question.mcq=null;
		
    	
	};
	//search form
	$scope.myForm.submitTheForm = function(item, event) {
       //console.log("--> Submitting form");
       $scope.formLoading=true;
      
	if ($scope.searchType == 'IDs') { 
	       var responsePromise = $http.get('/restserver/index.php/api/testcenter/search_questions_by_id/format/json/?search='+$scope.myForm.search);
	       responsePromise.success(function(dataFromServer, status, headers, config) {
		 // console.log(dataFromServer);
		  $scope.question.mcq={'questions':dataFromServer.mcq};
		  $scope.question.tbs ={'questions':dataFromServer.tbs};
		  angular.forEach($scope.question.tbs.questions, function(q, index) {
			var questionJSON = JSON.parse(unescape(JSON.parse(q.question_json).question)).question;
			if (questionJSON.question) {
				$scope.question.tbs.questions[index].question = questionJSON.question;
			}
			else {
				$scope.question.tbs.questions[index].question = questionJSON.widgets[0].model.content;
			}
		  });
		  $scope.current_topic="";
		  $scope.formLoading=false;
		  $scope.showSearchMessage=true;
	       });
		responsePromise.error(function(data, status, headers, config) {
		  alert("No search results");
		   $scope.formLoading=false;
	       });
	}
	else if ($scope.searchType == 'Keywords') {
	       var responsePromise = $http.get('/restserver/index.php/api/testcenter/search_all_questions/format/json/?search='+$scope.myForm.search);
	       responsePromise.success(function(dataFromServer, status, headers, config) {
		  //console.log(dataFromServer);
		  $scope.question.mcq={'questions':dataFromServer.results.mcq};

		  var mcqIds = [];
	          angular.forEach($scope.question.mcq.questions, function(q, index) {
			q.id = q.question_id;
			if ($.inArray(q.id, mcqIds) > -1) {
				q.exclude = true;
			}
			else {
				mcqIds.push(q.id);
			}
		  });
		  $scope.question.tbs ={'questions':dataFromServer.results.research.concat(dataFromServer.results.tbs).concat(dataFromServer.results.wc)};
		  angular.forEach($scope.question.tbs.questions, function(q, index) {
			var questionJSON = JSON.parse(unescape(JSON.parse(q.question_json).question)).question;
			if (questionJSON.question) {
				$scope.question.tbs.questions[index].question = questionJSON.question;
			}
			else {
				$scope.question.tbs.questions[index].question = questionJSON.widgets[0].model.content;
			}
		  });
		  $scope.current_topic="";
		  $scope.formLoading=false;
		  $scope.showSearchMessage=true;
	       });
		responsePromise.error(function(data, status, headers, config) {
		  alert("No search results");
		   $scope.formLoading=false;
	       });
	}
       
    };
	
	$scope.adminEdit = function(questionid) {
		if ($scope.viewingType === 'mcq') {
			window.open("/testmodule-admin/search-edit.php#/question/multiple-choice/edit/" + $scope.questionid, "_blank");
		}
		else if ($scope.viewingType === 'tbs') {
			window.open("/testmodule-admin/build-widget-question.php#/edit/" + $scope.questionid, "_blank");
		}
	};
	
	$scope.showQuestionDetail=function(questionid){
    	
    	$scope.message = 'Question id: '+questionid;
    	$scope.questionid=questionid;
	if ($scope.viewingType == 'mcq') {
    	$http.get('/restserver/index.php/api/testcenter/multiple_choice/format/json/?id='+questionid).
        success(function(data) {
            $scope.question_detail = data;
            var json = data;
            var answers = json.questions[0].choices;
            var correct = "";
            $scope.correct = correct;
            
            for(var i = 0; i < answers.length; i++) {
		    var obj = answers[i];
		    
		    //get the correct letter answer				
			if (obj.is_correct==1){
				switch(i)
					{
					case 0:
					  correct="A";	
					  break;			  
					case 1:
					  correct="B";
					  break;
					case 2:
					  correct="C";
					  break;
					case 3:
					  correct="D";
					  break;
					case 4:
					  correct="E";
						break;			
					}
					 $scope.correct = correct;
					 $scope.explanation=obj.explanation;
				//console.log(correct);
			}		
		    
			}
            
        });
	}
	else {
    		$http.get('/restserver/index.php/api/testcenter/tbs_questions/format/json/?id='+questionid).
		success(function(data) {
			$scope.question_detail = JSON.parse(unescape(decodeURI(JSON.parse(data[0].question_json).question)));
console.log($scope.question_detail);
			$scope.lists = $scope.question_detail.question.answerkeys;
			if ($scope.question_detail.question.widgets) {
				if ($scope.question_detail.question.widgets[1].type == "researchInput") {
					$scope.subType = "tbs-research";
				}
				else {
					$scope.subType = "tbs-journal";
				}
			}
			else {
				$scope.subType = "tbs-wc";
			}
		});
	}
    	
	};
	
	
    
     $scope.$watch('question.mcq', function(mcq) {
     	
     	if (mcq == null) return;
     	
     	//$scope.question=mcq;
     	//console.log($scope.question);
     	
     	 });
});
 
qViewerApp.service('QuestionService', function ($http){
	
	var self=this;
	this.getMcqQuestions=function(request){
	
	
	
	return $http.post( "/restserver/index.php/api/testcenter/questions_by_topic/format/json", request )
				.then(function(data) {
					//
					return data.data;	
									
				});
	
	};
});

qViewerApp.controller('ShowQuesionsController', function($scope, $http, QuestionService) {
 
    
    
    
	
    
		
});
