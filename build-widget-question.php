<?php
//no cookie, no showie. Cookie set on authorize.php
if (!$_COOKIE['isadmin']){
	//back to login
	header("Location: index.php");
	die();
};
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="css/cobalt/jquery-wijmo.css" rel="stylesheet" title="cobalt-jqueryui" type="text/css" />
	<link href="css/styles.css" rel="stylesheet" media="screen">
  <link href="css/wijmo/jquery.wijmo-pro.all.3.20132.9.min.css" rel="stylesheet" type="text/css" />
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
  <!--jQuery References-->    

<!-- jQuery/jQueryUI-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/string.min.js"></script>

    <!-- Angular (after jQuery)
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.js" type="text/javascript" ></script>
 -->
    <script src="js/angular.min.js"></script>
    <!-- Wijmo -->
    
<!--
    <link href="css/wijmo/jquery.wijmo-open.3.20132.9.css" rel="stylesheet" type="text/css" />
    <link href="css/wijmo/jquery.wijmo-pro.3.20132.9.min.css" rel="stylesheet" type="text/css" /> -->
    

    <script src="js/wijmo/jquery.wijmo-open.all.3.20132.9.min.js" type="text/javascript"></script>
    <script src="js/wijmo/jquery.wijmo-pro.all.custom.3.20132.9.js" type="text/javascript"></script>
    <script src="js/wijmo/angular.wijmo.3.20132.9.min.js" type="text/javascript"></script>
   
    <!--
    <script src="js/wijmo/angular.wijmo.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/interop/angular.wijmo.3.20132.8.min.js" type="text/javascript"></script>
-->

 <script src="js/angular-sanitize.js"></script>


<script src="js/wijmo/jquery.wijmo.wijspread.all.1.20132.5.js" type="text/javascript"></script>
<link href="css/wijmo/jquery.wijmo.wijspread.1.20132.5.css" rel="stylesheet" type="text/css" />

  <script src="js/tinymce4/tinymce.min.js"></script>
  <script src="js/masonry.js"></script>
  
  <!--
  <script src="js/tinymce-angular.js"></script>
  -->
  <script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
  <script src="js/NavModule.js"></script>
  <script src="js/TopicsModule.js"></script>
  <script src="js/build-widget-module.js"></script>
  <script src="js/build-widget-services.js"></script>
  <script src="js/build-widget-controllers.js"></script>
  <script src="js/build-widget-newgrid-controller.js"></script>
  <script src="js/build-widget-newspreadjs.js"></script> 
  
  <script>
    
    
    angular.element(document).ready(function() {
      angular.bootstrap($("#navModule"), ["NavModule"]);
      angular.bootstrap($("#buildWidgetQuestionApp"), ["BuildWidgetQuestionModule"]);
    });
  </script>

   <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php include 'navbar.php'; ?>
<div id="buildWidgetQuestionApp">

<!--
<div style="width:1px;height:1px;clear:both;">
<div class="canvasLinksContainer">
  <canvas id="answermapCanvas" width="99" height="1800"></canvas>
</div>
</div>
-->
    <div class="container">

      <div class="row" ng-controller="ToolBarController">
        
        <p class="lead"><span ng-show="question.model.id == null">Question Widget Builder</span>
        <span ng-show="question.model.id != null"><a class="tbs-delete-question" ng-click="question.deleteQuestion()" tooltip="Delete this question"><i class="fa fa-trash-o "></i></a> Editing question #{{question.model.id}}</span>
        </p>



        <span class="" ng-show="question.model.document.id == '0'"><i class="fa fa-warning"></i> This question is not linked to a document. <a  ng-click="searchForDocMatches()" >Search for possible matches in documents</a></span>
        <span class="alert alert-error" ng-show="question.model.document.error != null"><i class="fa fa-warning"></i> {{question.model.document.error}}. <a href="/testmodule-admin/checkout-question-documents-widget-TBS.php" class="alert-link">You should find a source document to check out first.</a></span>
        <div class="btn-toolbar" ng-show="question.model.document != null && question.model.document.id != '0' && question.model.document.error == null">
        <a class="btn btn-link" href="{{question.model.document.url}}"><i class="fa fa-file-text"></i> {{question.model.document.title}}</a>
        </div>

	<div class="alert alert-error" ng-show="question.dupes == true">Your question is too similar to already-existing questions. Please change the question text.</div>
        <div class="alert alert-success" ng-show="question.isSaved == true && question.isDirty == false">Your question has been saved. 
		<span ng-hide="isEditingQuestion()"><a href="/testmodule-admin/build-widget-question.php#/edit/{{question.model.question.id}}" target="_blank">Edit Question</a></span>
	</div>

        <div class="btn-toolbar " ng-show="question.model.parseError != true" style="margin-left:0">
         <div class="btn-group">
        <button class="btn btn-inverse" ng-click="newRichTextWidget()"><i class="fa fa-align-left"></i> Text block</button>
        <button class="btn btn-inverse" ng-click="newGridWidget()"><i class="fa fa-th-large"></i> Grid</button>
        <button class="btn btn-inverse" ng-click="newSpreadsheetWidget()"><i class="fa fa-th"></i> Spreadsheet</button>
        <button class="btn btn-inverse" ng-click="newResearchLink()"><i class="fa fa-book" style="color:#fff"></i> Research Input</button>
          

        <button class="btn btn-inverse" ng-click="newAnswerKey()"><i class="fa fa-list"></i> Answer Key</button>
        <button class="btn-inverse btn" ng-click="showSolutionScreen()"><i class="fa fa-check-circle"></i> Solution</button>
        </div>
        <button class="btn-inverse btn" ng-click="launchInQuiz()" ng-disabled="question.model.id == null"><i class="fa fa-external-link-square"></i> View question in quiz</button>  
        
        </div>

        <div ng-show="question.model.parseError == true" class="well">
          <p class="lead"><i class="fa fa-warning pull-left fa-4x"></i>There was an error parsing the JSON for this question. <span ng-show="model.document.id !='0'">Try downloading the document that this question originated from and recreating it.</span><span ng-show="model.document.id=='0'">There is no document linked to this question, so you should delete it to keep it from crashing user quiz sessions.</span></p>
            <div class="clearfix">
            <div class="btn-group pull-right">

            <button class="btn btn-danger" ng-click="question.deleteQuestion()"><i class="fa fa-trash-o"></i> Delete question {{model.id}}</button>
            <button class="btn btn-inverse" ng-click="resetAndRebuild()"><i class="fa fa-cloud-download"></i> Download document and rebuild</button>
            </div>
            </div>
        </div>
      </div>

      <div class="row" ng-controller="QuestionRenderer" id="question-region">
      <div class="span9">
        
        <div ng-repeat="template in model.question.widgets" class="row">
          <div ng-include="template.type" class="templateContainer">

          </div>  
        </div>
      
        <div class="well span7" ng-show="model.question.solution != null || ''" style="margin-top:30px">
          <div class='edit-toolbar solution'>
              <div class='btn-group btn-group-vertical'>
                <button class='btn btn-link' ng-click='deleteSolution()'><i class='fa fa-times'></i></button>
                <button class='btn btn-link' ng-click='editSolution(model.question.solution)'><i class='fa fa-edit'></i></button>
              </div>
          </div>

        <h1 class="lead muted">Solution</h1>
        <span ng-bind-html-unsafe="model.question.solution"></span>
        </div>

      </div>
      <div class="span2 offset1">

        <div ng-repeat="key in model.question.answerkeys" class="answerkey-container">
          <p><button class="btn btn-link btn-mini" ng-click="deleteAnswerkey(key)"><i class="fa fa-times"></i></button> {{key.model.name}}</p>
          <answerkey-gridblock-tabpane ng-model="key" parent-id="question_region"></answerkey-gridblock-tabpane>
         
        </div>

      </div>


      </div>

    </div>
<div class="navbar navbar-inverse navbar-fixed-bottom roger" ng-controller="SaveController">
      <div class="navbar-inner">
        <div class="container">
        <div class="row">

        <div class="span5 pull-left">
          <div class="btn-group dropup">
                  <a class="btn btn-inverse" disabled="disabled">{{topics.course}}</a>
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown">Topics</a>
                  <ul class="dropdown-menu btn-inverse">
                    <li ng-repeat="topic in topics.questionTopics">
                      <a><i class="fa fa-times" ng-click="topics.removeTopic(topic)" tooltip="Remove this topic"></i>
                      {{topic.prefix}} : <strong>{{topic.topic}}</strong></a>
                    </li>
                    <li><a ng-click="topics.addTopic()" class="add-new"><i class="fa fa-plus"></i> Add topic</a></li>
                    
                   
                  </ul>
                </div>
        </div>

        <!--
              <div class="span5 pull-left">
                <div class="btn-group dropup">
                  <a class="btn btn-inverse">{{question.currentUserList}}</a>
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a ng-click="listByUser('administrator')" >administrator</a></li>
                    <li><a ng-click="listByUser('dlee')">dlee</a></li>
                    <li><a ng-click="listByUser('sarahc')">sarahc</a></li>
                   
                  </ul>
                </div>
          
                <div class="btn-group dropup" ng-show="question.feedMode != 'default'">
                <button class="btn btn-inverse" ng-click="question.previousQuestionInUserList()" tooltip="previouse question" tooltip-trigger="mouseenter"><i class="icon-white icon-arrow-left"> </i></button>
                
                <button class="btn btn-inverse dropdown-toggle" data-toggle="dropdown"  tooltip="jump to question" tooltip-trigger="mouseenter">{{question.listIndex}} of {{question.userlist.length}} <i class="icon-white icon-share-alt" > </i></button>
                  <ul class="dropdown-menu">
                    <li><form ng-submit="question.jumpToPosition()"><input type="text" placeholder="list position" ng-model="question.listIndex" /></form></li>
                   
                  </ul>
                <button class="btn btn-inverse" ng-click="question.nextQuestionInUserList()" tooltip="next question" tooltip-trigger="mouseenter"><i class="icon-white icon-arrow-right"> </i></button>

                </div>

              </div>
          -->

                <div class="span2 pull-right">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button class="NextButton pull-right btn btn-success" ng-click="saveQuestion()"><span ng-bind-html-unsafe="msg.cmdMessage"></span>
                      </button>
                      </div>
			<div class="btn-group">
				<button class="NextButton btn" ng-hide="question.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Disable</button>  
				<button class="NextButton btn" ng-show="question.deprecated" ng-click="deprecateQuestion()"><i class="fa fa-external-link-square"></i>Enable</button>  
			</div>
                    </div>
                </div>
              </div>
              
              </div>
            </div>
        </div>
    </div>
  </div>
</div>
</div><!-- ./ #buildWidgetQuestionApp -->



</body>
</html>
