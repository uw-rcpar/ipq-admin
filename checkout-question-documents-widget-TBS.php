<!DOCTYPE html>
<html lang="en">
<head>

<link href="bootstrap/css/bootstrap-flatly.min.css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
<!--
<link href="bootstrap/css/bootstrap-spacelab.min.css" rel="stylesheet" media="screen">-->
	<link href="css/styles.css" rel="stylesheet" media="screen">
    <style type="text/css">
    .table th, .table td {
        font-size:11px;
    }
    </style>
  <!--jQuery References-->    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    
    <script src="js/tinymce4/tinymce.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-sanitize.js"></script>
    <script src="js/bootstrap-gh-pages/ui-bootstrap-tpls-0.4.0.js"></script>
    
    <script src="js/jquery.cookie.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/NavModule.js"></script>
    <script src="js/document-checkout-module.js"></script>
    <?php 
    if($_SERVER['SERVER_NAME'] != 'testcenter.rogercpareview.com') { ?>

      <style type="text/css">
      body {
        background-image:url("img/light_checkered_tiles.png");
      }

      </style>

    <?php } ?>
    
    <script>
        var ServiceURLS = {
          getDocumentsUrl:"/restserver/index.php/api/manage_docs/getdocs/format/json?type=tbs-journal",
          checkoutDocumentUrl:"/restserver/index.php/api/manage_docs/checkout/format/json",
          checkinDocumentUrl:"/restserver/index.php/api/manage_docs/checkin/format/json",
          flagDocumentUrl:"/testmodule-admin/services/flag-document.php"
        };
        angular.element(document).ready(function() {
          angular.bootstrap($("#navModule"), ["NavModule"]);
          angular.bootstrap($("#documentCheckoutApp"), ["DocumentCheckoutModule"]);
        });
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php include 'navbar.php'; ?>
 <div class="container" id="documentCheckoutApp">

    <div ng-controller="CheckOutController">
        <div class="row" >
            <p class="lead pull-left span3">Checkout Documents   </p>

            <div class="pull-right" style="transform:scale(0.75, 0.75);">
                
                   <div class="navbar pull-left" style="margin-bottom:0px;float:none;"> 
                    <div class="navbar-inner" style="border-radius:8px 8px 0 8px;">
                      <p class="navbar-text pull-left" style="margin-right:20px">Filter by</p>
                      <ul class="nav">
                        <li ng-class="{active: filterMode == 'all' }"><a  ng-click="filterBy('all')">All documents</a></li>
                        <li ng-class="{active: filterMode == 'unclaimed' }"><a  ng-click="filterBy('unclaimed')">Unclaimed</a></li>
                        <li  ng-class="{active: filterMode == 'checkedout' }"><a ng-class="{active: filterMode == 'checkedout' }"  ng-click="filterBy('checkedout')">Checked out</a></li>
                        <li  ng-class="{active: filterMode == 'checkedin' }"><a  ng-click="filterBy('checkedin')">Checked in</a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="well span3 pull-right" style="border-radius:0px 0px 8px 8px;padding:7px 8px 7px;background-color:#7f9293;margin:0;width:195px">
                    <form  class="form-horizontal pull-left" ng-submit="searchDocs()">
                      <div class="controls pull-left">
                        <div class="input-append ">
                          <input class="span2" type="text" placeholder="Search documents" ng-model="searchQuery"  tooltip-trigger="focus">
                          <button class="btn "><i class="icon-search"></i></button>
                        </div>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
            <div class="alert alert-error" ng-show="errors.checkoutError != null && errors.checkoutError != ''">{{errors.checkoutError}}</div>
      <div class="row" ng-show="documents.length == null">
      <p class="lead text-center" style="margin-top:40px">Loading.... this may take a moment.</p>
      </div>


        <div class="row" ng-show="filterMode == 'all'">
            <table class="table table-striped span12" ng-show="documents.length > 0" style="margin:auto;">
              <thead>
                <tr> 
                  <th class="small">ID</th><th class="small">User</th><th class="small">checked out</th> <th class="small">checked in</th><th colspan="2" class="small">Title</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="q in documents" >
                  <td>{{q.id}}</td>
                  <td>{{q.checkedOutBy}}</td>
                  <td>{{q.checkedOutAtDisplay}}</td>
                  <td><span ng-show="q.checkedInAtDisplay != null">{{q.checkedInAtDisplay}} <br/> <span class="label" ng-show="isAdmin" ng-class="{'label-success':q.milliseconds <= 10800000, 'label-warning':q.milliseconds > 10800000 && q.milliseconds < 18000000, 'label-danger':q.milliseconds >= 18000000}"> {{q.duration}} </span></td>
                  <td><span ng-bind-html-unsafe="q.title"></span>
                      <div class="well" ng-show="q.snippet != null"><p ng-bind-html-unsafe="q.snippet"></p></div>
                  </td>
                  <td>
                  <div class="btn-group" ng-show="q.id == currentDocument.id">

                    <button ng-show="q.id == currentDocument.id " class="btn btn-warning btn-small" ng-click="checkinDocument(q)">Mark as complete</button>
                    <button ng-show="q.id == currentDocument.id " tooltip="Report issue with this document" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn btn-danger btn-small" ng-click="reportIssue(q)"><i class="icon-flag icon-white"></i></button>
                    
                    
                  </div>
                  <ul class="nav nav-pills" ng-show="q.checkedOutBy == '' && currentDocument == null" >
                        <li><a ng-click="checkoutDocument(q)">Check out</a></li>
                  </ul>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>

        <div class="row" ng-show="filterMode == 'unclaimed'">
            <table class="table table-hover span12" ng-show="documents.length > 0" style="margin:auto;">
              <thead>
                <tr> 
                  <th class="small">ID</th><th class="small">User</th><th class="small">checked out</th> <th class="small">checked in</th><th colspan="2" class="small">Title</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="q in documents | filter:isUnclaimed" >
                  <td>{{q.id}}</td>
                  <td>{{q.checkedOutBy}}</td>
                  <td>{{q.checkedOutAtDisplay}}</td>
                  <td><span ng-show="q.checkedInAtDisplay != null">{{q.checkedInAtDisplay}} <br/> <span class="label"  ng-show="isAdmin"  ng-class="{'label-success':q.milliseconds <= 10800000, 'label-warning':q.milliseconds > 10800000 && q.milliseconds < 18000000, 'label-danger':q.milliseconds >= 18000000}"> {{q.duration}} </span></td>
                  <td><span ng-bind-html-unsafe="q.title"></span>
                    <div class="well" ng-show="q.snippet != null"><p ng-bind-html-unsafe="q.snippet"></p></div>
                  </td>
                  <td>
                  <div class="btn-group" ng-show="q.id == currentDocument.id">

                    <button ng-show="q.id == currentDocument.id " class="btn btn-warning btn-small" ng-click="checkinDocument(q)">Mark as complete</button>
                    <button ng-show="q.id == currentDocument.id " tooltip="Report issue with this document" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn btn-danger btn-small" ng-click="reportIssue(q)"><i class="icon-flag icon-white"></i></button>
                    
                    
                  </div>
                  <ul class="nav nav-pills" ng-show="q.checkedOutBy == '' && currentDocument == null" >
                        <li><a ng-click="checkoutDocument(q)">Check out</a></li>
                  </ul>
                  </td>

                   </tr>
              </tbody>
            </table>
        </div>


        <div class="row" ng-show="filterMode == 'checkedout'">
            <table class="table table-striped table-hover span12" ng-show="documents.length > 0" style="margin:auto;">
              <thead>
                <tr> 
                  <th class="small">ID</th><th class="small">User</th><th class="small">checked out</th> <th class="small">checked in</th><th colspan="2" class="small">Title</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="q in documents  | filter:isCheckedOut"  >
                  <td>{{q.id}}</td>
                  <td>{{q.checkedOutBy}}</td>
                  <td>{{q.checkedOutAtDisplay}}</td>
                  <td><span ng-show="q.checkedInAtDisplay != null">{{q.checkedInAtDisplay}} <br/> <span class="label"  ng-show="isAdmin"  ng-class="{'label-success':q.milliseconds <= 10800000, 'label-warning':q.milliseconds > 10800000 && q.milliseconds < 18000000, 'label-danger':q.milliseconds >= 18000000}"> {{q.duration}} </span></td>
                  <td><span ng-bind-html-unsafe="q.title"></span>
                      <div class="well" ng-show="q.snippet != null"><p ng-bind-html-unsafe="q.snippet"></p></div>
                  </td>
                  <td>
                  <div class="btn-group" ng-show="q.id == currentDocument.id">

                    <button ng-show="q.id == currentDocument.id " class="btn btn-warning btn-small" ng-click="checkinDocument(q)">Mark as complete</button>
                    <button ng-show="q.id == currentDocument.id " tooltip="Report issue with this document" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn btn-danger btn-small" ng-click="reportIssue(q)"><i class="icon-flag icon-white"></i></button>
                    
                    
                  </div>
                  <ul class="nav nav-pills" ng-show="q.checkedOutBy == '' && currentDocument == null" >
                        <li><a ng-click="checkoutDocument(q)">Check out</a></li>
                  </ul>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>

                <div class="row" ng-show="filterMode == 'checkedin'">
            <table class="table table-striped table-hover span12" ng-show="documents.length > 0" style="margin:auto;">
              <thead>
                <tr> 
                  <th class="small">ID</th><th class="small">User</th><th class="small">checked out</th> <th class="small">checked in</th><th colspan="2" class="small">Title</th>
                </tr>
              </thead>
              <tbody>
               <tr ng-repeat="q in documents  | filter:isCheckedIn"  >
                  <td>{{q.id}}</td>
                  <td>{{q.checkedOutBy}}</td>
                  <td>{{q.checkedOutAtDisplay}}</td>
                  <td><span ng-show="q.checkedInAtDisplay != null">{{q.checkedInAtDisplay}} <br/> <span class="label"  ng-show="isAdmin"  ng-class="{'label-success':q.milliseconds <= 10800000, 'label-warning':q.milliseconds > 10800000 && q.milliseconds < 18000000, 'label-danger':q.milliseconds >= 18000000}"> {{q.duration}} </span></td>
                  <td><span ng-bind-html-unsafe="q.title"></span>
                      <div class="well" ng-show="q.snippet != null"><p ng-bind-html-unsafe="q.snippet"></p></div>
                  </td>
                  <td>
                  <div class="btn-group" ng-show="q.id == currentDocument.id">

                    <button ng-show="q.id == currentDocument.id " class="btn btn-warning btn-small" ng-click="checkinDocument(q)">Mark as complete</button>
                    <button ng-show="q.id == currentDocument.id " tooltip="Report issue with this document" tooltip-placement="top" tooltip-trigger="mouseenter" class="btn btn-danger btn-small" ng-click="reportIssue(q)"><i class="icon-flag icon-white"></i></button>
                    
                    
                  </div>
                  <ul class="nav nav-pills" ng-show="q.checkedOutBy == '' && currentDocument == null" >
                        <li><a ng-click="checkoutDocument(q)">Check out</a></li>
                  </ul>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>
</div>

    </div>
</div>
</body>
</html>
